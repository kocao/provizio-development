var AUTH_TOKEN = $( 'meta[name=csrf-token]' ).attr( 'content' );

function onActivityCreate( message ){
	if( message.hasOwnProperty( 'error' ) ) {
		popMsg( 'Houve um erro ao tentar adicionar a atividade, tente novamente ou mais tarde.', 'error' );
	} else {
		popMsg( 'Atividade adicionada com sucesso!', 'success' );
		$( document ).trigger( 'timeLineRefresh' );
		$( '#activity-form' )[0].reset();
		$( '.parsley-success' ).removeClass( 'parsley-success' );
	}
}