( function( $ ){

	var INPUT_OUT = false;

	window.doAdminActions = function(){
		var $self = $( this );
		switch( $self.data( 'action' ) ) {
			case 'destroyProperty':
				if( confirm( 'Todo o histórico do empreendimento, associações, vendas e demais informações deste imóvel serão removidas permanentemente, deseja continuar mesmo assim?' ) ) {
					destroyProperty( $self.data( 'id' ) );
				}
				break;
			case 'destroyUser':
				if( confirm( 'Todo o histórico, associações e demais informações deste usuário serão removidas permanentemente, deseja continuar mesmo assim?' ) ) {
					destroyUser( $self.data( 'id' ) );
				}
				break;
			case 'destroyRole':
				if( confirm( 'Todos os usuários com esta role ficarão sem acesso ao sistema, deseja continuar mesmo assim?' ) ) {
					destroyRole( $self.data( 'id' ) );
				}
				break;
			case 'removeRow':
				var $row = $( this ).closest( '.admin-row-meta' );
				if( $( '.admin-row-meta[data-name="' + $row.data( 'name' ) + '"]' ).length == 1 ) {
					modalError( 'Erro!', 'Você não pode remover todos os elementos' );
					return;
				} else {
					$.ajax({
			            type: "DELETE",
			            url: '/meta/' + $row.data( 'id' ),
			            dataType: 'json',
			            data: {
			                '_method' : 'destroy',
			                'authenticity_token' : AUTH_TOKEN
			            },
			            success: function( result ){
			            	popMsg( 'Linha removida com sucesso.', 'success' );
			            	$row.fadeOut( 'fast', function(){
			            		var $repeater = $row.closest( '.repeater' );
			            		$row.remove();
			            		setRepeaterOrder( $repeater );
			            	});
			            },
			            error: function( result ) {
			            	popMsg( 'Não foi possível remover linha.', 'error' );
			            }
			        });
				}
				break;
			case 'cloneRow':
				var $row = $( this ).closest( '.admin-row-meta' );
				$.ajax({
		            type: "GET",
		            url: '/admin/add_page_meta_contents',
		            dataType: "json",
		            data: {
		            	'meta_ref_id': $row.data( 'id' ),
		                'authenticity_token' : AUTH_TOKEN
		            },
		            success: function( result ){
		            	var $newRow = $( result.tpl );
		            	$newRow.insertAfter( $row );
		            	setRepeaterOrder( $newRow.closest( '.repeater' ) );
		            	popMsg( 'Linha adicionada com sucesso!', 'success' );
		            },
		            error: function( result ) {
		            	popMsg( 'Não foi possível adicionar linha.', 'error' );
		            }
		        });
				break;
		}
	}
	window.editRoleRedirect = function(){
		window.location = $( '#current_url' ).val() + '?role=' + $( this ).val();
	}

	window.saveData = function() {
		clearTimeout( INPUT_OUT );
		var $input = $( this );
		INPUT_OUT = setTimeout( function(){
			updateData( $input );
		}, 500 );
	}

	window.updateData = function( $input ){
		$.ajax({
            type: "GET",
            url: '/admin/update_page_attr/',
            dataType: "json",
            data: {
                '_method' : 'update',
                'resource': $input.data( 'resource' ),
                'attr': $input.data( 'attr' ),
                'id': $input.data( 'id' ),
                'val': $input.val(),
                'name': $input.attr( 'name' ),
                'authenticity_token' : AUTH_TOKEN
            },
            success: function( result ){
            	popMsg( 'Campo alterado com sucesso!', 'success' );
            },
            error: function( result ) {
            	popMsg( 'Não foi possível salvar.', 'error' );
            }
        });
	}

	function initAdmin() {
		var repeaterNames = $.unique( $.map( $( '.admin-row-meta[data-type="repeater"]' ), function( value, key ){
			return $( value ).data( 'name' );
		}) );
		for( var i in repeaterNames ) {
			$( '.admin-row-meta[data-name="' + repeaterNames[i] + '"]' ).wrapAll( '<div class="repeater" data-name="repeater-for-' + repeaterNames[i] + '"></div>' );
		}
		$( '.repeater' ).sortable({
			stop: function( event, ui ){
				setRepeaterOrder( ui.item.closest( '.repeater' ) );
			}
		});
		initDropZoneForSingleImage( $( '.dropzone-si' ) );
	}

	function setRepeaterOrder( $repeater ) {
		$repeater.find( '.admin-row-meta' ).each( function( i ){
			var $input = $( this ).find( '.meta-order' );
			$input.val( i );
			updateData( $input );
		});
	}

	window.initDropZoneForSingleImage = function( $dropzone ) {
		
		Dropzone.autoDiscover = false;

		$dropzone.each( function(){
			var $si = $( this );
			$si.find( '.dropzone' ).dropzone({
			    maxFilesize: 2,
			    paramName: 'attachment[file]',
			    maxFiles: 1,
			    url: '/attachments',
			    success: function( file, picture ) {
			    	this.removeAllFiles( true ); 
			    	$si.find( 'form' ).addClass( 'dz-max-files-reached' );
			    	$si.prepend( '<img id="image-si-' + picture.id + '" class="img-responsive" src="' + picture.url_large + '"/>' );
			    	$si.find( '.admin-remove-si' ).attr( 'data-id', picture.id ).show();
			  	},
			    sending: function( file, xhr, formData ){
		            formData.append( 'attachment[attachable_id]', $si.data( 'id' ) );
		            formData.append( 'attachment[attachable_type]', $si.data( 'resource' ) );
		            formData.append( 'attachment[uploaded_by]', $si.data( 'user' ) );
		            formData.append( 'attachment[modifier]', $si.data( 'name' ) );
		        }
		  	});
		});
  	
	}

	function removeSingleImage() {
		var $button = $( this );
		var imageID = $button.attr( 'data-id' );
		$.ajax({
            type: "DELETE",
            url: '/attachments/' + imageID,
            dataType: "json",
            data: {
                '_method' : 'destroy',
                'authenticity_token' : AUTH_TOKEN
            },
            success: function( result ){
            	$( '#image-si-' + imageID ).remove();
            	$button.closest( '.dropzone-si' ).find( '.dz-max-files-reached' ).removeClass( 'dz-max-files-reached' );
            	$button.hide();
            	popMsg( 'Imagem removida com sucesso!', 'success' );
            },
            error: function( result ) {
            	popMsg( 'Não foi possível remover a imagem.', 'error' );
            }
        });
	}

	$( document ).on( 'change', '#roles-management #role', editRoleRedirect );
	$( document ).on( 'click', '.admin-action', doAdminActions );
	$( document ).on( 'keyup', '.admin-editable', saveData );
	$( document ).on( 'click', '.admin-remove-si', removeSingleImage );
	//$( document ).on( 'change', '.admin-editable', saveData );
	$( document ).on( 'ready', initAdmin );

})( jQuery );