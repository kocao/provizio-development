//= require jquery
//= require jquery_ujs
//= require dataTables/jquery.dataTables
//= require jquery-ui/widgets/sortable
//= require parsley
//= require parsley.i18n.pt-br
//= require bootstrap.min
//= require jquery.inputmask.bundle
//= require roundslider
//= require jquery.slick
//= require moment
//= require moment/pt-br.js
//= require bootstrap-datetimepicker
//= require bootbox
//= require dropzone
//= require select2
//= require fresco
//= require trix
// require submodules/squares/js/squares-renderer
// require submodules/squares/js/squares
// require submodules/squares/js/squares-elements-jquery
// require submodules/squares/js/squares-controls
// require submodules/wcp-editor/js/wcp-editor
// require submodules/wcp-editor/js/wcp-editor-controls
// require submodules/wcp-tour/js/wcp-tour
// require submodules/wcp-compress/js/wcp-compress
// require submodules/wcp-icons/js/wcp-icons
// require js/image-map-pro-defaults
// require js/image-map-pro-editor
// require js/image-map-pro-editor-content
// require js/image-map-pro-editor-local-storage
// require js/image-map-pro-editor-init-jquery
// require js/image-map-pro.min
//= require_tree .

( function( $ ){

	$( window ).on( 'load', equalheight );
	$( window ).on( 'resize', equalheight );

	window.uniqID = function() {

		 this.length = 8;
		 this.timestamp = +new Date;

		 var _getRandomInt = function( min, max ) {
			return Math.floor( Math.random() * ( max - min + 1 ) ) + min;
		 }

		 this.generate = function() {
			 var ts = this.timestamp.toString();
			 var parts = ts.split( "" ).reverse();
			 var id = "";

			 for( var i = 0; i < this.length; ++i ) {
				var index = _getRandomInt( 0, parts.length - 1 );
				id += parts[index];
			 }

			 return id;
		 }

		 return this.generate();
	}

	function equalheight(){
		$( "[data-match-height]" ).each( function() {
			var parentRow = $( this ),
		    childrenCols = $( this ).find( "[data-height-watch]" );
		    childrenCols.css( 'min-height', 'auto' );
		    childHeights = childrenCols.map( function(){
		    	return $( this ).outerHeight();
		    }).get(),
		    tallestChild = Math.max.apply( Math, childHeights );
		  	childrenCols.css( 'min-height', tallestChild );
		});
	}

	window.loadingIn = function() {
		$( '#loading' ).show();
	}

	window.loadingOut = function() {
		$( '#loading' ).hide();
	}

	window.initForms = function() {
		$( '.s2-select' ).select2();
		$( '.mask-phone' ).inputmask( '(99) 9999-9999[9]', { placeholder: "_" } );
		$( '.mask-cpf' ).inputmask( '999.999.999-99', { placeholder: "_" } );
		$( '.mask-cnpj' ).inputmask( '99.999.999/9999-99', { placeholder: "_" } );
		$( '.mask-date' ).inputmask( '99/99/9999', { placeholder: "_" } );
		$('.datepicker').datetimepicker({
      		locale: 'pt-br',
      		icons: {
        		time: 'fa fa-clock-o', date: 'fa fa-calendar', up: 'fa fa-arrow-up',  down: 'fa fa-arrow-down',
        		previous: 'fa fa-arrow-left', next: 'fa fa-arrow-right', today: 'fa fa-play', clear: 'fa fa-trash',
        		close: 'fa fa-times'
      		}
    	});
		$( '.mask-money' ).inputmask( 'decimal', {
            'alias': 'numeric',
            'groupSeparator': '.',
            'autoGroup': true,
            'digits': 2,
            'radixPoint': ",",
            'digitsOptional': false,
            'allowMinus': false,
            'placeholder': '000.000,00'
        });
        $( '[data-toggle="tooltip"]' ).tooltip();
	};

	window.modalError = function( title, text ) {
		var $modal = $( '#modal-window' );
		var content = $( '#modal-template' ).html();
		content = content.replace( '%{modal_title}', title );
		content = content.replace( '%{modal_body}', '<div class="text-center"><i class="fa fa-4 fa-exclamation-triangle" aria-hidden="true"></i><p>' + text + '</p></div>' );
		content = content.replace( '%{modal_footer}', '<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">OK</button>' );
		$modal.find( '.modal-dialog' ).removeClass( 'modal-lg' ).removeClass( 'modal-sm' );
		$modal.find( '.modal-content' ).html( content );
		$modal.modal();
	}

	window.popMsg = function( text, type, delay ){
		type = type == undefined ? 'default' : type;
		delay = delay == undefined ? 3000 : delay;
		$( '.pop-msg' ).remove();
		$pop = $( '<div class="pop-msg ' + type + '">' + text + '</div>' );
		$( 'body' ).append( $pop );
		setTimeout( function(){
			$pop.fadeOut( 'slow', function(){
				$pop.remove();
			});
		}, delay );
	}

	$.fn.copyAndReplace = function( template, data, location ) {
	    template = template instanceof jQuery ? template.html() : template;
	    location = location == undefined ? 'top' : location;
	    for( var key in data ) {
	        var regex = new RegExp( '%{' + key + '}', 'g' );
	        template = template.replace( regex, data[key] );
	    }
	    switch( location ) {
	        case 'top':
	            this.prepend( template );
	            break;
	        case 'bottom':
	            this.append( template );
	            break;
	        case 'after':
	        	this.after( template );
	       		break;
	        default:
	            if( location instanceof jQuery ) {
	                this.find( location ).replaceWith( template );
	            }
	            break;
	    }
	}

	$( document ).on( 'change', '.fill-cities', function(){
        var $origin = $( this );
        var $target = $( $origin.data( 'target' ) );
        $.ajax({
            type: "GET",
            url: '/autocomplete/cities',
            dataType: "json",
            data: {
                '_method' : 'index',
                'state_id' : $origin.val(),
                'authenticity_token' : AUTH_TOKEN
            },
            success: function( cities ){
            	var options = '';
                if( cities.length ) {
                    for( var i in cities ) {
                        var city = cities[i];
                        options+= '<option value="' + city.id + '">' + city.text + '</option>';
                    }
                    $target.html( options ).select2( 'destroy' ).select2();
                }
            },
            error : function( errors ) {
               modalError( 'Erro ao executar ação!', 'Não foi possível carregar as cidades do estado escolhido. Tente novamente ou mais tarde.' );
            }
        });
    });

    initModal = function(){
        var $modal = $( '#modal-window' );
        var content = $modal.find( '.modal-content' ).html();
        $modal.data( 'default-content', content );
        $modal.on( 'hidden.bs.modal', function( e ) {
            $modal.find( '.modal-content' ).html( $modal.data( 'default-content' ) );
        });
    }

		$( document ).on( 'ready', function(){
			initModal();
			initForms();
			// $('#myTable').DataTable({
			// 	language: {
			//     searchPlaceholder: "Buscar";
			// 	}
			// 	}
			// );
			$('#myTable	').dataTable( {
				language: {
					search: "_INPUT_",
					searchPlaceholder: "Buscar",
					"lengthMenu": "Mostrando _MENU_ registros por página",
					"zeroRecords": "Desculpe, nenhum registro encontrado",
					"info": "Mostrando página _PAGE_ de _PAGES_",
					"infoEmpty": "Nenhum registro encontrado",
					"infoFiltered": "(de um total de _MAX_ registros)",
					"paginate": {
						 "first":      "Primeiro",
						 "last":       "Último",
						 "next":       "Próximo",
						 "previous":   "Anterior"
	 			 }
				}
			});
		});

		window.navbarToggle = function navbarToggle() {
		    var x = document.getElementById("myTopnav");
		    if (x.className === "topnav") {
		        x.className += " responsive";
		    } else {
		        x.className = "topnav";
		    }
		}



})( jQuery );
