function initAttachmentsDropZone() {

	Dropzone.autoDiscover = false;

	var $container = $( '.dropzone-attachments' );

	$container.dropzone({
	    maxFilesize: 2,
	    paramName: 'attachment[file]',
	    maxFiles: 1,
	    addRemoveLinks: true,
	    autoProcessQueue: false,
	    url: '/attachments',
	    dictRemoveFile: 'Remover',
	    success: function( file, attachment ) {
	    	$container.removeClass( 'dz-started' );
	    	$( document ).trigger( 'timeLineRefresh' );
	  	},
	    sending: function( file, xhr, formData ){
	    	formData.append( 'attachment[modifier]', 'timeline' ); 
            formData.append( 'attachment[uploaded_by]', $container.find( 'attachment[uploaded_by]' ).val() );
            formData.append( 'attachment[attachable_id]', $container.find( 'attachment[attachable_id]' ).val() );
            formData.append( 'attachment[attachable_type]', $container.find( 'attachment[attachable_type]' ).val() );
        }
  	});
}

$( document ).on( 'click', '.do-dropzone', function( e ){
	e.preventDefault();
	e.stopPropagation();
	var dropzone = $( this ).closest( '.dropzone' ).get( 0 ).dropzone;
	dropzone.processQueue();
});

$( document ).on( 'ready', initAttachmentsDropZone );