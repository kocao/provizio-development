function onDealCreate( result ){
	if( result.hasOwnProperty( 'error' ) ) {
		if( result.errors.hasOwnProperty( 'quota' ) ) {
			popMsg( 'Lamentamos, mas o limite de reservas para o usuário foi atingido.', 'error' );
		} else {
			popMsg( 'Houve um erro ao tentar enviar proposta, tente novamente ou mais tarde.', 'error' );
		}
	} else {
		$( '#modal-window' ).modal( 'hide' );
		var deals = parseInt( $( '#counter-deals' ).text() );
		$( '#counter-deals' ).text( deals + 1 );
		setTimeout( function(){
			popMsg( 'Sua proposta foi enviada com sucesso! Aguarde contato do seu consultor.', 'success' );
			$( window ).trigger( 'redrawPropertyMap', [result.plant_id, result.map_data] );
		}, 400 );
	}
}

function onDealUpdate( result ){
	if( result.hasOwnProperty( 'error' ) ) {
		if( result.errors.hasOwnProperty( 'quota' ) ) {
			popMsg( 'Lamentamos, mas o limite de reservas para o usuário foi atingido.', 'error' );
		} else {
			popMsg( 'Houve um erro ao tentar enviar proposta, tente novamente ou mais tarde.', 'error' );
		}
	} else {
		$( '#modal-window' ).modal( 'hide' );
		$( '#deal-row-7' ).attr( 'data-status', result.status );
		$( '#deal-row-7' ).find( 'td.deal-status' ).html( result.status );
		popMsg( 'Sua proposta foi enviada com sucesso, aguarde o contato do seu consultor.', 'success' );
	}
}