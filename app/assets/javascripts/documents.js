( function( $ ){

	var init = function(){
		PROPERTY_ID = $( '#property_id' ).val();
		OWNER_ID = $('#document_owner_id' ).val();
		initDropzoneForDocuments();
	};

	var initDropzoneForDocuments = function() {

		Dropzone.autoDiscover = false;

		$( '#docs' ).find( '.dropzone' ).dropzone({
		    maxFilesize: 2,
		    paramName: 'document[file]',
		    maxFiles: 100,
		    url: '/documents',
		    success: function( file, doc ) {
		    	var row = '';
		    		row+= 	'<tr data-doc="' + doc.id + '">'
					row+=		'<td><input id="doc-' + doc.id + '" value="' + file.name + '" class="doc-editable" placeholder="Nome do documento" data-doc="' + doc.id  + '" type="text" name="document[name]"></td>';
					row+= 		'<td class="text-center">' + Math.round( file.size / 1024 ) + 'KB</td>';
					row+=		'<td class="text-center"><button class="doc-remove" data-doc="' + doc.id + '"><i class="fa fa-trash"></i></button></td>';
					row+= '</tr>';
					$( '.docs-table tbody' ).prepend( row );
					popMsg( 'Documento adicionado com sucesso!', 'success' );
		  	},
		    sending: function( file, xhr, formData ){
		    	loadingIn();
	            formData.append( 'document[property_id]', PROPERTY_ID );
							formData.append( 'document[owner_id]', OWNER_ID);
	        },
	        complete: function(){
		  		loadingOut();
		  	}
	  	});
	}

	window.saveDoc = function() {
		var $field = $( this );
		updateDocAttr( $field.data( 'doc' ), $field.val() );
	}

	window.updateDocAttr = function( docId, value ) {
		var object = {};
		object['document[name]'] = value;
		$.ajax({
            type: "PATCH",
            url: '/documents/' + docId,
            dataType: "json",
            data: $.extend({
                '_method' : 'update',
                'authenticity_token' : AUTH_TOKEN
            }, object ),
            success: function( doc ) {
            	popMsg( 'Nome do documento atualizado com sucesso!', 'success' );
            },
            error: function( result ) {
            	popMsg( 'Houve um erro ao atualizar, tente novamente ou mais tarde.', 'error' );
            }
        });
	}

	window.destroyDoc = function() {
		if( confirm ( 'Confirma remover o documento?' ) ) {
			var docId = $( this ).data( 'doc' )
			$.ajax({
	            type: "DELETE",
	            url: '/documents/' + docId,
	            dataType: "json",
	            data: {
	                '_method' : 'delete',
	                'authenticity_token' : AUTH_TOKEN
	            },
	            success: function( doc ) {
	            	popMsg( 'Documento removido com sucesso!', 'success' );
	            	$( '.docs-table' ).find( 'tr[data-doc="' + docId + '"]' ).remove();
	            },
	            error: function( result ) {
	            	popMsg( 'Houve um erro ao tentar remover, tente novamente ou mais tarde.', 'error' );
	            }
	        });
		}
	}

	$( document ).on( 'ready', init );
	$( document ).on( 'blur', '.doc-editable', saveDoc );
	$( document ).on( 'click', '.doc-remove', destroyDoc );

})( jQuery );
