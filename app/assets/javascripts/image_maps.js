( function( $ ){

	var TOOLTIP_CONFIG = {

	}
	var SHAPE_STATUS_CONFIG = {
		available: {
			color: '#80d4b7',
			opacity: 0.2,
			prependText: '<b class="tooltip-title">Informações do empreendimento</b><div class="tooltip-content">',
			appendText: '</div><button class="tooltip-button reserve-spot" data-imap="%{imap_id}" data-spot="%{id}" data-spot-name="%{name}">Reservar</button>'
		},
		reserved: {
			color: '#ffe7a5',
			opacity: 0.9,
			prependText: '<b class="tooltip-title">Informações do empreendimento</b><div class="tooltip-content">',
			appendText: '</div><p><i>Este empreendimento já esta reservado</i></p>'
		},
		sold: {
			color: '#df9086',
			opacity: 0.9,
			prependText: '<b class="tooltip-title">Informações do empreendimento</b><div class="tooltip-content">',
			appendText: '</div><p><i>Este empreendimento já foi vendido</i></p>'
		}
	}

	$( document ).on( 'ready', function(){
		var $closeBt = $( '<div id="wcp-editor-button-out" class="wcp-editor-main-button"><div class="wcp-editor-main-button-icon"><i class="fa fa-sign-out" aria-hidden="true"></i></div><div class="wcp-editor-main-button-text">Sair</div></div>' );

		$closeBt.on( 'click', function(){
			if( confirm( 'Deseja sair da edição?' ) ) {
				window.location = $( '#back_url' ).val();
			}
		});

		$( '#wcp-editor-main-buttons' ).append( $closeBt );
		$( '.wcp-editor-form-tab-button[data-wcp-form-tab-button-name="tooltip_content"]' ).trigger( 'click' );
	}); 

	$( document ).on( 'change', '#wcp-editor-form-control-tooltip_status select', function(){
		var $select = $( this );
		switch( $select.val() ) {
			case 'available':
				$( '#wcp-editor-form-control-fill input' ).val( SHAPE_STATUS_CONFIG.available.color ).trigger( 'change' );
				$( '#wcp-editor-form-control-mouseover_fill input' ).val( SHAPE_STATUS_CONFIG.available.color ).trigger( 'change' );
				//$( '#wcp-editor-form-control-plain_text' ).find( 'textarea' ).val( '' );
				break;
			case 'reserved':
				var color = '#ffe7a5';
				$( '#wcp-editor-form-control-fill input' ).val( SHAPE_STATUS_CONFIG.reserved.color ).trigger( 'change' );
				$( '#wcp-editor-form-control-mouseover_fill input' ).val( SHAPE_STATUS_CONFIG.reserved.color ).trigger( 'change' );
				//$( '#wcp-editor-form-control-plain_text' ).find( 'textarea' ).val( SHAPE_STATUS_CONFIG.reserved.text );
				break;
			case 'sold':
				var color = '#df9086';
				$( '#wcp-editor-form-control-fill input' ).val( SHAPE_STATUS_CONFIG.sold.color ).trigger( 'change' );
				$( '#wcp-editor-form-control-mouseover_fill input' ).val( SHAPE_STATUS_CONFIG.sold.color ).trigger( 'change' );
				//$( '#wcp-editor-form-control-plain_text' ).find( 'textarea' ).val( SHAPE_STATUS_CONFIG.sold.text );
				break;
		}
	});

	window.imageMapToDB = function( saves ) {
		if( ! saves[0].general.hasOwnProperty( 'width') ) {
			var imageSize = $( '#image_size' ).val().split( 'x' );
			var availableText = '';
			saves[0].general.width = imageSize[0];
			saves[0].general.naturalWidth = imageSize[0];
			saves[0].general.height = imageSize[1];
			saves[0].general.naturalHeight = imageSize[1];
			if( saves[0].hasOwnProperty( 'spots' ) ) {
				for( var i in saves[0].spots ) {
					var spot = saves[0].spots[i];
					var status = spot.tooltip_content.hasOwnProperty( 'tooltip_status' ) ? spot.tooltip_content.tooltip_status : 'available';
					//Specific per status
					//if( status == 'available' ) {
						availableText = spot.tooltip_content.plain_text;
						spot.tooltip_content.plain_text = spot.tooltip_content.hasOwnProperty( 'plain_text' ) ? spot.tooltip_content.plain_text : '';
						spot.tooltip_content.plain_text = SHAPE_STATUS_CONFIG[status].prependText + spot.tooltip_content.plain_text + SHAPE_STATUS_CONFIG[status].appendText.replace( '%{id}', spot.id ).replace( '%{name}', spot.title ).replace( '%{imap_id}', $( '#imap_id' ).val() );
					//} else {
					//	spot.tooltip_content.plain_text = SHAPE_STATUS_CONFIG[status].text;
					//}
					spot.actions = {
						mouseover: 'no-action',
						click: 'show-tooltip'
					}
					//Dinamyc
					spot.default_style = {
						fill: SHAPE_STATUS_CONFIG[status].color,
						fill_opacity: SHAPE_STATUS_CONFIG[status].opacity
					};
					spot.mouseover_style = {
						fill: SHAPE_STATUS_CONFIG[status].color,
						fill_opacity: 0.95
					};
					spot.tooltip_style = {
						border_radius: 0,
						background_color: '#FFFCF2',
						background_opacity: 0.9,
						position: 'right'
					};
					spot.tooltip_content.plain_text_color = '#767676';
					saves[0].spots[i] = spot;
				}
			}
		}
		if( ! saves[0].hasOwnProperty( 'image') ) {
			saves[0].image = {
				url: $( '#image_url' ).val()
			}
		}
		var dataToSave = {};
		if( availableText != '' ) {
			dataToSave.available_text = availableText;
		}
		$.ajax({
            type: 'PATCH',
            url: '/image_maps/' + $( '#imap_id' ).val(),
            dataType: 'json',
            data: $.extend({
                '_method' : 'update',
                'authenticity_token' : AUTH_TOKEN,
                'map_data': JSON.stringify( saves )
            }, dataToSave )
        });
	}

	window.onImageMapUpdate = function( result ) {
		if( result.hasOwnProperty( 'error' ) ) {
			modalError( 'Erro ao tentar salvar.', 'Não foi possível executar esta ação, verifique sua conexão e tente novamente.' );
		} else {
			setTimeout( function(){
				popMsg( 'Imagem salva com sucesso!', 'success' );
			}, 400 );
		}
	}


})( jQuery ); 