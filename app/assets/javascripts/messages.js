var AUTH_TOKEN = $( 'meta[name=csrf-token]' ).attr( 'content' );

function onDealMessageCreate( message ) {
    if( message.hasOwnProperty( 'error' ) ) {
        popMsg( 'Houve um erro ao tentar enviar mensagem, tente novamente ou mais tarde.', 'error' );
    } else {
        popMsg( 'Sua mensagem foi enviada com sucesso!', 'success' );
        $( '#modal-window' ).modal( 'toggle' );
        $( '#deal-row-' + message.deal_id ).find( '.deal-status' ).text( message.deal_status_text );
    } 
}

function onMessageCreate( message ){
	if( message.hasOwnProperty( 'error' ) ) {
		popMsg( 'Houve um erro ao tentar enviar mensagem, tente novamente ou mais tarde.', 'error' );
	} else {
		popMsg( 'Sua mensagem foi enviada com sucesso!', 'success' );
        $( document ).trigger( 'timeLineRefresh' );
		$( '#message-form' )[0].reset();
		$( '.parsley-success' ).removeClass( 'parsley-success' );
        $( '#message-attachments' ).find( '.message-attachments-items' ).empty();
		addMessageToBox( message, 'sent' );
	}
}

function destroyMessage( messageId, skipTrash ) {
	event.preventDefault();
	skipTrash = skipTrash == undefined ? false : skipTrash;
	$.ajax({
        type: 'DELETE',
        url: '/messages/' + messageId,
        dataType: 'json',
        data: {
            '_method' : 'destroy',
            'authenticity_token' : AUTH_TOKEN,
            'skip_trash' : skipTrash
        },
        success: function( message ){
        	var text =  message.trashed ? 'Sua mensagem foi movida para a lixeira.' : 'Mensagem removida com sucesso!';
        	popMsg( text, 'success' );
        	$( '.message-item[data-id="' + messageId + '"]' ).remove();
        	if( message.trashed ) addMessageToBox( message, 'trash' );
        },
        error: function( message ) {
        	modalError( 'Erro ao tentar deletar mensagem.', 'Não foi possível executar esta ação, verifique sua conexão e tente novamente.' );
        }
    });
}

function addMessageToBox( message, box ) {
	$( '#' + box ).find( '.message-item-header' ).copyAndReplace( $( '#message-item-template' ), message, 'after' );
}

$( document ).on( 'click', '[data-action="trash"]', function(){
	var $self = $( this );
	var skipTrash = $self.data( 'box' ) == 'trash' ? true : false;
	destroyMessage( $self.data( 'id' ), skipTrash );
    if( $self.data( 'redirect-to' ) != undefined ) {
        window.location = $self.data( 'redirect-to' );
    }
});
$( document ).on( 'click', '[data-action="reply"]', function( event ){
    event.preventDefault();
    var $self = $( this );
    $( '.message-form' ).show();
    $( '#message_to' ).val( $self.data( 'value' ) );
    $( '#message_subject' ).val( 'RES:' + $self.data( 'subject' ) );
}); 

$( document ).on( 'click', '[data-action="forward"]', function( event ){
    event.preventDefault();
    var $self = $( this );
    $( '.message-form' ).show();
    $( '#message_to' ).val( '' );
    $( '#message_subject' ).val( 'ENC:' + $( '#orig_message_subject' ).val() );
    $( 'trix-editor' ).html( $( '#orig_message_text' ).val() );
}); 

$( document ).on( 'click', '.remove-message-attachment', function(){
    var $self = $( this );
    $.ajax({
        type: 'DELETE',
        url: '/attachments/' + $self.data( 'id' ),
        dataType: 'json',
        data: {
            '_method' : 'destroy',
            'authenticity_token' : AUTH_TOKEN
        },
        success: function( message ){
            popMsg( 'Anexo removido com sucesso!', 'success' );
            $self.closest( '.message-attachment-item' ).remove();
        },
        error: function( message ) {
            popMsg( 'Erro ao tentar remover anexo', 'error' );
        }
    });
});