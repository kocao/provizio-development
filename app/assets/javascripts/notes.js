var AUTH_TOKEN = $( 'meta[name=csrf-token]' ).attr( 'content' );

function onNoteCreate( message ){
	if( message.hasOwnProperty( 'error' ) ) {
		popMsg( 'Houve um erro ao tentar adicionar a nota, tente novamente ou mais tarde.', 'error' );
	} else {
		popMsg( 'Nota adicionada com sucesso!', 'success' );
		$( document ).trigger( 'timeLineRefresh' );
		$( '#note-form' )[0].reset();
		$( '.parsley-success' ).removeClass( 'parsley-success' );
	}
}