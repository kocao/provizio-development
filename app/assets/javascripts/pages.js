function init() {
	$( '.full-sliders' ).slick({
		dots: true,
		autoplay: false,
		arrows: false
	});
}
$( document ).on( 'ready', init );