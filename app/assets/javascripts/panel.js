function removeContact( id ) {
    $.ajax({
        type: "GET",
        url: '/panel/remove_contact/' + id,
        dataType: "json",
        data: {
            'authenticity_token' : AUTH_TOKEN
        },
        success: function( result ){
        	popMsg( 'Contato removido com sucesso!', 'success' );
            $( '#bio-' + result.id ).fadeOut( function(){
                $( this ).remove();
            });
        },
        error : function( errors ) {
           modalError( 'Erro ao executar ação!', 'Não foi possível remover o contato. Tente novamente ou mais tarde.' );
        }
    });
}

$( document ).on( 'click', '.bio-actions [data-action="remove"]', function( e ){
    e.preventDefault();
    var $self = $( this );
    if( confirm( 'Confirma remover o contato?' ) ) {
        removeContact( $self.data( 'id' ) );
    }
});

$( document ).on( 'click', '.lt-message-more', function(){
    var $container = $( this ).closest( '.timeline-resume' );
    if( $container.hasClass( 'collapsed' ) ) {
        $container.removeClass( 'collapsed' );
        $container.css( 'height', '250px' );
        $container.find( '.lt-message' ).css( 'height', '170px' );
        $container.find( '.txt' ).css( 'height', '100px' );
    } else {
        $container.addClass( 'collapsed' );
        $container.css( 'height', 'auto' );
        $container.find( '.lt-message' ).css( 'height', 'auto' );
        $container.find( '.txt' ).css( 'height', 'auto' );
    }
});

$( document ).on( 'click', '.see-more', function( e ){
    e.preventDefault();
    var $container = $( this ).closest( '.notify-list-item' );
    if( $container.hasClass( 'collapsed' ) ) {
        $container.removeClass( 'collapsed' );
    } else {
        $container.addClass( 'collapsed' );
    }
});

$( document ).on( 'click', '#pushes', function(){
    var $self = $( this );
    var $badge = $self.find( '.badge' );
    $( '#push-dropdown' ).toggle();
    if( parseInt( $badge.data( 'count' ) ) > 0 ) {
        $.ajax({
            type: "GET",
            url: '/panel/mark_notifications_as_visualiazed',
            dataType: "json",
            data: {
                'authenticity_token' : AUTH_TOKEN
            },
            success: function( result ){
                $badge.attr( 'data-count', 0 );
            }
        });
    }
});
$( document ).on( 'click', '#pushes-group', function(){
    $( '#push-dropdown-group' ).toggle();
});
$( document ).on( 'click', '#pushes-role', function(){
    $( '#push-dropdown-role' ).toggle();
});
$( document ).on( 'click', '.push-list .notification', function(){
    var $self = $( this );
    window.location = $( '#notifications_url' ).val() + '?period=single&notification=' + $self.data( 'notification-id' );
});

$( document ).on( 'click', '.change-realtor', function( e ){
    e.preventDefault();
    var $self = $( this );
    if( $self.hasClass( 'active' ) ) {
        modalError( 'Aviso', $self.data( 'name' ) + ' já é seu consultor.' );
    } else {
        bootbox.confirm( 'Escolher ' + $self.data( 'name' ) + ' como seu consultor?', function( result ){
            if ( result ) {
                $.ajax({
                    type: "GET",
                    url: '/panel/change_realtor/' + $self.data( 'id' ),
                    dataType: "json",
                    data: {
                        'authenticity_token' : AUTH_TOKEN
                    },
                    success: function( result ){
                        popMsg( $self.data( 'name' ) + ' agora é seu consultor.', 'success' );
                        $( '.change-realtor' ).removeClass( 'active' );
                        $self.addClass( 'active' );
                        window.location = $( '#redirect_to' ).val();
                    },
                    error : function( errors ) {
                       modalError( 'Erro ao executar ação!', 'Não foi possível alterar o consultor. Tente novamente ou mais tarde.' );
                    }
                });
            }
        });
    }
});

$( document ).on( 'click', '.choose-role', function( e ){
    var $self = $( this );
    if( confirm( 'Confirma a escolha da opção: ' + $self.data( 'role-name' ) + '?' ) ) {
        $.ajax({
            type: "GET",
            url: '/panel/choose_role',
            dataType: "json",
            data: {
                'role': $self.data( 'role' ),
                'authenticity_token' : AUTH_TOKEN
            },
            success: function( result ){
                popMsg( 'Tudo certo, estamos redirecionando você para o seu painel.', 'success' );
                location.reload();
            },
            error : function( errors ) {
               modalError( 'Erro ao executar ação!', 'Esta ação não pode ser executada no momento. Tente novamente ou mais tarde.' );
            }
        });
    }
});

$( document ).on('click', '.slider-link', function(e){
	e.preventDefault();
	var selected = $(this).attr('id');
	$('.slider-profile').addClass('hidden');
	$("." + selected).removeClass('hidden');
	$('.slider-link').attr('data-following', 'false');
	$(this).attr('data-following', 'true');
});
window.initAutocomplete = function initAutocomplete() {
    var lat = parseFloat($('#user_latitude').val());
    var lng = parseFloat($('#user_longitude').val());
    if (isNaN(lat) || isNaN(lng)){
      lat = parseFloat(-33.8688);
      lng = parseFloat(151.2195);

    }

    var map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: lat, lng: lng},
      zoom: 13,
      mapTypeId: 'roadmap'
    });

    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
      searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }

      // Clear out the old markers.
      markers.forEach(function(marker) {
        marker.setMap(null);
      });
      markers = [];

      var location = places[0].geometry.location;
      $("#user_longitude").val(location.lng());
      $("#user_latitude").val(location.lat());

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function(place) {
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }
        var icon = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        // Create a marker for each place.
        markers.push(new google.maps.Marker({
          map: map,
          icon: icon,
          title: place.name,
          position: place.geometry.location
        }));

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      map.fitBounds(bounds);
    });
  }
