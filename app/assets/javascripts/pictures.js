( function( $ ){

	var INPUT_OUT = null;

	window.savePicture = function() {
		clearTimeout( INPUT_OUT );
		var $field = $( this );
		INPUT_OUT = setTimeout( function(){
			updatePictureAttr( $field.data( 'id' ), $field.val() );
		}, 700 );
	}

	window.updatePictureAttr = function( picId, value ) {
		var object = {};
		object['picture[label]'] = value;
		$.ajax({
            type: "PATCH",
            url: '/pictures/' + picId,
            dataType: "json",
            data: $.extend({
                '_method' : 'update',
                'authenticity_token' : AUTH_TOKEN
            }, object ),
            success: function( doc ) {
            	popMsg( 'Imagem atualizada com sucesso!', 'success' );
            },
            error: function( result ) {
            	popMsg( 'Houve um erro ao atualizar, tente novamente ou mais tarde.', 'error' );
            }
        });
	}

	window.destroyPicture = function() {
		if( confirm ( 'Confirma remover a planta?' ) ) {
			var docId = $( this ).data( 'doc' )
			$.ajax({
	            type: "DELETE",
	            url: '/pictures/' + docId,
	            dataType: "json",
	            data: {
	                '_method' : 'delete',
	                'authenticity_token' : AUTH_TOKEN
	            },
	            success: function( doc ) {
	            	popMsg( 'Planta removida com sucesso!', 'success' );
	            }, 
	            error: function( result ) {
	            	popMsg( 'Houve um erro ao tentar remover, tente novamente ou mais tarde.', 'error' );
	            }
	        });
		}
	}

	$( document ).on( 'keyup', '.plant-editable', savePicture );
	$( document ).on( 'click', '.plant-remove', destroyPicture );

})( jQuery );