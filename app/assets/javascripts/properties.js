( function( $ ){

	$( document ).ajaxStart( function() {
		loadingIn();
	});

	$( document ).ajaxComplete( function() {
		loadingOut();
	});

	var AUTH_TOKEN = $( 'meta[name=csrf-token]' ).attr( 'content' );
	var PROPERTY_ID = 0;
	var CURRENT_USER_ID = 0;

	window.onPropertyCreate = function( property ){
		window.location = property.editPath
	}

	window.onPropertyUpdate = function( result ){
		if( result.hasOwnProperty( 'error' ) ) {
			popMsg( 'Houve um erro ao atualizar, tente novamente ou mais tarde.', 'error' );
		} else {
			popMsg( 'Empreendimento salvo com sucesso!', 'success' );
			updateMap( result.latitude, result.longitude );
		}
	}

	window.updateMap = function( latitude, longitude ) {
		var $propertyMap = $( '#property-map' );
		var curLatitude = $propertyMap.data( 'latitude' );
		var curLongitude = $propertyMap.data( 'longitude' );

		if( arguments.length == 0 ) {
			latitude = curLatitude;
			longitude = curLongitude;
		} else {
			if( latitude != curLatitude && longitude != curLongitude ) {
				$propertyMap.data( 'latitude', latitude );
				$propertyMap.data( 'longitude', longitude );
			} else {
				return;
			}
		}

		if( latitude == undefined || longitude == undefined ) return;

		var latlng = new google.maps.LatLng( latitude, longitude );
		var mapOptions = {
			zoom: 16,
		  	center: latlng,
		  	mapTypeId: 'satellite'
		};
		var map = new google.maps.Map( document.getElementById( 'map-container' ), mapOptions );
		var marker = new google.maps.Marker({
        	position: latlng,
          	map: map
        });
	}

	window.updateStage = function( value ) {
		if( typeof value == 'number' ) {
			updatePropertyAttr( 'property[stage]', value );
		}
	}

	window.initProgressCircle = function( value ) {
		value = value == undefined ? 0 : value;
		$( '#stage-circle' ).roundSlider({
			radius: 115,
		    width: 14,
		    handleSize: "+8",
		    handleShape: "dot",
		    sliderType: "min-range",
		    value: value,
		    readOnly: $( '#editable' ).val() == 1 ? false : true,
		    change: function(){
		    	updateStage( this._prechange );
		    }
		});
		$( '.stage-circle-small' ).roundSlider({
			radius: 40,
		    width: 8,
		    handleSize: "+8",
		    handleShape: "dot",
		    sliderType: "min-range",
		    value: value,
		    readOnly: true
		});
	}

	window.initSlider = function(){
		$( '.slider-first:not([data-slider="false"])' ).each( function(){
			var $self = $( this );
			var $container = $self.closest( '.slider-container' );
			var items = $container.data( 'items' );
			var config = {}
			switch( items ) {
				case 1:
					config.variableWidth = true;
					config.slidesToShow = 1;
					config.infinite = true;
					break;
				case 2:
					config.variableWidth = false;
					config.slidesToShow = 4;
					config.infinite = true;
					break;
				case 3:
					config.variableWidth = false;
					config.slidesToShow = 3;
					config.infinite = true;
					break;
				case 4:
					config.variableWidth = true;
					config.slidesToShow = 3;
					config.infinite = true;
					break;
				default:
					config.variableWidth = true;
					config.slidesToShow = 4;
					config.infinite = true;
					break;
			}
			$self.slick({
			  	slidesToShow: config.slidesToShow,
			  	slidesToScroll: config.slidesToShow,
			  	variableWidth: config.variableWidth,
			  	infinite: config.infinite,
			  	centerMode: true,
			  	arrows: true,
					responsive: [
						     {
						       breakpoint: 969,
						       settings: {
						         slidesToShow: 1,
						         slidesToScroll: 1
						       }
						     }
						   ]

			});
		});
	}

	window.saveProperty = function() {
		var $field = $( this );
		var value = $field.val();
		if( $field.hasClass( 'formatted_content' ) ) {
			$field = $( '#' + $field.attr( 'input' ) );
		}
		if( $field.attr( 'type' ) == 'checkbox' ) {
			value = $field.is( ':checked' );
		}
		updatePropertyAttr( $field.attr( 'name' ), value );
	}

	window.updatePropertyAttr = function( attr, value ) {
		var object = {};
		object[attr] = value;
		$.ajax({
            type: "PATCH",
            url: '/properties/' + PROPERTY_ID,
            dataType: "script",
            data: $.extend({
                '_method' : 'update',
                'authenticity_token' : AUTH_TOKEN
            }, object ),
            error: function( result ) {
            	modalError( 'Erro ao salvar empreendimento', '<p><strong>Para correção do problema tente:</strong></p><ul class="list"><li>Verifique se os campos nome, tipo foram preenchidos corretamente.</li><li>Recarregue sua página e tente novamente.</li></ul>' );
            }
        });
	}

	window.initDropZoneForGallery = function() {

		Dropzone.autoDiscover = false;

		$( '#property-gallery' ).find( '.dropzone' ).dropzone({
		    maxFilesize: 2,
		    paramName: 'picture[file]',
		    maxFiles: 4,
		    url: '/pictures',
		    success: function( file, picture ) {
		    	var content = $( '#gallery-item-template' ).html();
		    	content = content.replace( /\%\{id\}/g, picture.id ).replace( /\%\{thumb\}/g, picture.thumb ).replace( /\%\{label\}/g, picture.label );
		    	$( '#property-edit-gallery .property-slider' ).append( content );
		    	updateGalleryCount();
		  	},
		    sending: function( file, xhr, formData ){
		    	loadingIn();
	            formData.append( 'picture[kind]', 'gallery' );
	            formData.append( 'picture[property_id]', PROPERTY_ID );
	        },
	        complete: function(){
		  		loadingOut();
		  	}
	  	});

		$( '#property-stage-gallery' ).find( '.dropzone' ).dropzone({
		    maxFilesize: 2,
		    paramName: 'picture[file]',
		    maxFiles: 99,
		    url: '/pictures',
		    success: function( file, picture ) {
		    	var content = $( '#gallery-item-template' ).html();
		    	content = content.replace( /\%\{id\}/g, picture.id ).replace( /\%\{thumb\}/g, picture.thumb ).replace( /\%\{label\}/g, '' );
		    	$( '#property-edit-stage-gallery .property-slider' ).append( content );
		    	$( '#property-stage-gallery' ).find( '.notify-users' ).removeClass( 'hide' );
		  	},
		    sending: function( file, xhr, formData ){
		    	loadingIn();
	            formData.append( 'picture[kind]', 'stage' );
	            formData.append( 'picture[property_id]', PROPERTY_ID );
	        },
	        complete: function(){
		  		loadingOut();
		  	}
	  	});

	}

	window.initDropZoneForBanner = function() {

		Dropzone.autoDiscover = false;

		$( '.dropzone-banner' ).each( function(){
			var $banner = $( this );
			$banner.find( '.dropzone' ).dropzone({
			    maxFilesize: 2,
			    paramName: 'picture[file]',
			    maxFiles: 1,
			    url: '/pictures',
			    success: function( file, picture ) {
			    	if( $banner.data( 'redirect' ) != undefined ) {
		    			window.location = $banner.data( 'redirect' ).replace( '#', '?picture_id=' + picture.id + '#' );
			    	} else {
			    		$banner.css( 'background-image', 'url( ' + picture.url + ')' );
			    		$banner.find( '.remove-picture' ).attr( 'data-id', picture.id );
			    	}
			  	},
			    sending: function( file, xhr, formData ){
			    	loadingIn();
		            formData.append( 'picture[kind]', $banner.data( 'kind' ) );
		            formData.append( 'picture[property_id]', PROPERTY_ID );
		        },
		        complete: function(){
			  		loadingOut();
			  	}
		  	});
		});

	}

	window.updateGalleryCount = function(){
		var $gallery = $( '#property-edit-gallery' );
		$gallery.attr( 'data-items', $gallery.find( '.gallery-item' ).length );
	}

	window.removePicture = function( event ) {
		event.preventDefault();
		var $picture = $( this );
		if( $picture.data( 'action' ) == 'destroyPlant' ) {
			if( ! confirm( 'Isso irá remover a planta e quaisquer negócios ligados a ela, tem certeza que deseja continuar?' ) ) {
				return;
			}
		}
		$.ajax({
            type: 'DELETE',
            url: '/pictures/' + $picture.attr( 'data-id' ),
            dataType: 'json',
            data: {
                '_method' : 'destroy',
                'authenticity_token' : AUTH_TOKEN
            },
            success: function( result ){
            	popMsg( 'Imagem removida com sucesso!', 'success' );
            	if( $picture.data( 'action' ) == 'remove' ) {
            		$( $picture.data( 'element' ) ).fadeOut( 'fast', function(){
            			$( this ).remove();
            			updateGalleryCount();
            		});
            	}else if( $picture.data( 'action' ) == 'destroyPlant' ) {
            		location.reload();
            	} else {
            		$( $picture.data( 'element' ) ).attr( 'style', '' );
            	}
            },
            error: function( result ) {
            	modalError( 'Erro ao tentar remover imagem.', 'Não foi possível remover, verifique sua conexão e tente novamente.' );
            }
        });
	}

	window.followProperty = function( propertyId, userId, $context ) {
		event.preventDefault();
		$.ajax({
            type: 'POST',
            url: '/accompaniments/',
            dataType: 'json',
            data: {
                '_method' : 'create',
                'authenticity_token' : AUTH_TOKEN,
                'accompaniment[user_id]': userId,
                'accompaniment[property_id]': propertyId
            },
            success: function( result ){
            	popMsg( 'Você esta seguindo este imóvel', 'success' );
            	$context.attr( 'data-following', true ).attr( 'data-accompaniment-id', result.id );
            },
            error: function( result ) {
            	modalError( 'Erro ao tentar seguir imóvel.', 'Não foi possível executar esta ação, verifique sua conexão e tente novamente.' );
            }
        });
	}

	window.unfollowProperty = function( accompanimentId, $context ) {
		event.preventDefault();
		$.ajax({
            type: 'DELETE',
            url: '/accompaniments/' + accompanimentId,
            dataType: 'json',
            data: {
                '_method' : 'destroy',
                'authenticity_token' : AUTH_TOKEN
            },
            success: function( result ){
            	popMsg( 'Você parou de seguir este imóvel', 'success' );
            	$context.attr( 'data-following', false ).attr( 'data-accompaniment-id', '' );
            },
            error: function( result ) {
            	modalError( 'Erro ao tentar executar ação.', 'Não foi possível executar esta ação, verifique sua conexão e tente novamente.' );
            }
        });
	}

	window.initGallerySorter = function() {
		var $slider = $( '.property-slider.sortable' );
		$slider.sortable({
			stop: function( event, ui ){
				var pics = [];
				$slider.find( '.gallery-item' ).each( function( i ){
					var $sliderItem = $( this );
					$.ajax({
			            type: 'PATCH',
			            url: '/pictures/' + $( this ).data( 'id' ),
			            dataType: 'json',
			            data: {
			                '_method' : 'update',
			                'authenticity_token' : AUTH_TOKEN,
			                'picture[sequence]' : i + 1
			            },
			            success: function(){
			            	popMsg( 'Imagem atualizada', 'success' );
			            	$( window ).trigger( 'onGalleryImageUpdated', [$sliderItem] );
			            },
			            error: function(){
							popMsg( 'Houve um erro ao tentar atualiza a imagen. Recarregue a página e tente novamente.', 'error' );
			            }
			        });
				});
			}
		});
	}

	window.init = function(){
		PROPERTY_ID = $( '#property_id' ).val();
		CURRENT_USER_ID = $( '#current_user_id' ).val();
		initProgressCircle( $( '#property_stage' ).val() );
		initSlider();
		initDropZoneForBanner();
		initDropZoneForGallery();
		initGallerySorter();
// initAutocomplete();
	};

	window.doActions = function(){
		var $self = $( this );
		switch( $self.data( 'action' ) ) {
			case 'follow':
				if( $self.attr( 'data-following' ) == 'false' ) {
					followProperty( PROPERTY_ID, CURRENT_USER_ID, $self );
				} else {
					unfollowProperty( $self.attr( 'data-accompaniment-id' ), $self );
				}
				break;
			case 'fast-follow':
				if( $self.attr( 'data-following' ) == 'false' ) {
					followProperty( $self.data( 'id' ), CURRENT_USER_ID, $self );
				} else {
					unfollowProperty( $self.attr( 'data-accompaniment-id' ), $self );
				}
				break;
			case 'destroy':
				if( confirm( 'Todo o histórico do empreendimento, associações, vendas e demais informações deste imóvel serão removidas permanentemente, deseja continuar mesmo assim?' ) ) {
					destroyProperty( $self.data( 'id' ) );
				}
				break;
		}
	}

	window.destroyProperty = function( id ) {
		event.preventDefault();
		$.ajax({
	        type: 'DELETE',
	        url: '/properties/' + id,
	        dataType: 'json',
	        data: {
	            '_method' : 'destroy',
	            'authenticity_token' : AUTH_TOKEN
	        },
	        success: function( property ){
	        	popMsg( 'Empreendimento removido com sucesso', 'success' );
	        	$( '#property-item-' + property.id ).fadeOut( 'fast' );
	        },
	        error: function( message ) {
	        	modalError( 'Erro ao tentar deletar empreendimento.', 'Não foi possível executar esta ação, verifique sua conexão e tente novamente.' );
	        }
	    });
	}

	window.toggleMapNav = function(){
		$self = $( this );
		$container = $self.closest( '.property-map' );
		$container.toggleClass( 'navigating' );
	}

	window.reserveSpot = function( event ) {
		var $button = $( this );
		var $cta = $( '#property-actions' ).find( '[data-action="proposal"]' );
		var screenX = 0;
		var screenY = $( window ).scrollTop();
		$( window ).on( 'onDealModalShown', function( e, $modal ) {
			$( '#deal_plant_id' ).find( 'option[value="' + $button.data( 'imap' ) + '"]' ).attr( 'selected', true );
			$( '#deal_plant_id' ).trigger( 'change' );
			$( '#deal_spot' ).find( 'option[value="' + $button.data( 'spot-name' ) + '"]' ).attr( 'selected', true );
		  	$( '#deal_spot' ).trigger( 'change' );
		  	$( '.deal-button[data-form="reservation"]' ).trigger( 'click' );
		  	$( window ).off( 'onDealModalShown' );
		});
		$( window ).on( 'hidden.bs.modal', function( e, $modal ) {
			window.scrollTo(screenX, screenY);
			$( window ).off( 'hidden.bs.modal' );
		});
		$cta.trigger( 'click' );
	}

	$( window ).on( 'redrawPropertyMap', function( e, plantId, map_data ){
		var data = JSON.parse( map_data )[0];
	    $('#image-map-pro-container-' + plantId).imageMapPro(data);
	});

	window.notifyGallery = function(){
		event.preventDefault();
		$.ajax({
	        type: 'GET',
	        url: '/panel/notify_gallery',
	        dataType: 'json',
	        data: {
	            '_method' : 'notify_gallery',
	            'authenticity_token' : AUTH_TOKEN,
	            'property_id': PROPERTY_ID
	        },
	        success: function( property ){
	        	popMsg( 'Usuários notificados com sucesso', 'success' );
	        	$( '#property-stage-gallery' ).find( '.notify-users' ).addClass( 'hide' );
	        },
	        error: function( message ) {
	        	modalError( 'Erro ao tentar enviar notificação.', 'Não foi possível executar esta ação, verifique sua conexão e tente novamente.' );
	        }
	    });
	}

	window.dealAction = function(){
		event.preventDefault();
		var $bt = $( this );
		$( '.deal-partial-form' ).addClass( 'hide' );
		$( '.deal-button' ).removeClass( 'active' );
		$bt.addClass( 'active' );
		$( '#' + $bt.data( 'form' ) + '-form' ).removeClass( 'hide' );
		if( $bt.data( 'form' ) == 'reservation' ) {
			$( '#deal_status' ).val( 'reserved' );
		} else {
			$( '#deal_status' ).val( 'waiting_customer' );
		}
		$( '#deal-send' ).attr( 'disabled', false );
	}

	window.toggleUserDealForm = function(){
		var $btn = $( this );
		var $container = $( '#deal-new-user-form' );
		if( $container.hasClass( 'hide' ) ) {
			$container.removeClass( 'hide' );
			$btn.text( 'Cancelar cadastro' );
			$( 'select#deal_customer_id' ).select2( 'destroy' ).val( '' ).attr( 'disabled', true ).select2();
			$( '#deal-send' ).attr( 'disabled', true );
		} else {
			$container.addClass( 'hide' );
			$btn.text( 'Cadastrar novo' );
			$( 'select#deal_customer_id' ).select2( 'destroy' ).val( '' ).attr( 'disabled', false ).select2();
			$( '#deal-send' ).attr( 'disabled', false );
			$( '.deal-user-field' ).each( function(){
				$( this ).val( '' );
			});
		}
	}

	var keyOff = null;

	window.checkUserFormIsReady = function(){
		var fieldsok = true;
		clearTimeout( keyOff );
		keyOff = setTimeout( function(){
			$( '.deal-user-field' ).each( function(){
				if( $( this ).val() == '' ) {
					fieldsok = false;
				}
			});
			if( fieldsok ) {
				$( '#deal-send' ).attr( 'disabled', false );
			} else {
				$( '#deal-send' ).attr( 'disabled', true );
			}
		}, 300 );
	}

	$( document ).on( 'ready', init );
	$( document ).on( 'blur', '.editable', saveProperty );
	$( document ).on( 'change', '.editable', saveProperty );
	$( document ).on( 'click', '.remove-picture', removePicture );
	$( document ).on( 'click', '.action-button', doActions );
	$( document ).on( 'click', '.map-toggle-nav', toggleMapNav );
	$( document ).on( 'click', '.reserve-spot', reserveSpot );
	$( document ).on( 'click', '#notify-gallery', notifyGallery );
	$( document ).on( 'click', '.deal-button', dealAction );
	$( document ).on( 'click', '.open-deal-new-user', toggleUserDealForm );
	$( document ).on( 'keyup', '.deal-user-field', checkUserFormIsReady );

	$( document ).on( 'click', '.btn-provizio', function(){
		var vlr_min = $('#vlr_min').val();
		var vlr_max = $('#vlr_max').val();
		var city_id = $('#city_id').val();
		var state_id = $('#state_id').val();
		var user_id = $('#user_id').val();
		var district = '';
		var kinds = [];
		var stages = [];

		$( ".check_kind" ).each(function() {
			if($(this).is(':checked')){
				kinds.push($( this ).val());
			}
		});

		$( ".stages" ).each(function() {
			if($(this).is(':checked')){
				stages.push($( this ).val());
			}
		});

		$.post( "/properties/search", {vlr_min: vlr_min, vlr_max: vlr_max, city_id: city_id,
				state_id: state_id, district: district, kinds: kinds, user_id: user_id, stages: stages}, function( data ) {
			$('.search-result').html(data);
		});
	});

	window.hideGallery = function(){
		$('.gallery-op').addClass('hidden-xs hidden-sm');
		$('.title-op').removeClass('bg-orange');
		$('.title-op').addClass('bg-white-gallery');
		$(this).removeClass('bg-white-gallery');
		$(this).addClass('bg-orange');
		gallery = $(this).next().toggle();
		if (gallery.hasClass('hidden-xs')){
			gallery.removeClass('hidden-xs')
		} else {
			gallery.addClass('hidden-xs')
		}
		if (gallery.hasClass('hidden-sm')){
			gallery.removeClass('hidden-sm')
		} else {
			gallery.addClass('hidden-sm')
		}
	}
	$( document ).on( 'click', '.title-op', hideGallery );



})( jQuery );
