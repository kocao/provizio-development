( function( $ ){

	var AUTH_TOKEN = $( 'meta[name=csrf-token]' ).attr( 'content' );

	$( document ).on( 'click', '.admin-kind-action', function( e ){
		e.preventDefault();
		var $self = $( this );
		if( $self.data( 'action') == 'destroy' ) {
			if( $self.data( 'count' ) == 0 && confirm( 'Confirma a remoção deste item?' ) ) {
				$.ajax({
		            type: "DELETE",
		            url: '/property_kinds/' + $self.data( 'id' ),
		            dataType: "json",
		            data: {
		                '_method' : 'delete',
		                'authenticity_token' : AUTH_TOKEN
		            },
		            success: function( kind ) {
		            	popMsg( 'Item removido com sucesso!', 'success' );
		            	$( '#kind-row-' + kind.id ).remove();
		            }, 
		            error: function( result ) {
		            	popMsg( 'Houve um erro ao tentar remover, tente novamente ou mais tarde.', 'error' );
		            }
		        });
			} else {
				alert( 'Não é possível remover este item pois existem empreendimentos pertecentes a ele.' );
			}
		}
	});

	window.onPropertyKindCreate = function(){
		popMsg( 'Tipo de empreendimento cadastrado com sucesso! Atualizando...', 'success' );
		$( '#modal-window' ).find( 'button[type="submit"]' ).attr( 'disabled', true ).text( 'Aguarde...' );
		location.reload();
	};

	window.onPropertyKindUpdate = function( kind ){
		$( '#modal-window' ).modal( 'toggle' );
		popMsg( 'Tipo de empreendimento atualizado com sucesso!', 'success' );
		$( '#property-kinds-table' ).copyAndReplace( $( '#kind-item-template' ), kind, $( '#kind-row-' + kind.id ) );
	}

})( jQuery );