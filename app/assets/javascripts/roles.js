function onRoleCreate( result ) {
	if( result.hasOwnProperty( 'error' ) ) {
		popMsg( 'Houve um erro ao tentar adicionar o novo perfil, tente novamente ou mais tarde.', 'error' );
	} else {
		popMsg( 'Perfil adicionado com sucesso!', 'success' );
		window.location = result.redirect_to
	}
}

function destroyRole( id ) {
	event.preventDefault();
	$.ajax({
        type: 'DELETE',
        url: '/roles/' + id,
        dataType: 'json',
        data: {
            '_method' : 'destroy',
            'authenticity_token' : AUTH_TOKEN
        },
        success: function( property ){
        	popMsg( 'Perfil de usuário removido com sucesso', 'success' );
        	window.location = $( '#current_url' ).val();
        },
        error: function( message ) {
        	modalError( 'Erro ao tentar deletar perfil.', 'Não foi possível executar esta ação, verifique sua conexão e tente novamente.' );
        }
    });
}