function timeLineRefresh() {
	$.ajax({
        type: "GET",
        url: '/timelines/',
        dataType: "html",
        data: {
            'authenticity_token': AUTH_TOKEN,
            'user_id': $( '#customer-id' ).val()
        },
        success: function( html ){
        	$( '#timeline' ).html( html )
        },
        error: function( errors ) {
           modalError( 'Erro ao tentar atualizar!', 'Não foi possível atualizar a timeline. Tente recarregar a página.' );
        }
    });
}

$( document ).on( 'timeLineRefresh', timeLineRefresh );