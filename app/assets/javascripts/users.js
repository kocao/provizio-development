( function( $ ){
	window.destroyUser = function( id ) {
		event.preventDefault();
		$.ajax({
	        type: 'DELETE',
	        url: '/users/' + id,
	        dataType: 'json',
	        data: {
	            '_method' : 'destroy',
	            'authenticity_token' : AUTH_TOKEN
	        },
	        success: function( user ){
	        	popMsg( 'Usuário removido com sucesso', 'success' );
	        	$( '#user-item-' + user.id ).fadeOut( 'fast' );
	        },
	        error: function( message ) {
	        	modalError( 'Erro ao tentar remover usuário.', 'Não foi possível executar esta ação, verifique sua conexão e tente novamente.' );
	        }
	    });
	}
})( jQuery );