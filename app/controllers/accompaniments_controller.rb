class AccompanimentsController < ApplicationController

	def create
		@accompaniment = Accompaniment.new( accompaniment_params )
    	if @accompaniment.save
      		render json: { message: 'success', id: @accompaniment.id }, :status => 200
    	else
      		render json: { error: @accompaniment.errors.full_messages.join( ',' ) }, :status => 400
    	end
	end

	def destroy
		@accompaniment = Accompaniment.find( params[:id] )
	    if @accompaniment.destroy
      		render json: { message: 'success', id: @accompaniment.id }, :status => 200
    	else
      		render json: { error: @accompaniment.errors.full_messages.join( ',' ) }, :status => 400
    	end
	end

	private

		def accompaniment_params
	    	params.require( :accompaniment ).permit( :property_id, :user_id )
	  	end

end
