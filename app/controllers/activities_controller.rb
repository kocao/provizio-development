class ActivitiesController < ApplicationController

	def create
    	@activity = Activity.new( activity_params )

    	if @activity.save
      		render json: { message: 'success', id: @activity.id, text: @activity.text }, :status => 200, :callback => 'onActivityCreate' 
    	else
      		render json: { error: @activity.errors.full_messages.join( ',' ) }, :status => 400, :callback => 'onActivityCreate' 
    	end
  	end

  	def destroy
		  @activity = Activity.find( params[:id] )
	    if @activity.destroy
      		render json: { message: 'success', id: @activity.id }, :status => 200
    	else
      		render json: { error: @activity.errors.full_messages.join( ',' ) }, :status => 400
    	end
	end	

	private
  	
	  	def activity_params
	    	params.require( :activity ).permit( :user_id, :property_id, :created_by, :title, :text, :scheduled_to, :warn )
	  	end

end
