class AdminController < ApplicationController

	helper PropertiesHelper
	helper AdminHelper
	helper EnumI18nHelper

	before_action :authenticate_user!
	before_filter :my_auth_filter

	before_action :set_filters, only: [:deals]

	def set_filters
        @filters = {
            'Tipo': 'kind',
            'Status': 'status'
        }
        @deal_statuses = {
            'Aprovado': 'finished',
            'Contra Proposta': 'counter',
            'Em andamento': 'waiting_customer',
            'Reservado': 'reserved'
        }

        @realtors = User.where( role_id: 2)
        @realtors = @realtors.map {|p| [ p.name, p.id ] }
        @investors = User.where( role_id: 3)
        @investors = @investors.map {|p| [ p.name, p.id ] }
        @construction_companies = User.where( role_id: 4)
        @construction_companies = @construction_companies.map {|p| [ p.name, p.id ] }
        @filters['Consultor'] = 'realtor'
        @filters['Investidor'] = 'investor'
        @filters['Parceiro'] = 'construction_company'

        @form_url = admin_deals_url
    end

	def index
		@owners = User.where( role: 4 ).order( 'name asc' ).collect {|p| [ p.name, p.id ] }
		@properties = Property.includes(:views).page( params[:page] ).order( 'name asc' )
		@kinds = PropertyKind.all.order( display_name: :asc ).collect {|p| [ p.display_name, p.id ] }
		if params.has_key?( :search )
			if params.has_key?( :q ) && ! params[:q].empty?
				@properties = @properties.search( params[:q] )
			end
			if params.has_key?( :kind ) && ! params[:kind].empty?
				@properties = @properties.where( kind: params[:kind] )
			end
			if params.has_key?( :status ) && ! params[:status].empty?
				@properties = @properties.where( status: params[:status] )
			end
			if params.has_key?( :owner ) && ! params[:owner].empty?
				@properties = @properties.where( user_id: params[:owner] )
			end
		end
	end

	def deals
        @kinds = PropertyKind.all.order( display_name: :asc ).collect {|p| [ p.display_name, p.id ] }


        @cards = []

        #Propostas enviadas
        card = {
            id: 1,
            title: 'Propostas',
            title_link: '',
            follow_button: false,
            progress_bar: true,
            can_edit: false,
            show_user: true,
            template: 'table',
            icon: 'file-text-o',
            items: []
        }

        deals = Deal.joins( :property ).order( "deals.updated_at DESC" )

        if params.has_key?( :search )
            if params.has_key?( :kind ) && ! params[:kind].empty?
                deals = deals.where( "properties.kind = #{params[:kind]}" )
            end
            if params.has_key?( :status ) && ! params[:status].empty?
                deals = deals.where( status: Deal.statuses[params[:status]] )
            else
                deals = deals.where( status: ['waiting_customer', 'finished', 'reserved', 'counter'] )
            end
            if params.has_key?( :realtor ) && ! params[:realtor].empty?
                deals = deals.where( dealer_id: params[:realtor] )
            end
            if params.has_key?( :investor ) && ! params[:investor].empty?
                deals = deals.where( customer_id: params[:investor] )
            end
            if params.has_key?( :construction_company ) && ! params[:construction_company].empty?
                deals = deals.joins( :property ).where( "properties.user_id = #{params[:construction_company]}" )
            end
            if params.has_key?( :q ) && ! params[:q].empty?
                deals = deals.where( "properties.name ILIKE '%#{params[:q]}%'" )
            end
        else
            deals = deals.where( status: ['waiting_customer', 'finished', 'reserved', 'counter'] )
        end

        deals.each do |deal|
            card[:items].push({
                id: deal.id,
                name: deal.property.name,
                updated_at: deal.updated_at.strftime( "%d/%m/%Y %H:%M" ),
                plant: deal.plant.nil? ? '--' : deal.plant.picture.label,
                spot: deal.plant.nil? ? '--' : deal.spot,
                realtor_name: deal.dealer.name,
                owner_name: deal.property.owner.name,
                 investor_name: deal.customer.name,
                down_payment: deal.down_payment,
                installments: deal.installments,
                status: self.helpers.enum_i18n( Deal, 'status', deal.status ),
                deal_status_url: change_deal_status_url + "?deal_id=#{deal.id}",
                deal_show_url: deal_url( deal ),
                edit_deal_url: edit_deal_url( deal )
            })
        end
        @cards.push( card )
        @cards_paginatable = Kaminari.paginate_array( [], total_count: @cards[0][:items].count ).page( params[:page] ).per( 2 )

    end


	def manage_users
		@users = User.all.order( 'name asc' ).page( params[:page] )
		if params.has_key?( :search )
			if params.has_key?( :q ) && ! params[:q].empty?
				@users = @users.search( params[:q] )
			end
			if params.has_key?( :role ) && ! params[:role].empty?
				@users = @users.where( role_id: params[:role] )
			end
		end
		@roles = Role.registrable.collect {|p| [ p.display_name, p.id ] }
	end

	def configurations
		@roles = Role.registrable.collect {|p| [ p.display_name, p.id ] }
		@role = params.has_key?( :role ) ? Role.find( params[:role] ) : Role.find( 1 )
		@page = Page.find_by_name( :terms )
		@kinds = PropertyKind.all.order( display_name: :asc )
	end

	def edit_pages
		@pages = Page.all
		if params.has_key?( :page)
			@page = Page.find_by_name( params[:page] )
		else
			@page = Page.find_by_name( :home )
		end
	end

	def my_auth_filter
		if current_user.role.name != 'admin'
			redirect_to root_url
		end
	end

	def update_page_attr
		case params[:resource]
			when 'meta'
				@resource = Metum.find( params[:id] )
			when 'contents'
				@resource = Content.find( params[:id] )
			else
				@resource = Page.find( params[:id] )
		end

		if @resource.update_attribute( params[:name], params[:val] )
			render json: { message: 'success', id: @resource.id }, :status => 200
		else
			render json: { error: @resource.errors.full_messages.join( ',' ) }, :status => 400
		end
	end

	def add_page_meta_contents
		metum = Metum.find( params[:meta_ref_id] )
	    new_metum = metum.amoeba_dup
	    if new_metum.save
	    	render json: { message: 'success', id: new_metum.id, tpl: render_to_string( 'admin/pages/_page_meta_row', layout: false, locals: { meta: new_metum } ) }, :status => 200
	    else
	    	render json: { error: new_metum.errors.full_messages.join( ',' ) }, :status => 400
	    end
	end

end
