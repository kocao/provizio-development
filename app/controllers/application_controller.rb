class ApplicationController < ActionController::Base

	#helper ApplicationHelper
	
	protect_from_forgery with: :exception
	before_action :configure_permitted_parameters, if: :devise_controller?

	helper_method :current_user_has_role?

	before_filter :set_counters, :force_investor_realtor, :user_must_fill_profile, :get_push_notifications

	def force_investor_realtor
		roles = ['investor', 'realtor', 'construction_company']
		if  ! current_user.nil? && current_user.role.name == 'investor' && 
			! request.xhr? && params[:action] != 'my_realtor' && current_user.investor_realtor.nil? &&
			! ( params[:controller] == 'devise/sessions' && params[:action] == 'destroy' )
			redirect_to my_realtor_url and return		
		elsif ! current_user.nil? && params[:action] != 'my_realtor' && ! current_user.ready? && roles.include?( current_user.role.name ) && ! request.xhr? && 
			params[:controller] != 'users' && params[:action] != 'edit_profile' && ! ( params[:controller] == 'devise/sessions' && params[:action] == 'destroy' )
			redirect_to edit_profile_url and return
		end
	end

	def user_must_fill_profile
		
	end

	def get_push_notifications
		@push_notifications = 0
		@push_count = 0
		unless  current_user.nil?
			@push_notifications = Notification.where( user_id: current_user.id, visualiazed: [nil, false] ).order( created_at: :desc );
			@push_count = @push_notifications.count
			@push_notifications = Notification.where( user_id: current_user.id ).order( created_at: :desc ).limit(5);
		end
	end

	def set_counters
		@counters = {
			deals: 0,
			deals_finished: 0,
			deals_waiting_customer: 0,
			oportunities: Property.where( visibility: 'visible', status: 'available' ).count,
			customers: 0
		}
		unless  current_user.nil?
			case current_user.role.name
			when 'admin'
					Deal.all.scoping do
						@counters[:deals] = Deal.count
						@counters[:deals_finished] = Deal.where( status: 1 ).count
						@counters[:deals_waiting_customer] = Deal.where( status: 0 ).count
					end
					@counters[:realtors] = User.where( role_id: 2 ).count
					@counters[:investors] = User.where( role_id: 3 ).count
					@counters[:partners] = User.where( role_id: 4 ).count
				when 'investor'
					Deal.where( customer_id: current_user.id ).scoping do
						@counters[:deals] = Deal.count
						@counters[:deals_finished] = Deal.where( status: 1 ).count
						@counters[:deals_waiting_customer] = Deal.where( status: 0 ).count
					end
				when 'realtor'
					Deal.where( dealer_id: current_user.id ).scoping do
						@counters[:deals] = Deal.count
						@counters[:deals_finished] = Deal.where( status: 1 ).count
						@counters[:deals_waiting_customer] = Deal.where( status: 0 ).count
					end
					@counters[:customers] = RealtorCustomer.where( user_id: current_user.id ).count
				when 'construction_company'
					Deal.joins( :property ).where( "properties.user_id = #{current_user.id}" ).scoping do
						@counters[:deals] = Deal.count
						@counters[:deals_waiting_customer] = Deal.where( status: 0 ).count
						@counters[:deals_finished] = Deal.where( status: 1 ).count
					end
			end
		end
	end

 	def after_sign_out_path_for(resource_or_scope)
		new_user_session_path
	end

	def current_user_has_role?( *role_names )
    	current_user && role_names.include?( current_user.role.name )
  	end

	protected

		def configure_permitted_parameters
	    	added_attrs = [:name, :email, :password, :password_confirmation, :remember_me, :role_id]
	    	devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
	    	devise_parameter_sanitizer.permit :account_update, keys: added_attrs
	  	end

end
