class AttachmentsController < ApplicationController
  	
  	def create
    	@attachment = Attachment.new( attachment_params )

    	if @attachment.save
      		render json: { 
              message: 'success', 
              id: @attachment.id, 
              url: @attachment.file.url, 
              url_large: @attachment.file.url(), 
              name: File.basename( @attachment.file.original_filename, ".*" ) 
            }, :status => 200
    	else
      		render json: { error: @attachment.errors.full_messages.join( ',' ) }, :status => 400
    	end
  	end

    def update
      @attachment = Attachment.find( params[:id] )
      if @attachment.update!( attachment_params )
        render json: { message: 'success', id: @attachment.id, url: @attachment.file.url, name: File.basename( @attachment.file.original_filename, ".*" ) }, :status => 200
      else
        render json: { error: @attachment.errors.full_messages.join( ',' ) }, :status => 400
      end
    end

  	def destroy
		  @attachment = Attachment.find( params[:id] )
	    if @attachment.destroy
      		render json: { message: 'success', id: @attachment.id }, :status => 200
    	else
      		render json: { error: @attachment.errors.full_messages.join( ',' ) }, :status => 400
    	end
	end	

	private
  	
  	def attachment_params
    	params.require( :attachment ).permit( :attachable_id, :attachable_type, :property_id, :uploaded_by, :modifier, :file, :sequence )
  	end

end
