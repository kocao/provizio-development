module Autocomplete
  class CitiesController < ApplicationController
    def index
      return render(json: []) if params[:state_id].blank?

      cities = City.where(state_id: params[:state_id]).order_by_name.all
      render json: cities.map { |city| { id: city.id.to_s, text: city.name } }
    end
  end
end
