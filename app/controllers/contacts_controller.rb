class ContactsController < ApplicationController

	helper ApplicationHelper
	before_action :add_data

	def add_data
		@contact = self.helpers.action?( 'edit', 'update' ) ? Contact.find( params[:id] ) : Contact.new
		@cities = self.helpers.action?( 'edit', 'update' ) && ! @contact.state.nil? ? City.where( state: @contact.state.id ) : []
		@states = State.all.order( acronym: :asc )
	end

	def new
	end
	def edit
	end
	def create
		@contact = Contact.new( contact_params )
	    if @contact.save
	    	flash[:user_created] = true
	      	redirect_to my_contacts_url
	    else
    		render template: 'panel/add_contact'
	    end
	end
	def update
	    if @contact.update( contact_params )
	    	flash[:user_edited] = true
	      	redirect_to my_contacts_url
	    else
    		render template: 'panel/edit_contact'
	    end
	end
	def destroy
		@contact = Contact.find( params[:id] )
	    if @contact.destroy
	    	respond_to do |format|
		    	format.html { redirect_to contact_path, flash: { :message => 'Contato removido com sucesso!' } }
		      	format.json { render json: @contact, status: :ok, location: @contact }
		    end
	    else
	    	respond_to do |format|
		      	format.html { redirect_to contact_path, flash: { :warning => 'Falha ao tentar remover contato!' } }
		      	format.json { render json: @contact.errors, status: :unauthorized }
		    end
	    end
	end	

	private

    def contact_params
    	params.require( :contact ).permit( :name, :email, :phone, :cellphone, :avatar, :state_id, :city_id, :address, :bio, :job, :user_id )
    end
end
