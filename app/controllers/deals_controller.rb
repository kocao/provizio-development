class DealsController < ApplicationController

	require 'json'

	helper EnumI18nHelper

	def new
		@deal = Deal.new
		@property = Property.find( params[:property_id] )
		@plants = ImageMap.joins( :picture ).where( "pictures.property_id = #{@property.id} AND kind = 4" ).order( "pictures.label ASC" )
		@spots = {}
		@customers = current_user.role.name == 'realtor' ? current_user.customers.uniq : []
		@plants.each do |plant|
			unless plant.map_data.nil?
			spots = JSON.parse( plant.map_data )
				spots = spots[0]['spots']
				@spots[plant.id] = []
				spots.each do |spot|
					if ! spot['tooltip_content'].has_key?('tooltip_status') || spot['tooltip_content'].has_key?('tooltip_status') && spot['tooltip_content']['tooltip_status'] == 'available'
						@spots[plant.id].push( spot['title'] )
					end
				end
			end
		end
	end

	def edit
		@deal = Deal.find( params[:id] )
		params[:property_id] = @deal.property.id
		@property = @deal.property
		@plants = ImageMap.joins( :picture ).where( "pictures.property_id = #{@property.id} AND kind = 4" ).order( "pictures.label ASC" )
		@spots = {}
		@customers = current_user.role.name == 'realtor' ? current_user.customers.uniq : []
		@plants.each do |plant|
			unless plant.map_data.nil?
			spots = JSON.parse( plant.map_data )
				spots = spots[0]['spots']
				@spots[plant.id] = []
				spots.each do |spot|
					if ! spot['tooltip_content'].has_key?('tooltip_status') || spot['tooltip_content'].has_key?('tooltip_status') && spot['tooltip_content']['tooltip_status'] == 'available'
						@spots[plant.id].push( spot['title'] )
					end
				end
			end
		end
	end

	def show
		@deal = Deal.find( params[:id] )
	end

	def create

		if current_user.role.name == 'realtor'
			unless params[:deal][:customer_id].present?
				#pry.binding
				@customer = User.find_by_email( params[:user_email] )
				if @customer.nil?
					@user_hash = {
		                name: params[:user_name],
		                email: params[:user_email],
		                role_id: 3,
		                cpf: params[:user_cpf],
		                phone: params[:user_phone],
		                password: params[:user_cpf].delete( "^0-9" ),
		                password_confirmation: params[:user_cpf].delete( "^0-9" ),
		                terms: '1'
		            }
					@customer = User.create!( @user_hash )
					RealtorCustomer.create({
						user_id: current_user.id,
						customer_id: @customer.id
					})
					InvestorRealtor.create({
						realtor_id: current_user.id,
						user_id: @customer.id
					})
				end
				params[:deal][:customer_id] = @customer.id
			end
		end
		
		@deal = Deal.new( deals_params )
		
	    if @deal.save
	    	DealMailer.new_deal( @deal ).deliver_now
	    	map_data = {}
	    	if ! @deal.spot.nil? && ! @deal.plant_id.nil?
	    		image_map = ImageMap.find( @deal.plant_id )
	    		#spots = JSON.parse( image_map.map_data )
				#spots[0]['spots'].each_with_index do |spot,i|
				#	if spot['title'] == @deal.spot
				#		spots[0]['spots'][i]['default_style']['fill'] = '#ffe7a5'
				#		spots[0]['spots'][i]['default_style']['fill_opacity'] = 0.9
				#		spots[0]['spots'][i]['mouseover_style']['fill'] = '#ffe7a5'
				#		spots[0]['spots'][i]['tooltip_content']['tooltip_status'] = 'reserved'
				#	end
				#end
				#spots = spots.to_json
				#image_map.update_attribute( 'map_data', spots )
				spots = image_map.update_spot( @deal.spot, 'reserved' )
				map_data = spots
	    	end
	    	respond_to do |format|
			    format.js do
			    	data = {
			    		:id => @deal.id,
			    		:plant_id => @deal.plant_id,
			    		:map_data => map_data
			    	}
			    	render :json => data, :callback => 'onDealCreate' 
			    end 
		    end
	    else
	    	respond_to do |format|
		    	format.js { render :json => { "error": true, "errors": @deal.errors, "formated_message": self.helpers.formated_errors( @deal.errors ) }, status: :unprocessable_entity, :callback => 'onDealCreate' }
		  	end
	    end
	end

	def update

		@deal = Deal.find( params[:id] )

		#pry.binding		
 
	    if @deal.update!( deals_params )
	    	DealMailer.new_deal( @deal ).deliver_now
	    	respond_to do |format|
			    format.js do
			    	data = {
			    		:id => @deal.id,
			    		:status => self.helpers.enum_i18n( Deal, 'status', @deal.status )
			    	}
			    	render :json => data, :callback => 'onDealUpdate' 
			    end 
		    end
	    else
	    	respond_to do |format|
		    	format.js { render :json => { "error": true, "errors": @deal.errors, "formated_message": self.helpers.formated_errors( @deal.errors ) }, status: :unprocessable_entity, :callback => 'onDealUpdate' }
		  	end
	    end
	end

	private 

	def deals_params
    	params.require( :deal ).permit( 
    		:dealer_id, :customer_id, :property_id, :status, :down_payment, :installments, :is_change,
    		:exchange_object, :details, :spot, :plant_id, :payment_method, :indication, :total_value
    		)
  	end

end