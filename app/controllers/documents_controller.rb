class DocumentsController < ApplicationController
	def create
    	@document = Document.new( document_params )
			@document.owner_id = @current_user.id unless @document.owner_id == 0

    	if @document.save
      		render json: {
              message: 'success',
              id: @document.id,
              name: @document.file.name,
              size: ( @document.file.size / 1024 ).to_f ,
              url: @document.file.url( :large ),
              thumb: @document.file.url( :thumb )
            }, :status => 200
    	else
      		render json: { error: @document.errors.full_messages.join( ',' ) }, :status => 400
    	end
  	end

    def update
      @document = Document.find( params[:id] )
      if @document.update!( document_params )
        render json: {
              message: 'success',
              id: @document.id,
              name: @document.file.name,
              size: ( @document.file.size / 1024 ).to_f ,
              url: @document.file.url( :large ),
              thumb: @document.file.url( :thumb )
            }, :status => 200
      else
        render json: { error: @document.errors.full_messages.join( ',' ) }, :status => 400
      end
    end

  	def destroy
		  @document = Document.find( params[:id] )
	    if @document.destroy
      		render json: { message: 'success', id: @document.id }, :status => 200
    	else
      		render json: { error: @document.errors.full_messages.join( ',' ) }, :status => 400
    	end
	end

	private

  	def document_params
    	params.require( :document ).permit( :property_id, :file, :name, :owner_id )
  	end
end
