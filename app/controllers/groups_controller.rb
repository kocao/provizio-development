class GroupsController < ApplicationController
  before_action :set_group, only: [:show, :edit, :update, :destroy]
  before_action :require_permission

  def require_permission
		unless current_user_has_role?( 'construction_company' )
      redirect_to '/'
		end
	end

  # GET /groups
  # GET /groups.json
  def index
    @groups = Group.all
  end

  # GET /groups/1
  # GET /groups/1.json
  def show
    puts params.inspect
    investors_group =  @group.groups_user.collect {|u| u.user_id}
    @tab = 'membros'
    @tab = params['tab'] unless params['tab'].nil?
    @investors = User.investors
    @investors = @investors.where("id not in(?)", investors_group) unless investors_group.blank?
    @invites = Invite.where(group_id: @group.id)
  end

  # GET /groups/new
  def new
    @user_id = current_user.id
    @group = Group.new
    @properties = Property.where(user_id: @user_id)
  end

  # GET /groups/1/edit
  def edit
  end

  # POST /groups
  # POST /groups.json
  def create
    @group = Group.new(group_params)
    @group.user_id = current_user.id
    # @investors = User.investors

    respond_to do |format|
      if @group.save
        format.html { redirect_to @group, notice: 'Group was successfully created.' }
        format.json { render :show, status: :created, location: @group }
      else
        format.html { render :new }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /groups/1
  # PATCH/PUT /groups/1.json
  def update
    respond_to do |format|
      if @group.update(group_params)
        format.html { redirect_to @group, notice: 'Group was successfully updated.' }
        format.json { render :show, status: :ok, location: @group }
      else
        format.html { render :edit }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /groups/1
  # DELETE /groups/1.json
  def destroy
    @group.destroy
    respond_to do |format|
      format.html { redirect_to '/panel/my_ventures', notice: 'Grupo excluído com sucesso.' }
      format.json { head :no_content }
    end
  end

  def add_investor
    @group = Group.find(params['group_id'])
    @group.groups_user.new(user_id: params['investor_id'])
    accompaniment_exists = Accompaniment.where(property_id: @group.property.id, user_id: params['investor_id']).count > 0
    accompaniment = Accompaniment.new(property_id: @group.property.id, user_id: params['investor_id'])

    respond_to do |format|
      if @group.save
        unless accompaniment_exists
          accompaniment.save
        end
        format.html { redirect_to group_url(@group.id) + "?tab=membros", notice: 'Investidor adicionado com sucesso.', tab: 'membros' }
        format.json { render :show, status: :ok, location: @group }
      else
        format.html { render :show }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  def invite_investor
    @group = Group.find(params['group_id'])
    token = Invite.generate_token
    @invite = Invite.new(name: params[:name], email: params[:email],
      group_id: @group.id, token: token)

    respond_to do |format|
      if @invite.save
        format.html { redirect_to group_url(@group.id) + "?tab=convidados", notice: 'Convite enviado com sucesso.', tab: 'convidados' }
        format.json { render :show, status: :ok, location: @group }
      else
        format.html { render :show }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy_investor
    @group_user = GroupsUser.find(params['id'])
    @group = @group_user.group
    accompaniment = Accompaniment.where(property_id: @group.property.id, user_id: @group_user.user.id)

    respond_to do |format|
      if @group_user.destroy
        if accompaniment.count > 0
          accompaniment.first.destroy
        end
        format.html { redirect_to group_url(@group.id) + "?tab=membros", notice: 'Investidor excluído com sucesso.' }
        format.json { render :show, status: :ok, location: @group }
      else
        format.html { render :show }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy_invite
    @invite = Invite.find(params[:id])
    @group = Group.find(@invite.group_id)

    respond_to do |format|
      if @invite.destroy
        format.html { redirect_to group_url(@group.id) + "?tab=convidados", notice: 'Convite excluído com sucesso.' }
        format.json { render :show, status: :ok, location: @group }
      else
        format.html { render :show }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_group
      @group = Group.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def group_params
      params.require(:group).permit(:name, :property_id, :user_id)
    end
end
