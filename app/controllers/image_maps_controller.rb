class ImageMapsController < ApplicationController

	helper PicturesHelper

	def edit
		@imap = ImageMap.find( params[:id] )
	end

	def update
		@imap = ImageMap.find( params[:id] )
	    if @imap.update( image_map_params )
	    	render json: { message: 'success', id: @imap.id }, :status => 200, :callback => 'onImageMapUpdate' 
	    else
	    	render json: { error: @imap.errors.full_messages.join( ',' ) }, :status => 400, :callback => 'onImageMapUpdate' 
	    end
	end

	private

	    def image_map_params
	    	params.permit( :map_data, :available_text )
	    end

end
