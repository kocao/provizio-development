class MessagesController < ApplicationController

	helper MessagesHelper
	helper EnumI18nHelper

	before_action :authenticate_user!
	before_action :set_global_data, only: [:index, :show]
	
	def index
		if params.has_key?( :q )
			@messages = Message.where( user_id: current_user.id ).search( params[:q] ).page( params[:page] )
		end
	end

	def create
		
		attachs = []
		
		if params[:message].has_key?( :attachments_attributes )
			params[:message][:attachments_attributes].each do |attach|
				attachs.push( attach[:id] )
			end
			params[:message][:attachments_attributes] = []
		end

    	@message = Message.new( message_params )

    	if @message.save
    		#Saves the attachment to original image
    		attachs.each do |attach|
    			Attachment.find( attach ).update_attribute( :attachable_id, @message.id )
    		end
    		emails = params[:message][:to].split( ',' )
    		emails.each do |email|
    			user = User.find_by_email( email )
    			unless user.nil?
	    			Message.create({
	    				user_id: user.id,
	    				prev_id: @message.id,
	    				to: 'me', 
	    				from: @message.from,
	    				subject: @message.subject,
	    				text: @message.text,
	    				property_id: @property_id
	    			})
	    		end
    		end
    		unless @message.deal_id.nil?
    			@deal = Deal.find( @message.deal_id )
    			@deal.update_attribute( :status, @message.modifier )
    			spot_status = nil
    			case @message.modifier
    				when 'finished'
    					spot_status = 'sold'
    				when 'refused'
    					spot_status = 'available'
    				when 'counter'
    					spot_status = 'reserved'
    			end   				
    			if ! @deal.spot.nil? && ! @deal.plant_id.nil?	
    				ImageMap.find( @deal.plant_id ).update_spot( @deal.spot, spot_status );
    			end
    		end
	    	respond_to do |format|
			    format.js do
			    	if @message.deal_id.nil?
			    		data = self.helpers.message_data( @message )
			    		render :json => data, :callback => 'onMessageCreate' 
			    	else
			    		data = {
			    			deal_id: @message.deal.id,
			    			deal_status: @message.modifier,
			    			deal_status_text: self.helpers.enum_i18n( Deal, 'status', @message.modifier )
			    		}
			    		render :json => data, :callback => 'onDealMessageCreate' 
			    	end
			    end 
		    end
	    else
	    	respond_to do |format|
		    	format.js { render :json => { "error": true, "errors": @message.errors, "formated_message": self.helpers.formated_errors( @message.errors ) }, status: :unprocessable_entity, :callback => 'onMessageCreate' }
		  	end
	    end


  	end

  	def show
  		@message = Message.find( params[:id] )
  		@message.update_attribute( :read, true )
  		@reply_all = @message.prev.nil? ? @message.from : @message.prev.to.gsub( current_user.email, @message.from )
  		@from = @message.from
		user = User.find_by_email( @message.from )
		unless user.nil?
			@from = user.name + ' - ' + @from
		end
  	end

  	def destroy
		@message = Message.find( params[:id] )
		if params.has_key?( 'skip_trash' ) && params[:skip_trash] == 'true' || @message.box == :trash
			result = @message.destroy
			trashed = false
		else
			result = @message.update_attribute( :box, :trash )
			trashed = true
		end
    	if result
    		data = self.helpers.message_data( @message )
    		data[:message] = 'success'
    		data[:trashed] = trashed
  			render json: data, :status => 200
		else
  			render json: { error: @message.errors.full_messages.join( ',' ) }, :status => 400
		end
	end

  	private

  		def set_global_data
  			@message = Message.new
			@inbox = Message.where( user_id: current_user.id, box: :inbox ).order( created_at: :desc ).page( params[:page] )
			@outbox = nil
			@sent = Message.where( user_id: current_user.id, box: :sent ).order( created_at: :desc ).page( params[:page] )
			@trash = Message.where( user_id: current_user.id, box: :trash ).order( created_at: :desc ).page( params[:page] )
			@field = {}
			@field[:to] = ''
			if params.has_key?( 'mail_template' )
				property = Property.find( params[:property_id] )
				if params[:mail_template] == 'send'
					@field[:subject] = 'Veja este imóvel - ' + property.name
					@field[:text] = '<p>Encontrei um imóvel que você pode se interessar. Detalhes abaixo:</p>'.html_safe + property.info_html
				end
				if params[:mail_template] == 'proposal'
					@field[:subject] = 'Me interessei pelo imóvel - ' + property.name
					@field[:text] = '<p>Gostaria de mais informações sobre o imóvel abaixo:</p>'.html_safe + property.info_html
				end
			end
  		end 
	  	
	  	def message_params
	    	params.require( :message ).permit( :user_id, :to, :from, :subject, :text, :box, :read, :read_at, :property_id, :deal_id, :modifier, attachments_attributes: [:id, :attachable_type, :attachable_id, :file, :modifier, :sequence]
	    	)
	  	end

end
