class MetaController < ApplicationController
	def destroy
		@metum = Metum.find( params[:id] )
	    if @metum.destroy
      		render json: { message: 'success', id: @metum.id }, :status => 200
    	else
      		render json: { error: @metum.errors.full_messages.join( ',' ) }, :status => 400
    	end
    end
end
