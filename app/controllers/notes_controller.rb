class NotesController < ApplicationController

	def create
    	@note = Note.new( note_params )

    	if @note.save
      		render json: { message: 'success', id: @note.id, text: @note.text }, :status => 200, :callback => 'onNoteCreate' 
    	else
      		render json: { error: @note.errors.full_messages.join( ',' ) }, :status => 400, :callback => 'onNoteCreate' 
    	end
  	end

  	def destroy
		  @note = Note.find( params[:id] )
	    if @note.destroy
      		render json: { message: 'success', id: @note.id }, :status => 200
    	else
      		render json: { error: @note.errors.full_messages.join( ',' ) }, :status => 400
    	end
	end	

	private
  	
	  	def note_params
	    	params.require( :note ).permit( :user_id, :property_id, :created_by, :text )
	  	end

end
