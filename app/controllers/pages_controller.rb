class PagesController < ApplicationController

	helper PagesHelper

	def index
		@page = Page.find_by_name( :home )
	end

	def about
		@page = Page.find_by_name( :about )
	end

	def partner
		@page = Page.find_by_name( :partner )
	end

	def realtor
		@page = Page.find_by_name( :realtor )
	end

	def investor
		@page = Page.find_by_name( :investor )
	end

	def pages_params
		params.require( :page ).permit( 
			:name, :title, :description, :keywords, 
			meta_attributes: [:id, :page_id, :name, :value, :order, :_destroy, 
			  contents_attributes: [:id, :metum_id, :name, :value, :order, :_destroy]
			] 
		)
	end

end
