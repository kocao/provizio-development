class PanelController < ApplicationController

    before_action :add_data 
    before_action :authenticate_user!, except: [:public_profile]
    before_action :set_filters, only: [:my_deals, :my_ventures]

    helper PropertiesHelper
    helper EnumI18nHelper

    def add_data
      @map_api_key = 'AIzaSyC-jjFQQSwpaeLKD2njDbIbUETRYnrYNl0'
      @reservation_times = [3,5,7]
    end

  	def index
  	end

    def set_filters
        @filters = {
            'Tipo': 'kind',
            'Status': 'status'
        }
        @deal_statuses = {
            'Aprovado': 'finished',
            'Contra Proposta': 'counter',
            'Em andamento': 'waiting_customer',
            'Reservado': 'reserved'
        }
        if current_user.role.name == 'investor'
            @realtors = Deal.select( :dealer_id ).where( customer_id: current_user.id, status: [:waiting_customer, :finished] ).group( :dealer_id )
            @realtors = @realtors.map {|p| [ p.dealer.name, p.dealer.id ] }
            @construction_companies = Deal.joins( :property ).select( 'property_id, "properties".user_id' ).where( customer_id: current_user.id, status: [:waiting_customer, :finished] ).group( 'property_id, "properties".user_id' )
            @construction_companies = @construction_companies.map {|p| [ p.property.owner.name, p.property.owner.id ] }
            @investors = []
            @filters['Consultor'] = 'realtor'
            @filters['Parceiro'] = 'construction_company'
        elsif current_user.role.name == 'realtor'
            @realtors = []
            @investors = Deal.select( :customer_id ).where( dealer_id: current_user.id, status: [:waiting_customer, :finished] ).group( :customer_id )
            @investors = @investors.map {|p| [ p.customer.name, p.customer.id ] }
            @construction_companies = Deal.joins( :property ).select( 'property_id, "properties".user_id' ).where( dealer_id: current_user.id, status: [:waiting_customer, :finished] ).group( 'property_id, "properties".user_id' )
            @construction_companies = @construction_companies.map {|p| [ p.property.owner.name, p.property.owner.id ] }
            @filters['Parceiro'] = 'construction_company'
            @filters['Investidor'] = 'investor'
        elsif current_user.role.name == 'construction_company'
            @realtors = Deal.joins( :property ).select( :dealer_id ).where( "properties.user_id = #{current_user.id}" ).group( :dealer_id )
            @realtors = @realtors.map {|p| [ p.dealer.name, p.dealer.id ] }
            @construction_companies = []
            @investors = Deal.joins( :property ).select( :customer_id ).where( "properties.user_id = #{current_user.id}" ).group( :customer_id )
            @investors = @investors.map {|p| [ p.customer.name, p.customer.id ] }
            @filters['Consultor'] = 'realtor'
            @filters['Investidor'] = 'investor'
        end
        @realtors = @realtors.uniq
        @investors = @investors.uniq
        @construction_companies = @construction_companies.uniq
        @form_url = current_user.role.name == 'construction_company' ? my_ventures_url : my_deals_url
    end

    def view_docs
        @documents = Document.where( property_id: params[:property_id] )
    end

    def notify_gallery
        PropertyMailer::new_stage_pictures( Property.find( params[:property_id] ) ).deliver_now
    end

    def choose_role
        if [2,3,4].include?( params[:role].to_i ) && current_user.update_attribute( :role_id, params[:role] )
            render json: { message: 'success' }, :status => 200
        else
            render json: { error: true }, :status => 400
        end
    end

    def first_login
        @page = Page.find_by_name( :home )
    end

  	def my_clients
	  	if params.has_key?( :char )
	  		@customers = current_user.customers.where( "users.name LIKE '#{params[:char]}%'" ).order( name: :asc ).page( params[:page] )
	  	elsif params.has_key?( :q )
	  		@customers = current_user.customers.where( "users.name LIKE '%#{params[:q]}%' OR users.email LIKE '%#{params[:q]}%'" ).order( name: :asc ).page( params[:page] )
	  	else
	  		@customers = current_user.customers.order( name: :asc ).page( params[:page] )
	  	end
        @customers = @customers.group( "users.id" )
  	end

  	def my_contacts
	  	if params.has_key?( :char )
	  		@contacts = current_user.contacts.where( "contacts.name LIKE '#{params[:char]}%'" ).order( name: :asc ).page( params[:page] )
	  	elsif params.has_key?( :q )
	  		@contacts = current_user.contacts.where( "contacts.name LIKE '%#{params[:q]}%' OR contacts.email LIKE '%#{params[:q]}%'" ).order( name: :asc ).page( params[:page] )
	  	else
	  		@contacts = current_user.contacts.order( name: :asc ).page( params[:page] )
	  	end
  	end


    def my_deals
        @kinds = PropertyKind.all.order( display_name: :asc ).collect {|p| [ p.display_name, p.id ] }
        if current_user.role.name == '_realtor'

            @cards = []

            #Propostas enviadas
            card = {
                id: 1,
                title: 'Suas propostas',
                title_link: '',
                follow_button: false,
                progress_bar: true,
                can_edit: false,
                show_user: true,
                template: 'table',
                icon: 'file-text-o',
                items: []
            }

            deals = Deal.where( dealer_id: current_user.id ).joins( :property ).order( "deals.updated_at DESC" )

            if params.has_key?( :search )
                if params.has_key?( :kind ) && ! params[:kind].empty?
                    deals = deals.where( "properties.kind = #{params[:kind]}" )
                end
                if params.has_key?( :status ) && ! params[:status].empty?
                    deals = deals.where( status: Deal.statuses[params[:status]] )
                else
                    deals = deals.where( status: ['waiting_customer', 'finished', 'reserved', 'counter'] )
                end
                if params.has_key?( :realtor ) && ! params[:realtor].empty?
                    deals = deals.where( dealer_id: params[:realtor] )
                end
                if params.has_key?( :investor ) && ! params[:investor].empty?
                    deals = deals.where( customer_id: params[:investor] )
                end
                if params.has_key?( :construction_company ) && ! params[:construction_company].empty?
                    deals = deals.joins( :property ).where( "properties.user_id = #{params[:construction_company]}" )
                end
                if params.has_key?( :q ) && ! params[:q].empty?
                    deals = deals.where( "properties.name ILIKE '%#{params[:q]}%'" )
                end
            else
                deals = deals.where( status: ['waiting_customer', 'finished', 'reserved', 'counter'] )
            end

            deals.each do |deal|
                card[:items].push({
                    id: deal.id,
                    name: deal.property.name,
                    updated_at: deal.updated_at.strftime( "%d/%m/%Y %H:%M" ),
                    plant: deal.plant.nil? ? '--' : deal.plant.picture.label,
                    spot: deal.plant.nil? ? '--' : deal.spot,
                    realtor_name: deal.dealer.name,
                    owner_name: deal.property.owner.name,
                     investor_name: deal.customer.name,
                    down_payment: deal.down_payment,
                    installments: deal.installments,
                    status: self.helpers.enum_i18n( Deal, 'status', deal.status ),
                    deal_status_url: change_deal_status_url + "?deal_id=#{deal.id}",
                    deal_show_url: deal_url( deal ),
                    edit_deal_url: edit_deal_url( deal )
                })
            end
            @cards.push( card )

        elsif current_user.role.name == 'realtor'

            @cards = {
                reserved: [],
                waiting_customer: [],
                finished: []
            }

            card = {
                id: 1,
                title: 'Reservado',
                title_link: '',
                follow_button: false,
                progress_bar: true,
                can_edit: false,
                show_user: true,
                template: 'table',
                icon: 'file-text-o',
                items: []
            }

            deals = Deal.where( dealer_id: current_user.id, status: [:reserved] ).joins( :property ).order( "deals.updated_at DESC" )
            if params.has_key?( :search )
                if params.has_key?( :kind ) && ! params[:kind].empty?
                    deals = deals.where( "properties.kind = #{params[:kind]}" )
                end
                if params.has_key?( :status ) && ! params[:status].empty?
                    deals = deals.where( status: Deal.statuses[params[:status]] )
                end
                if params.has_key?( :realtor ) && ! params[:realtor].empty?
                    deals = deals.where( dealer_id: params[:realtor] )
                end
                if params.has_key?( :investor ) && ! params[:investor].empty?
                    deals = deals.where( customer_id: params[:investor] )
                end
                if params.has_key?( :construction_company ) && ! params[:construction_company].empty?
                    deals = deals.joins( :property ).where( "properties.user_id = #{params[:construction_company]}" )
                end
                if params.has_key?( :q ) && ! params[:q].empty?
                    deals = deals.where( "properties.name ILIKE '%#{params[:q]}%'" )
                end
            end

            deals.each do |deal|
                card[:items].push({
                    id: deal.id,
                    name: deal.property.name,
                    updated_at: deal.updated_at.strftime( "%d/%m/%Y %H:%M" ),
                    plant: deal.plant.nil? ? '--' : deal.plant.picture.label,
                    spot: deal.plant.nil? ? '--' : deal.spot,
                    realtor_name: deal.dealer.name,
                    owner_name: deal.property.owner.name,
                    investor_name: deal.customer.name,
                    down_payment: deal.down_payment,
                    installments: deal.installments,
                    status: self.helpers.enum_i18n( Deal, 'status', deal.status ),
                    deal_status_url: change_deal_status_url + "?deal_id=#{deal.id}",
                    deal_show_url: deal_url( deal ),
                    edit_deal_url: edit_deal_url( deal )
                })
            end

            @cards[:reserved].push( card )

            card = {
                id: 2,
                title: 'Em andamento',
                title_link: '',
                follow_button: false,
                progress_bar: true,
                can_edit: false,
                show_user: true,
                template: 'table',
                icon: 'file-text-o',
                items: []
            }

            deals = Deal.where( dealer_id: current_user.id, status: [:waiting_customer, :counter] ).joins( :property ).order( "deals.updated_at DESC" )
            if params.has_key?( :search )
                if params.has_key?( :kind ) && ! params[:kind].empty?
                    deals = deals.where( "properties.kind = #{params[:kind]}" )
                end
                if params.has_key?( :status ) && ! params[:status].empty?
                    deals = deals.where( status: Deal.statuses[params[:status]] )
                end
                if params.has_key?( :realtor ) && ! params[:realtor].empty?
                    deals = deals.where( dealer_id: params[:realtor] )
                end
                if params.has_key?( :investor ) && ! params[:investor].empty?
                    deals = deals.where( customer_id: params[:investor] )
                end
                if params.has_key?( :construction_company ) && ! params[:construction_company].empty?
                    deals = deals.joins( :property ).where( "properties.user_id = #{params[:construction_company]}" )
                end
                if params.has_key?( :q ) && ! params[:q].empty?
                    deals = deals.where( "properties.name ILIKE '%#{params[:q]}%'" )
                end
            end

            deals.each do |deal|
                card[:items].push({
                    id: deal.id,
                    name: deal.property.name,
                    updated_at: deal.updated_at.strftime( "%d/%m/%Y %H:%M" ),
                    plant: deal.plant.nil? ? '--' : deal.plant.picture.label,
                    spot: deal.plant.nil? ? '--' : deal.spot,
                    realtor_name: deal.dealer.name,
                    owner_name: deal.property.owner.name,
                    investor_name: deal.customer.name,
                    down_payment: deal.down_payment,
                    installments: deal.installments,
                    status: self.helpers.enum_i18n( Deal, 'status', deal.status ),
                    deal_status_url: change_deal_status_url + "?deal_id=#{deal.id}",
                    deal_show_url: deal_url( deal ),
                    edit_deal_url: edit_deal_url( deal )
                })
            end

            @cards[:waiting_customer].push( card )

            card = {
                id: 3,
                title: 'Finalizado',
                title_link: '',
                follow_button: false,
                progress_bar: true,
                can_edit: false,
                show_user: true,
                template: 'table',
                icon: 'file-text-o',
                items: []
            }

            deals = Deal.where( dealer_id: current_user.id, status: [:finished] ).joins( :property ).order( "deals.updated_at DESC" )
            if params.has_key?( :search )
                if params.has_key?( :kind ) && ! params[:kind].empty?
                    deals = deals.where( "properties.kind = #{params[:kind]}" )
                end
                if params.has_key?( :status ) && ! params[:status].empty?
                    deals = deals.where( status: Deal.statuses[params[:status]] )
                end
                if params.has_key?( :realtor ) && ! params[:realtor].empty?
                    deals = deals.where( dealer_id: params[:realtor] )
                end
                if params.has_key?( :investor ) && ! params[:investor].empty?
                    deals = deals.where( customer_id: params[:investor] )
                end
                if params.has_key?( :construction_company ) && ! params[:construction_company].empty?
                    deals = deals.joins( :property ).where( "properties.user_id = #{params[:construction_company]}" )
                end
                if params.has_key?( :q ) && ! params[:q].empty?
                    deals = deals.where( "properties.name ILIKE '%#{params[:q]}%'" )
                end
            end

            deals.each do |deal|
                card[:items].push({
                    id: deal.id,
                    name: deal.property.name,
                    updated_at: deal.updated_at.strftime( "%d/%m/%Y %H:%M" ),
                    plant: deal.plant.nil? ? '--' : deal.plant.picture.label,
                    spot: deal.plant.nil? ? '--' : deal.spot,
                    realtor_name: deal.dealer.name,
                    owner_name: deal.property.owner.name,
                    investor_name: deal.customer.name,
                    down_payment: deal.down_payment,
                    installments: deal.installments,
                    status: self.helpers.enum_i18n( Deal, 'status', deal.status ),
                    deal_status_url: change_deal_status_url + "?deal_id=#{deal.id}",
                    deal_show_url: deal_url( deal ),
                    edit_deal_url: edit_deal_url( deal )
                })
            end

            @cards[:finished].push( card )

        else

            @cards = []

            card = {
                id: 1,
                title: 'Suas propostas',
                title_link: '',
                follow_button: false,
                progress_bar: true,
                can_edit: false,
                show_user: true,
                template: 'table',
                icon: 'file-text-o',
                items: []
            }

            deals = Deal.where( customer_id: current_user.id, status: [:reserved, :waiting_customer] ).joins( :property ).order( "deals.updated_at DESC" )
            if params.has_key?( :search )
                if params.has_key?( :kind ) && ! params[:kind].empty?
                    deals = deals.where( "properties.kind = #{params[:kind]}" )
                end
                if params.has_key?( :status ) && ! params[:status].empty?
                    deals = deals.where( status: Deal.statuses[params[:status]] )
                end
                if params.has_key?( :realtor ) && ! params[:realtor].empty?
                    deals = deals.where( dealer_id: params[:realtor] )
                end
                if params.has_key?( :investor ) && ! params[:investor].empty?
                    deals = deals.where( customer_id: params[:investor] )
                end
                if params.has_key?( :construction_company ) && ! params[:construction_company].empty?
                    deals = deals.joins( :property ).where( "properties.user_id = #{params[:construction_company]}" )
                end
                if params.has_key?( :q ) && ! params[:q].empty?
                    deals = deals.where( "properties.name ILIKE '%#{params[:q]}%'" )
                end
            end

            deals.each do |deal|
                card[:items].push({
                    id: deal.id,
                    name: deal.property.name,
                    updated_at: deal.updated_at.strftime( "%d/%m/%Y %H:%M" ),
                    plant: deal.plant.nil? ? '--' : deal.plant.picture.label,
                    spot: deal.plant.nil? ? '--' : deal.spot,
                    realtor_name: deal.dealer.name,
                    owner_name: deal.property.owner.name,
                    investor_name: deal.customer.name,
                    down_payment: deal.down_payment,
                    installments: deal.installments,
                    status: self.helpers.enum_i18n( Deal, 'status', deal.status ),
                    deal_status_url: change_deal_status_url + "?deal_id=#{deal.id}",
                    deal_show_url: deal_url( deal ),
                    edit_deal_url: edit_deal_url( deal )
                })
            end

            @cards.push( card )

            @deals = Deal.where( customer_id: current_user.id, status: :finished ).joins( :property ).order( "properties.name ASC, deals.spot ASC" )
            @boxes = {}
            @deals.each do |deal|
                key = deal.property_id.to_s
                unless @boxes.has_key?( key )
                    @boxes[key] = {
                        property: deal.property,
                        deals: []
                    }
                end
                @boxes[key][:deals].push(deal)
            end
        end
    end

    def my_ventures
        @groups = Group.paginate(:page => params[:page], :per_page => 5)
        # @posts = Post.paginate(:page => params[:page])

        @cards = []
        @kinds = PropertyKind.all.order( display_name: :asc ).collect {|p| [ p.display_name, p.id ] }
        #Propostas enviadas
        card = {
            id: 1,
            title: 'Propostas',
            title_link: '',
            follow_button: false,
            progress_bar: true,
            can_edit: false,
            show_user: true,
            template: 'table',
            icon: 'file-text-o',
            items: []
        }

        deals = Deal.joins( :property ).where( "properties.user_id = #{current_user.id}" ).order( "deals.updated_at DESC" )
        if params.has_key?( :search )
            if params.has_key?( :kind ) && ! params[:kind].empty?
                deals = deals.where( "properties.kind = #{params[:kind]}" )
            end
            if params.has_key?( :status ) && ! params[:status].empty?
                deals = deals.where( status: Deal.statuses[params[:status]] )
            else
                deals = deals.where( status: ['waiting_customer', 'finished', 'reserved', 'counter'] )
            end
            if params.has_key?( :realtor ) && ! params[:realtor].empty?
                deals = deals.where( dealer_id: params[:realtor] )
            end
            if params.has_key?( :investor ) && ! params[:investor].empty?
                deals = deals.where( customer_id: params[:investor] )
            end
            if params.has_key?( :construction_company ) && ! params[:construction_company].empty?
                deals = deals.joins( :property ).where( "properties.user_id = #{params[:construction_company]}" )
            end
            if params.has_key?( :q ) && ! params[:q].empty?
                deals = deals.where( "properties.name ILIKE '%#{params[:q]}%'" )
            end
        else
            deals = deals.where( status: ['waiting_customer', 'finished', 'reserved', 'counter'] )
        end

        deals.each do |deal|
            card[:items].push({
                id: deal.id,
                name: deal.property.name,
                updated_at: deal.updated_at.strftime( "%d/%m/%Y %H:%M" ),
                plant: deal.plant.nil? ? '--' : deal.plant.picture.label,
                spot: deal.plant.nil? ? '--' : deal.spot,
                realtor_name: deal.dealer.name,
                owner_name: deal.property.owner.name,
                investor_name: deal.customer.name,
                down_payment: deal.down_payment,
                installments: deal.installments,
                status: self.helpers.enum_i18n( Deal, 'status', deal.status ),
                deal_status_url: change_deal_status_url + "?deal_id=#{deal.id}",
                deal_show_url: deal_url( deal ),
                edit_deal_url: edit_deal_url( deal )
            })
        end
        @cards.push( card )

    end

    def change_deal_status

        @deal = Deal.find( params[:deal_id] )

        @field = {}

        #@field[:to] = @deal.customer.email + "," + @deal.dealer.email
        @field[:to] = @deal.customer.email
        @field[:modifier] = params[:status]

        case params[:status]
            when 'finished'
                @field[:subject] = 'Parabéns! Sua proposta foi aprovada.'
                @field[:text] = "É com grande satisfação que comunicamos que a proposta para o empreendimento #{@deal.property.name} foi aprovada."
            when 'refused'
                @field[:subject] = 'Sua proposta foi recusada.'
                @field[:text] = "Olá #{@deal.customer.name}, infelizmente sua proposta para o empreendimento #{@deal.property.name} não foi aprovada"
            when 'counter'
                @field[:subject] = 'Temos uma contra proposta para você'
                @field[:text] = "Olá #{@deal.customer.name}, segue uma contra proposta referente ao empreendimento #{@deal.property.name}:"
        end

    end

    def mark_notifications_as_visualiazed
        if Notification.where( user_id: current_user.id ).update_all( visualiazed: true )
           render json: { message: 'success' }, :status => 200
        else
            render json: { error: true }, :status => 400
        end
    end

  	def my_profile
      render 'my_profile'
  	end

  	def my_profile_cc
      @user = User.find(params['id'])
      @sliders = {}
      @sliders.merge!(slider_l: Property.sliders_by_type("L", @user.id))
      @sliders.merge!(slider_a: Property.sliders_by_type("A", @user.id))
      @sliders.merge!(slider_p: Property.sliders_by_type("P", @user.id))

      render 'my_profile_cc'
  	end

    def public_profile
      @user = User.find(params['id'])
      @sliders = {}
      @sliders.merge!(slider_l: Property.sliders_by_type("L", @user.id))
      @sliders.merge!(slider_a: Property.sliders_by_type("A", @user.id))
      @sliders.merge!(slider_p: Property.sliders_by_type("P", @user.id))

      render 'my_profile_cc'
    end

    def notifications
        today = Date.current
        Notification.where( user_id: current_user.id ).order( created_at: :desc ).scoping do
            if params.has_key?( 'period' )
                case params[:period]
                when 'this_week'
                    @notifications = Notification.where( created_at: today.beginning_of_week..today.end_of_week )
                when 'this_month'
                    @notifications = Notification.where( created_at: today.beginning_of_month..today.end_of_month )
                when 'this_year'
                    @notifications = Notification.where( created_at: today.beginning_of_year..today )
                when 'today'
                    @notifications = Notification.where( created_at: today.beginning_of_day..today.end_of_day )
                when 'single'
                    @notifications = Notification.where( id: params[:notification] )
                else
                    @notifications = Notification.all
                end
            end
        end
    end

    def customer_activities
        @customer = User.find( params[:id] )
        @message = Message.new
        @note = Note.new
        @activity = Activity.new
        @note_for = @customer.id
        @activity_for = @customer.id
        @field = {}
        @field[:to] = @customer.email
        @attachable_id = @customer.id
        @attachable_type = 'User'
        @timeline_entries = Timeline.where( user_id: @customer.id ).order( created_at: :desc ).all
    end

  	def my_realtor
  		@realtor = InvestorRealtor.where( user_id: current_user.id )
        if @realtor.empty? || params[:change_realtor]
            @realtors = User.where( role: 2 ).order( name: :asc ).page( params[:page] )
            if params.has_key?( :q )
                @realtors = @realtors.search( params[:q] )
            end
            @my_realtor = @realtor.empty? ? 0 : @realtor.first.realtor.id
            @redirect = @my_realtor == 0 ? root_url : my_realtor_url
            @template = 'change_realtor'
        else
            @realtor = @realtor.first.realtor
      		@message = Message.new
      		@field = {}
    		@field[:to] = @realtor.email
            @template = 'investor_realtor'
        end
  	end

    def search_results
        @results = []
        @properties = Property.where( visibility: :visible, status: :available ).search( params[:q] )
        @messages = Message.where( user_id: current_user.id ).search( params[:q] )
        @contacts = []
        @customers = []
        if current_user_has_role?( 'realtor' )
            #@contacts = current_user.contacts.search( params[:q] )
            @customers = current_user.customers.search( params[:q] )
        end
        @results = @properties + @customers + @contacts + @messages
        @results = Kaminari.paginate_array( @results ).page( params[:page] ).per( 25 )
    end

  	def change_realtor
        my_current_realtor = InvestorRealtor.where( user_id: current_user.id )
        unless my_current_realtor.empty?
            InvestorRealtor.destroy( my_current_realtor.first.id )
        end
        realtor = InvestorRealtor.create!({user_id: current_user.id, realtor_id: params[:id]})
        if realtor
            render json: { message: 'success', id: realtor.id }, :status => 200
        else
            render json: { error: realtor.errors.full_messages.join( ',' ) }, :status => 400
        end
  	end

    def edit_profile
        @user = current_user
        @states = State.all.order( acronym: :asc )
        @cities =  ! @user.state.nil? ? City.where( state: @user.state.id ) : []
        @password = @user.password
        @roles = Role.order( 'name asc' ).collect {|p| [ p.display_name, p.id ] }
    end

    def edit_profile_cc

        @user = current_user
        @pictures = @user.pictures
        @states = State.all.order( acronym: :asc )
        @cities =  ! @user.state.nil? ? City.where( state: @user.state.id ) : []
        @password = @user.password
        @roles = Role.order( 'name asc' ).collect {|p| [ p.display_name, p.id ] }
    end

  	def add_contact
  		@contact = Contact.new
  		@states = State.all.order( acronym: :asc )
  		@cities = []
  	end

  	def edit_contact
  		@contact = Contact.find( params[:id] )
		@states = State.all.order( acronym: :asc )
  		@cities =  ! @contact.state.nil? ? City.where( state: @contact.state.id ) : []
  	end

  	def remove_contact
  		@contact = Contact.find( params[:id] )
	    if @contact.destroy
      		render json: { message: 'success', id: @contact.id }, :status => 200
    	else
      		render json: { error: @contact.errors.full_messages.join( ',' ) }, :status => 400
    	end
  	end

end
