class PicturesController < ApplicationController

  	def create
    	@picture = Picture.new( picture_params )
      @picture.user_id = current_user.id if picture_params['property_id'] == "undefined"

    	if @picture.save
      		render json: { message: 'success', id: @picture.id, url: @picture.file.url( :large ), thumb: @picture.file.url( :thumb ), label: @picture.label }, :status => 200
    	else
      		render json: { error: @picture.errors.full_messages.join( ',' ) }, :status => 400
    	end
  	end

    def update
      @picture = Picture.find( params[:id] )
      if @picture.update!( picture_params )
        render json: { message: 'success', id: @picture.id, url: @picture.file.url( :large ), thumb: @picture.file.url( :thumb ) }, :status => 200
      else
        render json: { error: @picture.errors.full_messages.join( ',' ) }, :status => 400
      end
    end

  	def destroy
		  @picture = Picture.find( params[:id] )
	    if @picture.destroy
      		render json: { message: 'success', id: @picture.id }, :status => 200
    	else
      		render json: { error: @picture.errors.full_messages.join( ',' ) }, :status => 400
    	end
	end

	private

  	def picture_params
    	params.require( :picture ).permit( :property_id, :file, :kind, :sequence, :label )
  	end

end
