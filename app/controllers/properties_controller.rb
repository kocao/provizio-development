class PropertiesController < ApplicationController

	helper UsersHelper
	helper PropertiesHelper
	helper EnumI18nHelper

	before_action :add_data, :authenticate_user!
	before_filter :require_permission, only: :edit

	def add_data
		@map_api_key = 'AIzaSyC-jjFQQSwpaeLKD2njDbIbUETRYnrYNl0'
		@reservation_times = [3,5,7]
	end

	def require_permission
		if current_user_has_role?( 'admin' )
			return
		end
		if current_user.id != Property.find( params[:id] ).user_id
			redirect_to root_path
		end
	end

	def index
		@sliders = []
		query_limit = params.has_key?( :filter_by ) ? 999999 : 10
		case current_user.role.name
		when 'realtor'
			#Pré lançamentos
			slide = {
				name: 'stage_0',
				title: 'Pré-lançamentos',
				title_link: '',
				follow_button: true,
				progress_bar: true,
				can_edit: false,
				show_user: false,
				items: []
			}
			if params.has_key?( :q )
				properties = Property.where( visibility: 'visible', status: 'available', stage: 0 ).search( params[:q] ).order( 'id desc' )
			else
				properties = Property.where( visibility: 'visible', status: 'available', stage: 0 ).order( 'id desc' ).limit( query_limit )
			end
			properties.each do |property|
				image = property.pictures.find_by_kind( :cover )
				image = image.nil? ? self.helpers.missing_img_url : property.pictures.find_by_kind( :cover ).file.url( :thumb )
				accompaniment = Accompaniment.where( user_id: current_user.id, property_id: property.id ).first
				slide[:items].push({
					id: property.id,
					name: property.name,
					stage: property.stage,
					stage_percent: "#{property.stage}%",
					image_src: image,
					following: ! accompaniment.nil?,
					accompaniment_id: accompaniment.nil? ? '' : accompaniment.id,
					user_src: '',
					url: property_path( property.id ),
					edit_url: edit_property_path( property.id )
				})
			end
			@sliders.push( slide )
			#Em andamento
			slide = {
				name: 'stage_1_99',
				title: 'Em andamento',
				title_link: '',
				follow_button: true,
				progress_bar: true,
				can_edit: false,
				show_user: false,
				items: []
			}
			if params.has_key?( :q )
				properties = Property.where( visibility: 'visible', status: 'available', stage: 1..99 ).search( params[:q] ).order( 'id desc' )
			else
				properties = Property.where( visibility: 'visible', status: 'available', stage: 1..99 ).order( 'id desc' ).limit( query_limit )
			end
			properties.each do |property|
				image = property.pictures.find_by_kind( :cover )
				image = image.nil? ? self.helpers.missing_img_url : property.pictures.find_by_kind( :cover ).file.url( :thumb )
				accompaniment = Accompaniment.where( user_id: current_user.id, property_id: property.id ).first
				slide[:items].push({
					id: property.id,
					name: property.name,
					stage: property.stage,
					stage_percent: "#{property.stage}%",
					image_src: image,
					following: ! accompaniment.nil?,
					accompaniment_id: accompaniment.nil? ? '' : accompaniment.id,
					user_src: '',
					url: property_path( property.id ),
					edit_url: edit_property_path( property.id )
				})
			end
			@sliders.push( slide )
			#Prontos para morar
			slide = {
				name: 'stage_100',
				title: 'Prontos para morar',
				title_link: '',
				follow_button: true,
				progress_bar: true,
				can_edit: false,
				show_user: false,
				items: []
			}
			if params.has_key?( :q )
				properties = Property.where( visibility: 'visible', status: 'available', stage: 100 ).search( params[:q] ).order( 'id desc' )
			else
				properties = Property.where( visibility: 'visible', status: 'available', stage: 100 ).order( 'id desc' ).limit( query_limit )
			end
			properties.each do |property|
				image = property.pictures.find_by_kind( :cover )
				image = image.nil? ? self.helpers.missing_img_url : property.pictures.find_by_kind( :cover ).file.url( :thumb )
				accompaniment = Accompaniment.where( user_id: current_user.id, property_id: property.id ).first
				slide[:items].push({
					id: property.id,
					name: property.name,
					stage: property.stage,
					stage_percent: "#{property.stage}%",
					image_src: image,
					following: ! accompaniment.nil?,
					accompaniment_id: accompaniment.nil? ? '' : accompaniment.id,
					user_src: '',
					url: property_path( property.id ),
					edit_url: edit_property_path( property.id )
				})
			end
			@sliders.push( slide )
			#Favoritos
			slide = {
				name: 'favourites',
				title: 'Favoritos',
				title_link: '',
				follow_button: true,
				progress_bar: true,
				can_edit: false,
				show_user: false,
				items: []
			}
			Accompaniment.where( user_id: current_user.id ).each do |accompaniment|
				property = Property.find( accompaniment.property_id )
				image = property.pictures.find_by_kind( :cover )
				image = image.nil? ? self.helpers.missing_img_url : property.pictures.find_by_kind( :cover ).file.url( :thumb )
				accompaniment = Accompaniment.where( user_id: current_user.id, property_id: property.id ).first
				slide[:items].push({
					id: property.id,
					name: property.name,
					stage: property.stage,
					stage_percent: "#{property.stage}%",
					image_src: image,
					following: ! accompaniment.nil?,
					accompaniment_id: accompaniment.nil? ? '' : accompaniment.id,
					user_src: '',
					url: property_path( property.id ),
					edit_url: edit_property_path( property.id )
				})
			end
			@sliders.push( slide )
			slide = {
				name: 'my_customers',
				title: 'Meus clientes',
				title_link: '',
				follow_button: false,
				progress_bar: false,
				can_edit: false,
				show_user: false,
				items: []
			}
			RealtorCustomer.where( user_id: current_user.id ).each do |c|
				user = User.find( c.customer_id )
				slide[:items].push({
					id: user.id,
					name: user.name,
					stage: 0,
					stage_percent: "0%",
					image_src: user.avatar? ? user.url( :thumb ) : self.helpers.missing_avatar_url,
					following: 0,
					accompaniment_id: '',
					user_src: '',
					url: customer_activities_url( user.id ),
					edit_url: ''
				})
			end
			@sliders.push( slide )
		when '_realtor'
			slide = {
				name: 'oportunities',
				title: 'Oportunidades',
				title_link: '',
				follow_button: true,
				progress_bar: true,
				can_edit: false,
				show_user: false,
				items: []
			}
			if params.has_key?( :q )
				properties = Property.where( visibility: 'visible', status: 'available' ).search( params[:q] ).order( 'id desc' )
			else
				properties = Property.where( visibility: 'visible', status: 'available' ).order( 'id desc' ).limit( query_limit )
			end
			properties.each do |property|
				image = property.pictures.find_by_kind( :cover )
				image = image.nil? ? self.helpers.missing_img_url : property.pictures.find_by_kind( :cover ).file.url( :thumb )
				accompaniment = Accompaniment.where( user_id: current_user.id, property_id: property.id ).first
				slide[:items].push({
					id: property.id,
					name: property.name,
					stage: property.stage,
					stage_percent: "#{property.stage}%",
					image_src: image,
					following: ! accompaniment.nil?,
					accompaniment_id: accompaniment.nil? ? '' : accompaniment.id,
					user_src: '',
					url: property_path( property.id ),
					edit_url: edit_property_path( property.id )
				})
			end
			@sliders.push( slide )
			Deal.statuses.each_with_index do |status, index|
				slide = {
					name: 'status_' + status[0],
					title: self.helpers.enum_i18n( Deal, 'status', status[0] ),
					title_link: '',
					follow_button: false,
					progress_bar: true,
					can_edit: false,
					show_user: true,
					items: []
				}
				Deal.where( dealer_id: current_user.id, status: status[0] ).each do |deal|
					property = Property.find( deal.property_id )
					image = property.pictures.find_by_kind( :cover )
					image = image.nil? ? self.helpers.missing_img_url : property.pictures.find_by_kind( :cover ).file.url( :thumb )
					accompaniment = Accompaniment.where( user_id: current_user.id, property_id: property.id ).first
					slide[:items].push({
						id: property.id,
						name: property.name,
						stage: property.stage,
						stage_percent: "#{property.stage}%",
						image_src: image,
						following: ! accompaniment.nil?,
						accompaniment_id: accompaniment.nil? ? '' : accompaniment.id,
						user_src: deal.customer.avatar? ? deal.customer.url( :thumb ) : self.helpers.missing_avatar_url,
						url: property_path( property.id ),
						edit_url: edit_property_path( property.id )
					})
				end
				@sliders.push( slide )
			end
			slide = {
				name: 'my_customers',
				title: 'Meus clientes',
				title_link: '',
				follow_button: false,
				progress_bar: false,
				can_edit: false,
				show_user: false,
				items: []
			}
			RealtorCustomer.where( user_id: current_user.id ).each do |c|
				user = User.find( c.customer_id )
				slide[:items].push({
					id: user.id,
					name: user.name,
					stage: 0,
					stage_percent: "0%",
					image_src: user.avatar? ? user.url( :thumb ) : self.helpers.missing_avatar_url,
					following: 0,
					accompaniment_id: '',
					user_src: '',
					url: customer_activities_url( user.id ),
					edit_url: ''
				})
			end
			@sliders.push( slide )
		when 'investor'
			#Pré lançamentos
			slide = {
				name: 'stage_0',
				title: 'Pré-lançamentos',
				title_link: '',
				follow_button: true,
				progress_bar: true,
				can_edit: false,
				show_user: false,
				items: []
			}
			if params.has_key?( :q )
				properties = Property.where( visibility: 'visible', status: 'available', stage: 0 ).search( params[:q] ).order( 'id desc' )
			else
				properties = Property.where( visibility: 'visible', status: 'available', stage: 0 ).order( 'id desc' ).limit( query_limit )
			end
			properties.each do |property|
				image = property.pictures.find_by_kind( :cover )
				image = image.nil? ? self.helpers.missing_img_url : property.pictures.find_by_kind( :cover ).file.url( :thumb )
				accompaniment = Accompaniment.where( user_id: current_user.id, property_id: property.id ).first
				slide[:items].push({
					id: property.id,
					name: property.name,
					stage: property.stage,
					stage_percent: "#{property.stage}%",
					image_src: image,
					following: ! accompaniment.nil?,
					accompaniment_id: accompaniment.nil? ? '' : accompaniment.id,
					user_src: '',
					url: property_path( property.id ),
					edit_url: edit_property_path( property.id )
				})
			end
			@sliders.push( slide )
			#Em andamento
			slide = {
				name: 'stage_1_99',
				title: 'Em andamento',
				title_link: '',
				follow_button: true,
				progress_bar: true,
				can_edit: false,
				show_user: false,
				items: []
			}
			if params.has_key?( :q )
				properties = Property.where( visibility: 'visible', status: 'available', stage: 1..99 ).search( params[:q] ).order( 'id desc' )
			else
				properties = Property.where( visibility: 'visible', status: 'available', stage: 1..99 ).order( 'id desc' ).limit( query_limit )
			end
			properties.each do |property|
				image = property.pictures.find_by_kind( :cover )
				image = image.nil? ? self.helpers.missing_img_url : property.pictures.find_by_kind( :cover ).file.url( :thumb )
				accompaniment = Accompaniment.where( user_id: current_user.id, property_id: property.id ).first
				slide[:items].push({
					id: property.id,
					name: property.name,
					stage: property.stage,
					stage_percent: "#{property.stage}%",
					image_src: image,
					following: ! accompaniment.nil?,
					accompaniment_id: accompaniment.nil? ? '' : accompaniment.id,
					user_src: '',
					url: property_path( property.id ),
					edit_url: edit_property_path( property.id )
				})
			end
			@sliders.push( slide )
			#Prontos para morar
			slide = {
				name: 'stage_100',
				title: 'Prontos para morar',
				title_link: '',
				follow_button: true,
				progress_bar: true,
				can_edit: false,
				show_user: false,
				items: []
			}
			if params.has_key?( :q )
				properties = Property.where( visibility: 'visible', status: 'available', stage: 100 ).search( params[:q] ).order( 'id desc' )
			else
				properties = Property.where( visibility: 'visible', status: 'available', stage: 100 ).order( 'id desc' ).limit( query_limit )
			end
			properties.each do |property|
				image = property.pictures.find_by_kind( :cover )
				image = image.nil? ? self.helpers.missing_img_url : property.pictures.find_by_kind( :cover ).file.url( :thumb )
				accompaniment = Accompaniment.where( user_id: current_user.id, property_id: property.id ).first
				slide[:items].push({
					id: property.id,
					name: property.name,
					stage: property.stage,
					stage_percent: "#{property.stage}%",
					image_src: image,
					following: ! accompaniment.nil?,
					accompaniment_id: accompaniment.nil? ? '' : accompaniment.id,
					user_src: '',
					url: property_path( property.id ),
					edit_url: edit_property_path( property.id )
				})
			end
			@sliders.push( slide )
			#Favoritos
			slide = {
				name: 'favourites',
				title: 'Favoritos',
				title_link: '',
				follow_button: true,
				progress_bar: true,
				can_edit: false,
				show_user: false,
				items: []
			}
			Accompaniment.where( user_id: current_user.id ).each do |accompaniment|
				property = Property.find( accompaniment.property_id )
				image = property.pictures.find_by_kind( :cover )
				image = image.nil? ? self.helpers.missing_img_url : property.pictures.find_by_kind( :cover ).file.url( :thumb )
				accompaniment = Accompaniment.where( user_id: current_user.id, property_id: property.id ).first
				slide[:items].push({
					id: property.id,
					name: property.name,
					stage: property.stage,
					stage_percent: "#{property.stage}%",
					image_src: image,
					following: ! accompaniment.nil?,
					accompaniment_id: accompaniment.nil? ? '' : accompaniment.id,
					user_src: '',
					url: property_path( property.id ),
					edit_url: edit_property_path( property.id )
				})
			end
			@sliders.push( slide )
		when 'construction_company'
			@kinds = PropertyKind.all.order( display_name: :asc )
			@kinds.each do |kind|
				if params.has_key?( :q )
					properties = Property.where( user_id: current_user.id, kind: kind.id ).search( params[:q] ).order( created_at: :desc )
				else
					properties = Property.where( user_id: current_user.id, kind: kind.id ).order( created_at: :desc )
				end
				if properties.any?
					slide = {
						name: "kind_#{kind.id}",
						title: kind.display_name,
						title_link: '',
						follow_button: false,
						progress_bar: true,
						can_edit: true,
						show_user: false,
						items: []
					}
					properties.each do |property|
						image = property.pictures.find_by_kind( :cover )
						image = image.nil? ? self.helpers.missing_img_url : property.pictures.find_by_kind( :cover ).file.url( :thumb )
						accompaniment = Accompaniment.where( user_id: current_user.id, property_id: property.id ).first
						slide[:items].push({
							id: property.id,
							name: property.name,
							stage: property.stage,
							stage_percent: "#{property.stage}%",
							image_src: image,
							realtor_src: '',
							following: ! accompaniment.nil?,
							accompaniment_id: accompaniment.nil? ? '' : accompaniment.id,
							user_src: '',
							url: property_path( property.id ),
							edit_url: edit_property_path( property.id )
						})
					end
					@sliders.push( slide )
				end
			end
		else
			properties = Property.all
		end

		@states = State.all.order( acronym: :asc )
		@cities = []
		@properties_kinds = PropertyKind.all
	end

	def show
		@property = Property.find( params[:id] )
		View.create(property: @property, user: current_user ) unless @property.user_id == current_user.id
		@gallery = @property.pictures.where( kind: :gallery ).order( sequence: :asc )
		@gallery_count = @gallery.nil? ? 0 : @gallery.count
		@gallery_stage = @property.pictures.where( kind: :stage ).order( sequence: :asc )
		@gallery_stage_count = @gallery_stage.nil? ? 0 : @gallery_stage.count
		@accompaniment = @property.accompaniments.where( user_id: current_user.id, property_id: @property.id ).last
	end

	def new
		@kinds = PropertyKind.all.order( display_name: :asc ).collect {|p| [ p.display_name, p.id ] }
		@property = Property.new
		@states = State.all
		respond_to do |format|
	    	format.html
	    	format.js
	  	end
	end

	def create
		@property = Property.new( property_params )

		    if @property.save
		    	respond_to do |format|
		    		format.html do
				    	flash[:message] = "Empreendimento salvo com sucesso!"
				      	redirect_to edit_property_path( @property.id )
				    end
				    format.js do
				    	data = {
				    		:id => @property.id,
						 	:name => @property.name,
						 	:editPath => edit_property_path( @property.id )
				    	}
				    	render :json => data, :callback => 'onPropertyCreate'
				    end
			    end
		    else
		    	respond_to do |format|
			    	format.html { render 'new' }
			    	format.js { render :json => { "error": true, "errors": @property.errors, "formated_message": self.helpers.formated_errors( @property.errors ) }, status: :unprocessable_entity, :callback => 'onPropertyCreate' }
			  	end
		    end
	end

	def change_plants_order
		@property = Property.find( params[:id] )
	end

	def edit
		@property = Property.find( params[:id] )

		if @property.state_id.nil?
			@cities = []
		else
			@cities = City.where(state_id: @property.state_id)
		end

		@states = State.all.order( acronym: :asc )
		@kinds = PropertyKind.all.order( display_name: :asc ).collect {|p| [ p.display_name, p.id ] }
		@gallery = @property.pictures.where( kind: :gallery ).order( sequence: :asc )
		@gallery_count = @gallery.nil? ? 0 : @gallery.count
		@gallery_stage = @property.pictures.where( kind: :stage ).order( sequence: :asc )
		@gallery_stage_count = @gallery_stage.nil? ? 0 : @gallery_stage.count
		@owners = User.where( role: [1,4] ).order( 'name asc' ).collect {|p| [ p.name, p.id ] }
	end

	def update
		@property = Property.find( params[:id] )

	    if @property.update!( property_params )
	    	respond_to do |format|
	    		format.html do
			    	flash[:message] = "Empreendimento salvo com sucesso!"
			      	redirect_to edit_property_path( @property.id )
			    end
			    format.js do
			    	data = {
			    		:id => @property.id,
					 	:name => @property.name,
					 	:latitude => @property.latitude,
					 	:longitude => @property.longitude,
					 	:editPath => edit_property_path( @property.id )
			    	}
			    	render :json => data, :callback => 'onPropertyUpdate'
			    end
		    end
	    else
	      	respond_to do |format|
		    	format.html { render 'edit' }
		    	format.js { render :json => { "error": true, "errors": @property.errors, "formated_message": self.helpers.formated_errors( @property.errors ) }, status: :unprocessable_entity, :callback => 'onPropertyUpdate' }
		  	end
	    end
	end

	def destroy
		@property = Property.find( params[:id] )
	    if @property.destroy
				  redirect_to action: :index
      		# render json: { message: 'success', id: @property.id }, :status => 200
    	else
      		render json: { error: @property.errors.full_messages.join( ',' ) }, :status => 400
    	end
  end

	def search
    @cities = []
		@stages = Property.stages
		@states = State.all.order( acronym: :asc )
		@properties_kinds = PropertyKind.all.order( display_name: :asc )
		@partners = User.users_with_properties
	end

	def search_filter
		@properties = Property.search_filter(params.to_unsafe_hash)

		render partial: 'properties/search_result'
	end

	private

    def property_params
    	params.require( :property ).permit(
    		:name, :kind, :user_id, :info, :doubts, :value, :payment_conditions, :stage,
    		:stage_info, :map_coords, :address, :city, :state, :zipcode, :visibility, :notified, :status,
    		:accept_other_payment_methods, :other_payment_methods, :reservation_time, :city_id, :state_id, :district )
    end

end
