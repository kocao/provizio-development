class PropertyKindsController < ApplicationController

	helper PropertyKindsHelper

	def create
    	@kind = PropertyKind.new( kind_params )

    	if @kind.save
      		render json: { message: 'success', id: @kind.id, name: @kind.name, display_name: @kind.display_name }, :status => 200, callback: 'onPropertyKindCreate' 
    	else
      		render json: { error: @kind.errors.full_messages.join( ',' ) }, :status => 400
    	end
  	end

  	def new
  		@kind = PropertyKind.new
  	end

  	def edit
  		@kind = PropertyKind.find( params[:id] )
  	end

    def update
      @kind = PropertyKind.find( params[:id] )
      if @kind.update!( kind_params )
        render json: { 
        	message: 'success', 
        	id: @kind.id, 
        	name: @kind.name, 
        	display_name: @kind.display_name,
        	properties_count: Property.where( kind: @kind.id ).count, 
        }, :status => 200, callback: 'onPropertyKindUpdate' 
      else
        render json: { error: @kind.errors.full_messages.join( ',' ) }, :status => 400
      end
    end

  	def destroy
		  @kind = PropertyKind.find( params[:id] )
	    if @kind.destroy
      		render json: { message: 'success', id: @kind.id }, :status => 200
    	else
      		render json: { error: @kind.errors.full_messages.join( ',' ) }, :status => 400
    	end
	end	

	private
  	
  	def kind_params
    	params.require( :property_kind ).permit( :id, :name, :display_name )
  	end
end
