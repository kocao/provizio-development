class RolesController < ApplicationController

	def new
		@role = Role.new
		respond_to do |format|
	    	format.html
	    	format.js
	  	end
	end

	def create
    	@role = Role.new( roles_params )

    	if @role.save
      		render json: { message: 'success', id: @role.id, redirect_to: configurations_url + "?role=#{@role.id}" }, :status => 200, :callback => 'onRoleCreate' 
    	else
      		render json: { error: @role.errors.full_messages.join( ',' ) }, :status => 400
    	end
  end

  def destroy
    @role = Role.find( params[:id] )
    if @role.destroy
        render json: { message: 'success', id: @role.id }, :status => 200
    else
        render json: { error: @role.errors.full_messages.join( ',' ) }, :status => 400
    end
  end

	private

    def roles_params
    	params.require( :role ).permit( :name, :display_name, :created_by )
    end
end
