class TimelinesController < ApplicationController
	before_action :authenticate_user!
	def index
		@timeline_entries = Timeline.where( user_id: params[:user_id] ).order( created_at: :desc ).all
		respond_to do |format| 
			format.json { render json: @timeline_entries, status: :ok }
	        format.html { render partial: 'timelines/timeline', status: :ok }
	    end
	end
end
