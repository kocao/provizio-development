class UsersController < ApplicationController

	helper ApplicationHelper
	before_action :add_data

	def add_data
		@user = self.helpers.action?( 'edit', 'update' ) ? User.find( params[:id] ) : User.new
		@cities = self.helpers.action?( 'edit', 'update' ) && ! @user.state.nil? ? City.where( state: @user.state.id ) : []
		@states = State.all.order( acronym: :asc )
		@roles = Role.registrable.collect {|p| [ p.display_name, p.id ] }
	end

	def new
		@user = User.new
	end
	def edit
		@user = User.find( params[:id] )
	end
	def create
		@user = User.new( user_params )
		role_id = @user.role_id
	    if @user.save
	    	flash[:user_created] = true
	    	if role_id == 1 && current_user.role.name == 'admin'
	    		@user.update_attribute( :role_id, 1 )
	    	end
	      	redirect_to params.has_key?( :resource ) ? eval( params[:resource] + '_url' ) : users_url
	    else
	    	if params.has_key?( :resource ) && params[:resource] == 'my_contacts'
	    		render template: 'panel/add_contact'
	    	else
	      		render 'new'
	      	end
	    end
	end
	def update
		if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
    		params[:user].delete(:password)
		    params[:user].delete(:password_confirmation)
		end

	    if @user.update( user_params )
	    	flash[:user_edited] = true
	      	redirect_to params.has_key?( :resource ) ? eval( params[:resource] + '_url' ) : users_url
	    else
	      	if params.has_key?( :resource ) && params[:resource] == 'my_contacts'
	    		render template: 'panel/edit_contact'
	    	else
	      		render 'edit'
	      	end
	    end
	end
	def destroy
		@user = User.find( params[:id] )
	    if @user.destroy
	    	respond_to do |format|
		    	format.html { redirect_to user_path, flash: { :message => 'Usuário removido com sucesso!' } }
		      	format.json { render json: @user, status: :ok, location: @user }
		    end
	    else
	    	respond_to do |format|
		      	format.html { redirect_to user_path, flash: { :warning => 'Falha ao tentar remover usuário!' } }
		      	format.json { render json: @user.errors, status: :unauthorized }
		    end
	    end
	end

	def change_profile
		user = current_user
		if user.availables_roles.collect {|r| r.id}.include?(params['id'].to_i)
			new_role_id = params['id']
			user.original_role_id = user.role_id if user.original_role_id.nil?
			user.role_id = new_role_id
			user.save
		end

		redirect_to root_url
	end

	def accept_invite
		@invite = Invite.where(token: params['token']).first

		if @invite.blank?
			redirect_to new_user_registration_url
		else
			redirect_to new_user_registration_url + "?name=#{@invite.name}&email=#{@invite.email}"
		end
	end


	private
    def user_params
    	params.require( :user ).permit( :terms, :role_id, :name, :email, :phone, :cellphone, :avatar, :state_id, :city_id, :address, :bio, :job, :password, :password_confirmation, :creci, :cpf, :cnpj, :birthdate, :ready, :marca, :latitude, :longitude, :about_me, :back_image )
    end
end
