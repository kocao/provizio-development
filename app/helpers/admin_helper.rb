module AdminHelper

	def admin_field( data, parent = nil )
		resource = parent.nil? ? 'meta' : 'contents'
		case data.field_type
			when 'text_field'
				text_field :page, :meta, name: 'value', value: data.value, id: "#{resource}_#{data.id}", class: 'admin-editable', "data-attr": data.name, "data-id": data.id, "data-resource": resource 
			when 'text_area'
				text_area :page, :meta, name: 'value', value: data.value, id: "#{resource}_#{data.id}", class: 'admin-editable admin-textarea', "data-attr": data.name, "data-id": data.id, "data-resource": resource 
			when 'file_field'
				file_field :page, :meta, name: 'value', value: data.value, id: "#{resource}_#{data.id}", class: 'admin-editable-file', "data-attr": data.name, "data-id": data.id, "data-resource": resource 
			when 'dropbox_image'
				render partial: "admin/pages/dropzone_single_image", locals: { data: data, parent: parent }
			when 'trix_editor'
				"<input name=\"value\" id=\"#{resource}_#{data.id}\" class=\"admin-editable\" data-attr=\"#{data.name}\" data-id=\"#{data.id}\" data-resource=\"#{resource}\" value=\"#{data.value.html_safe}\" type=\"hidden\">
				<trix-editor input=\"#{resource}_#{data.id}\"></trix-editor>".html_safe
			else
		end
	end

end
