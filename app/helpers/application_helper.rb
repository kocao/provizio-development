module ApplicationHelper
	def controller?(*controller)
    	controller.include?(params[:controller])
  	end
  	def action?(*action)
    	action.include?(params[:action])
  	end
  	def formated_errors( errors ) 
		message = ""
		errors.each do |field,error|
			message+= "Campo #{ field }: " + error + "\n\r"
		end
		message
	end
end
