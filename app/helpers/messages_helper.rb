module MessagesHelper

	def message_data( message )
		from_user = User.find_by_email( message.from )
		attachments  = ''
		if message_with_attachments?( message )
			attachments = '<i class="fa fa-paperclip"></i>'
		end
		{
			id: message.id,
			box: message.box,
			read: message.read,
			attachment: attachments.html_safe,
			excerpt: sanitize( message.text, :tags=>[] )[0..30],
			from: from_user.nil? ? message.from : from_user.name,
			subject: message.subject,
			datetime: message.created_at.to_datetime.strftime( "%d/%m/%Y %H:%M" ),
			edit_url: message_path( message )
		}
	end 

	def message_with_attachments?( message )
		message.prev_id.nil? && ! message.attachments.empty? || ! message.prev_id.nil? && ! message.prev.attachments.empty?
	end

	def message_item( message )
		ac = ActionController::Base.new()  
		template = ac.render_to_string :partial => "messages/message_item"
		replaces = message_data( message )
		template % replaces
	end
end
