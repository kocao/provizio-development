module PagesHelper
	def page_content( name )
		metum = @page.meta.where( name: name ).order( order: :asc, id: :asc )
		case metum.first.field_type
			when 'repeater'
				data = []
				metum.each do |m|
					data_content = {}
					m.contents.order( order: :asc, id: :asc ).each do |c|
						if c.field_type == 'dropbox_image'
							image = Attachment.find( c.value )
							image = image.nil? ? '' : image.file.url()
							data_content[c.name.to_sym] = image
						else
							data_content[c.name.to_sym] = c.value.html_safe
						end
					end
					data.push( data_content )
				end
				data
			when 'dropbox_image'
				image = Attachment.find( metum.first.value )
				image.nil? ? '' : image.file.url()
			else
				metum.first.value.html_safe
		end	
	end
end
