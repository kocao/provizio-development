module PicturesHelper
	def picture_geometry( picture, style = :original )
		@geometry ||= {}
	  	picture_path = (Rails.application.config.respond_to?( 'paperclip_defaults' ) && Rails.application.config.paperclip_defaults[:storage] == :s3) ? picture.url( style ) : picture.path( style )
	  	@geometry[style] ||= Paperclip::Geometry.from_file( picture_path )
	end	
end
