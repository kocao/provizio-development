module PropertiesHelper
	def render_slider( slider, template = 'slider' )
		slides_html = ''
		ac = ActionController::Base.new()
		slider_item_template = template == 'table' ? 'table_row' : 'slider_item'
		slider_template = ac.render_to_string :partial => "properties/templates/#{template}"
		slider_item_template = ac.render_to_string :partial => "properties/templates/#{slider_item_template}"
		slider[:items].each do |slide|
			slides_html+= slider_item_template % slide
		end
		replaces = slider
		replaces[:slides] = slides_html.html_safe
		#replaces[:items] = replaces[:items].length
		replaces[:more_button_url] = root_url + "?filter_by=#{slider[:name]}"
		slider_template % replaces
	end
	def render_gallery_item( picture, action, group = 'gallery' )
		ac = ActionController::Base.new()
		template = ac.render_to_string :partial => "properties/#{action}/gallery_item"
		replaces = {
			id: picture.id,
			thumb: picture.file.url( :thumb ),
			large: picture.file.url( :large ),
			label: picture.label,
			group: group
		}
		template % replaces
	end
	def property_cover_url( property )
		image = property.pictures.find_by_kind( :cover )
		image = image.nil? ? self.missing_img_url : property.pictures.find_by_kind( :cover ).file.url( :thumb )
	end
	def property_icon( property )
		case property.kind
		when 'allotment'
			return 'tree'
		when 'house'
			return 'home'
		when 'apartment'
			return 'building-o'
		when 'condominium'
			return 'building'
		when 'comercial_room'
			return 'credit-card'
		when 'terrain'
			return 'road'
		end
	end
	def missing_img_url
		image_url( 'missing.png' )
	end
	def image_menu( image_name, alt, controller, action = nil )
		if action.nil?
			sufix = controller?( controller ) ? '-ativo' : ''
		else
			sufix = controller?( controller ) && action?( action ) ? '-ativo' : ''
		end
		image_tag image_name.gsub( /.png/, "#{sufix}.png" ), alt: alt
	end
	def currency_format(value)
		value_string = value.to_s
		array_string = value_string.reverse.scan(/\w{1,3}/)
		array_string.reverse!
		value_string = ""
		array_string.each do |centena|
			value_string << "." unless value_string.empty?
			value_string << centena.reverse
		end

		value_string = "R$ " + value_string
		return value_string
	end
end
