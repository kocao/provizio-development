module PropertyKindsHelper
	def kind_data( kind )
		{
			id: kind.id,
			name: kind.name,
			display_name: kind.display_name,
			properties_count: Property.where( kind: kind.id ).count,
			edit_url: edit_property_kind_url( kind ),
		}
	end 
	def property_kind_item( kind )
		ac = ActionController::Base.new()  
		template = ac.render_to_string :partial => "property_kinds/kind_item"
		replaces = kind_data( kind )
		template % replaces
	end
end
