class ActivityMailer < ApplicationMailer
	default from: 'davsilveira@gmail.com'

	def warn_customer( activity )

		attachments.inline['mail-header.png'] = File.read('app/assets/images/mail-header.png')
		attachments.inline['mail-footer.png'] = File.read('app/assets/images/mail-footer.png')
		
		@activity = activity
		@user = User.find( activity.user_id )
		to = @user.email
		subject = "Lembrete de atividade: #{activity.title}"
    	mail( to: to, subject: subject )
    	
  	end

  	def warn_realtor( activity )

		attachments.inline['mail-header.png'] = File.read('app/assets/images/mail-header.png')
		attachments.inline['mail-footer.png'] = File.read('app/assets/images/mail-footer.png')
		
		@activity = activity
		@user = User.find( activity.created_by )
		to = @user.email
		subject = "Lembrete de atividade: #{activity.title}"
    	mail( to: to, subject: subject )
    	
  	end

end
