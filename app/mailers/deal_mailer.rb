class DealMailer < ApplicationMailer
	default from: 'davsilveira@gmail.com'

	def new_deal( deal )
		attachments.inline['mail-header.png'] = File.read('app/assets/images/mail-header.png')
		attachments.inline['mail-footer.png'] = File.read('app/assets/images/mail-footer.png')
		@deal = deal
		subject = "Novo negócio aberto"
		to = []
		to.push( deal.dealer.email )
		to.push( deal.property.owner.email )
		mail( to: to, subject: subject )
	end
end
