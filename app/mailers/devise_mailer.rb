class DeviseMailer < Devise::Mailer

  before_filter :add_inline_attachment!

  private

  def add_inline_attachment!
    attachments.inline['mail-header.png'] = File.read('app/assets/images/mail-header.png')
    attachments.inline['mail-footer.png'] = File.read('app/assets/images/mail-footer.png')
  end

end