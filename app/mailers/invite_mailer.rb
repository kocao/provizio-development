class InviteMailer < ApplicationMailer
	default from: 'davsilveira@gmail.com'

	def invite_provizio( invite )
		attachments.inline['mail-header.png'] = File.read('app/assets/images/mail-header.png')
		attachments.inline['mail-footer.png'] = File.read('app/assets/images/mail-footer.png')
		# @property = property
		subject = "Convite grupo de investimentos."
		to = [invite.email]
		@invite = invite
		mail( to: to.uniq, subject: subject )
	end
end
