class NotificationMailer < ApplicationMailer
	default from: 'davsilveira@gmail.com'

	def notification_email( notification )
		send_message = true
		attachments.inline['mail-header.png'] = File.read('app/assets/images/mail-header.png')
		attachments.inline['mail-footer.png'] = File.read('app/assets/images/mail-footer.png')
		case notification.notificable_type
			when 'Message'
				@message = Message.find( notification.notificable_id )
				if @message.to == 'me'
					send_message = false
				else
					sent_by = User.find_by_email( @message.from )
					sent_by = sent_by.nil? ? @message.from : sent_by.name
					subject = @message.property_id.nil? ? sent_by + ' enviou uma nova mensagem' : sent_by + ' recomendou um imóvel'
					to = @message.to 
					@body = "<p>#{subject}:</p>" + @message.text
					@template = 'message'
				end
			when 'Note'
				@note = Note.find( notification.notificable_id )
				if @note.user_id == @note.created_by
					send_message = false
				else
					@user = User.find( @note.user_id )
					to = @user.email
					subject = 'Uma nova nota foi adicionada para você'
					@body = "<p>#{subject}:</p>" + @note.text
					@template = 'note'
				end
			when 'Activity'
				@activity = Activity.find( notification.notificable_id )
				@user = User.find( @activity.user_id )
				to = @user.email
				subject = 'Uma nova atividade foi adicionada para você'
				@template = 'activity'
			when 'Property'
				@property = Property.find( notification.notificable_id )
				case notification.modifier
					when 'sold'
						subject = "O empreendimento #{@property.name} que você estava seguindo foi vendido"
					when 'published'
						subject = "#{@property.owner.name} adicionou um novo empreendimento"
				end
				@user = User.find( notification.user_id )
				to = @user.email
				@template = "property_#{notification.modifier}"

		end
		if send_message
    		mail( to: to, subject: subject )
    	end
  	end

end
