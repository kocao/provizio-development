class PropertyMailer < ApplicationMailer
	default from: 'davsilveira@gmail.com'

	def new_stage_pictures( property )
		attachments.inline['mail-header.png'] = File.read('app/assets/images/mail-header.png')
		attachments.inline['mail-footer.png'] = File.read('app/assets/images/mail-footer.png')
		@property = property
		subject = "O empreendimento #{property.name} tem novas fotos"
		to = []
		Deal.where( property_id: property.id ).each do |deal|
			to.push( deal.dealer.email )
			to.push( deal.customer.email )
		end
		Accompaniment.where( property_id: property.id ).each do |acc|
			to.push( acc.user.email )
		end
		mail( to: to.uniq, subject: subject )
	end
end
