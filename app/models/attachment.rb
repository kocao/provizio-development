class Attachment < ApplicationRecord
	
	belongs_to :attachable, polymorphic: true
	belongs_to :property, optional: true
	belongs_to :uploader, class_name: 'User', foreign_key: 'uploaded_by'

	validates :file, attachment_presence: true
	has_attached_file :file, styles: { thumb: "60x60#", large: "1920x1080>" }
	CONTENT_TYPES = [
						'image/jpeg', 
						'image/png', 
						'application/pdf',
						'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
			            'application/msword', 
			            'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 
			            'text/plain',
			            'text/xml',
			            'application/x-rar-compressed', 
			            'application/octet-stream',
			            'application/zip'
						]
  	validates_attachment_content_type :file, 
  									  content_type: CONTENT_TYPES, 
  									  :size => { :less_than => 2.megabyte }

  	after_save :add_to_timeline, :add_metum_reference, :add_content_reference
  	before_destroy :destroy_metum_reference, :destroy_content_reference

  	before_post_process :skip_for_non_images

  	private

  		def skip_for_non_images
		    %w(image/jpeg image/png).include?( CONTENT_TYPES )
		end

	  	def add_to_timeline
	  		if modifier == 'timeline'
		  		Timeline.create!({
		  			timelinable_id: id,
		  			timelinable_type: 'Attachment',
		  			user_id: attachable_id,
		  			property_id: property_id
		  		})
		  	end
	  	end

	  	def add_metum_reference
	  		if attachable_type == "Metum"
		  		Metum.find_by( id: attachable_id, name: modifier ).update_attribute( :value, id )
		  	end
	  	end

	  	def add_content_reference
	  		if attachable_type == "Content"
		  		Content.find_by( id: attachable_id, name: modifier ).update_attribute( :value, id )
		  	end
	  	end

	  	def destroy_metum_reference
	  		metum = Metum.find_by( id: attachable_id, name: modifier )
	  		metum.update_attribute( :value, nil ) unless metum.nil?
	  	end

	  	def destroy_content_reference
	  		content = Content.find_by( id: attachable_id, name: modifier )
	  		content.update_attribute( :value, nil ) unless content.nil?
	  	end

end
