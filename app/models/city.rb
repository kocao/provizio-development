class City < ApplicationRecord
  belongs_to :state

  validates :name, presence: true
  validates :state, presence: true

  scope :order_by_name, -> { order('unaccent(lower(name))') }
end
