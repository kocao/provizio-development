class Contact < ApplicationRecord
	belongs_to :state, optional: true
	belongs_to :city, optional: true
	belongs_to :realtor, class_name: 'User', foreign_key: 'user_id'

	has_attached_file :avatar, styles: { thumb: "140x140#" }, default_url: ":style/missing-avatar.png"
	validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/, :size => { :less_than => 2.megabyte }

	def self.search( search )
		where( "user.email ILIKE ? OR user.name ILIKE ? OR user.bio ILIKE ?", "%#{search}%", "%#{search}%", "%#{search}%" )
	end

end
