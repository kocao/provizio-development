
class LimitReservationForInvestors < ActiveModel::Validator
	def validate( record )
		quota = 10
		if record.customer.role.name == 'investor'
			reservations = Deal.where( customer_id: record.customer_id, status: :reserved ).count
	    	if reservations >= quota
	      		record.errors[:quota] << 'Limite de reservas atingido.'
	    	end
	    end
	    record
  	end
end

class Deal < ApplicationRecord

	include ActiveModel::Validations
  	validates_with LimitReservationForInvestors

	belongs_to :dealer, class_name: 'User', foreign_key: "dealer_id"
	belongs_to :customer, class_name: 'User', foreign_key: "customer_id"
	belongs_to :property
	belongs_to :plant, class_name: 'ImageMap', foreign_key: 'plant_id', dependent: :destroy

	monetize :down_payment_centavos
	monetize :total_value_centavos

	before_create :add_dealer_to_deal, :add_expiration_date

	enum status: {
		waiting_customer: 0,
		finished: 1,
		refused: 2,
		counter: 3,
		canceled: 4,
		expired: 5,
		reserved: 6
	}

	def add_dealer_to_deal
		self.dealer_id = InvestorRealtor.find_by_user_id( self.customer_id ).realtor_id
	end

	def add_expiration_date
		if self.property.reservation_time > 0
			self.expires_at = Time.now + self.property.reservation_time.days
		end
	end

end