class Document < ApplicationRecord
	belongs_to :property
	validates :file, attachment_presence: true
	has_attached_file :file, styles: { thumb: "60x60#", large: "1920x1080>" }
  	validates_attachment_content_type :file, 
  									  content_type: [
  									  	'image/jpeg', 
  									  	'image/png', 
  									  	'application/pdf',
  									  	'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
							            'application/msword', 
							            'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 
							            'text/plain',
							            'text/xml',
							            'application/x-rar-compressed', 
							            'application/octet-stream',
							            'application/zip'
  									  ], 
  									  :size => { :less_than => 2.megabyte }

end
