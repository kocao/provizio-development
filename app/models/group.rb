class Group < ApplicationRecord
  belongs_to :property
  belongs_to :user

  has_many :groups_user, dependent: :destroy
end
