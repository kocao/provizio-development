class ImageMap < ApplicationRecord
	belongs_to :picture

	def update_spot( spot_name, spot_status )
		if spot_status.nil? || self.map_data.nil? 
			return false
		end
		spots = JSON.parse( self.map_data )
		spots[0]['spots'].each_with_index do |spot,i|
			if spot['title'] == spot_name
				case spot_status
					when 'reserved'
						replaces = {
							available_text: self.available_text
						}
						spots[0]['spots'][i]['default_style']['fill'] = '#ffe7a5'
						spots[0]['spots'][i]['default_style']['fill_opacity'] = 0.9
						spots[0]['spots'][i]['mouseover_style']['fill'] = '#ffe7a5'
						spots[0]['spots'][i]['tooltip_content']['tooltip_status'] = 'reserved'
						#spots[0]['spots'][i]['tooltip_content']['plain_text'] = 'Este empreendimento já esta reservado'
						spots[0]['spots'][i]['tooltip_content']['plain_text'] = '<b class="tooltip-title">Informações do empreendimento</b><div class="tooltip-content">%{available_text}</div><p><i>Este empreendimento já esta reservado</i></p>'
						spots[0]['spots'][i]['tooltip_content']['plain_text'] = spots[0]['spots'][i]['tooltip_content']['plain_text'] % replaces
					when 'available'
						replaces = {
							available_text: self.available_text,
							id: spot['id'],
							name: spot['title'],
							imap_id: self.id
						}
						spots[0]['spots'][i]['default_style']['fill'] = '#80d4b7'
						spots[0]['spots'][i]['default_style']['fill_opacity'] = 0.89
						spots[0]['spots'][i]['mouseover_style']['fill'] = '#80d4b7'
						spots[0]['spots'][i]['tooltip_content']['tooltip_status'] = 'available'
						spots[0]['spots'][i]['tooltip_content']['plain_text'] = '<b class="tooltip-title">Informações do empreendimento</b><div class="tooltip-content">%{available_text}</div><button class="tooltip-button reserve-spot" data-imap="%{imap_id}" data-spot="%{id}" data-spot-name="%{name}">Reservar</button>'
						spots[0]['spots'][i]['tooltip_content']['plain_text'] = spots[0]['spots'][i]['tooltip_content']['plain_text'] % replaces
					when 'sold'
						replaces = {
							available_text: self.available_text
						}
						spots[0]['spots'][i]['default_style']['fill'] = '#df9086'
						spots[0]['spots'][i]['default_style']['fill_opacity'] = 0.9
						spots[0]['spots'][i]['mouseover_style']['fill'] = '#df9086'
						spots[0]['spots'][i]['tooltip_content']['tooltip_status'] = 'sold'
						#spots[0]['spots'][i]['tooltip_content']['plain_text'] = 'Este empreendimento já foi vendido'
						spots[0]['spots'][i]['tooltip_content']['plain_text'] = '<b class="tooltip-title">Informações do empreendimento</b><div class="tooltip-content">%{available_text}</div><p><i>Este empreendimento já foi vendido</i></p>'
						spots[0]['spots'][i]['tooltip_content']['plain_text'] = spots[0]['spots'][i]['tooltip_content']['plain_text'] % replaces
				end
			end
		end
		spots = spots.to_json
		self.update_attribute( 'map_data', spots )
		spots
	end

end
