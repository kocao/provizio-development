class InvestorRealtor < ApplicationRecord
	belongs_to :user
	belongs_to :realtor, class_name: 'User', foreign_key: 'realtor_id'

	after_create :add_realtor_customer
	before_destroy :remove_realtor_customer

	def add_realtor_customer
		RealtorCustomer.create({
			user_id: realtor.id,
			customer_id: user.id
		})
	end

	def remove_realtor_customer
		realtor_customer = RealtorCustomer.where( user_id: realtor.id, customer_id: user.id ).first
		unless realtor_customer.nil?
			RealtorCustomer.destroy( realtor_customer.id )
		end
	end

end
