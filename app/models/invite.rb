class Invite < ApplicationRecord
  belongs_to :group
  after_create :send_email_invite

  def self.generate_token
    token = ''
    range = ('a'..'z').to_a + ('a'..'z').to_a + ('1'..'9').to_a
    50.times do
      token << range.shuffle.shuffle.first
    end
    return token
  end

  def send_email_invite
    InviteMailer.invite_provizio(self).deliver_now
  end
end
