class Message < ApplicationRecord

	paginates_per 50

	belongs_to :user
	belongs_to :property, optional: true
	belongs_to :deal, optional: true
	belongs_to :prev, class_name: 'Message', foreign_key: 'prev_id', optional: true
	has_many :attachments, as: :attachable

	accepts_nested_attributes_for :attachments

	validates :to, presence: true
	validates :subject, presence: true

	enum box: {
		inbox: 0,
	    outbox: 1,
	    sent: 2,
	    trash: 3
	}

	after_save :add_to_timeline
	after_create :create_notification
	before_save :set_read_at_if_read

	def text_html
		self.text.nil? ? self.text : self.text.html_safe
	end

	def self.search( search )
		where( "messages.to ILIKE ? OR messages.subject ILIKE ? OR messages.text ILIKE ?", "%#{search}%", "%#{search}%", "%#{search}%" )
	end

	private

		def set_read_at_if_read
			self.read_at = read? ? Time.current : nil
		end

		def add_to_timeline
	  		Timeline.create!({
	  			timelinable_id: id,
	  			timelinable_type: 'Message',
	  			user_id: user_id,
	  			property_id: property_id
	  		})
	  	end

	  	def create_notification
	  		if to == 'me'
		  		Notification.create!({
		  			notificable_id: id,
		  			notificable_type: 'Message',
		  			user_id: user_id
		  		})
		  	end
	  	end

end