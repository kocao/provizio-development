class Note < ApplicationRecord

	belongs_to :user
	belongs_to :property, optional: true
	belongs_to :creator, class_name: 'User', foreign_key: 'created_by'

	validates :text, presence: true

	after_save :add_to_timeline
	after_create :create_notification

	private

	  	def add_to_timeline
	  		Timeline.create!({
	  			timelinable_id: id,
	  			timelinable_type: 'Note',
	  			user_id: user_id,
	  			property_id: property_id
	  		})
	  	end

	  	def create_notification
	  		Notification.create!({
	  			notificable_id: id,
	  			notificable_type: 'Note',
	  			user_id: user_id
	  		})
	  	end

end
