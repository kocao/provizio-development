class Notification < ApplicationRecord
	belongs_to :notificable, polymorphic: true
	belongs_to :user

	after_create :send_notification_mail

	private

	def send_notification_mail
		#pry.binding
		NotificationMailer.notification_email( self ).deliver_now
	end
end
