class Picture < ApplicationRecord

	belongs_to :property, optional: true
	belongs_to :user, optional: true
	has_one :image_map, dependent: :destroy

	enum kind: {
		  gallery: 0,
	    cover: 1,
	    featured: 2,
	    map: 3,
	    plant: 4,
	    stage: 5
	}

	after_create :create_image_map_for_plant

	validates :kind, presence: true
	validates :file, attachment_presence: true
	has_attached_file 	:file,
						styles: { thumb: "230x210#", large: "1920x550>" },
						default_url: "/images/:style/missing.png"

  	validates_attachment_content_type :file, content_type: /\Aimage\/.*\z/, :size => { :less_than => 2.megabyte }

  	def create_image_map_for_plant
  		if kind == 'plant'
  			ImageMap.create({
  				picture_id: id
  			})
  		end
  	end

end
