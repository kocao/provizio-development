class Property < ApplicationRecord

	belongs_to :owner, class_name: 'User', foreign_key: "user_id"
	belongs_to :property_kind, class_name: 'PropertyKind', foreign_key: "kind"
	belongs_to :state
	belongs_to :city
	#has_many :places, dependent: :destroy
	has_many :pictures, dependent: :destroy
	has_many :accompaniments, dependent: :destroy
	has_many :deals, dependent: :destroy
	has_many :users, through: :accompaniments
	has_many :documents, dependent: :destroy
	has_many :views, dependent: :destroy

	monetize :value_centavos
	geocoded_by :address
	after_validation :geocode
	after_save :create_notification
	after_destroy :destroy_notifications
	#reverse_geocoded_by :latitude, :longitude
	#after_validation :reverse_geocode

	#enum kind: {
	#	apartment: 1,
	#	house: 0,
	#    condominium: 4,
	#    allotment: 2,
	#    comercial_room: 3,
	#    terrain: 5
	#}

	enum status: {
		available: 0,
		sold: 1
	}

	enum visibility: {
		draft: 0,
	    visible: 1
	}

	validates :kind, presence: true
	validates :name, presence: true

	def self.stages
		{L: "Pré-lançamento", A: "Em andamento", P: "Pronto"}
	end

	def self.search( search )
  		where("name ILIKE ?", "%#{search}%")
	end

	def self.search_filter(params)
		data = {}
		query = "visibility = 1"

		unless params['city_id'].empty?
			query += " and city_id = :city_id"
			data.merge!(city_id: params['city_id'])
		end

		unless params['state_id'].empty?
			query += " and state_id = :state_id"
			data.merge!(state_id: params['state_id'])
		end

		unless params['district'].empty?
			query += " and district like '%:district%'"
			data.merge!(district: params['district'])
		end

		unless params['vlr_min'].empty?
			query += " and value_centavos >= :vlr_min"
			data.merge!(vlr_min: params['vlr_min'])
		end

		unless params['vlr_max'].empty?
			query += " and value_centavos <= :vlr_max"
			data.merge!(vlr_max: params['vlr_max'])
		end

		unless params['kinds'].nil?
			query += " and kind in (:kind)"
			data.merge!(kind: params['kinds'])
		end

		unless params['stages'].nil?
			stages = params['stages']
			# L = Pré-lançamento, A = Em andamento, P = Pronto
			query_stage = " and ( stage = 1000"
			query_stage += " or stage = 0" if params['stages'].include?('L')
			query_stage += " or stage between 1 and 99" if params['stages'].include?('A')
			query_stage += " or stage = 100" if params['stages'].include?('P')
			query_stage += ")"

			query += query_stage
		end

		unless params['user_id'].empty?
			query += " and user_id = :user_id"
			data.merge!(user_id: params['user_id'])
		end

		Property.where(query, data)
	end

	def self.sliders_by_type(type = "L", user_id = nil)
		# L = Pré-lançamento, A = Em andamento, P = Pronto
		conditions = {}
		conditions.merge!(user_id: user_id) unless user_id.nil?

		case type
			when "L"
				conditions.merge!(stage: 0)
			when "A"
				conditions.merge!(stage: 1..99)
			when "P"
				conditions.merge!(stage: 100)
		end

  	properties = Property.where(conditions)

		slide = self.slide_show

		properties.each do |property|
			slide[:items].push(property.slider)
		end
		return slide
	end

	def self.slide_show
		{
			name: "",
			title: "",
			title_link: '',
			follow_button: false,
			progress_bar: false,
			can_edit: false,
			show_user: false,
			items: []
		}
	end

	def info_html
		self.info.nil? ? self.info : self.info.html_safe
	end

	def doubts_html
		self.doubts.nil? ? self.doubts : self.doubts.html_safe
	end

	def stage_info_html
		self.stage_info.nil? ? self.stage_info : self.stage_info.html_safe
	end

	def stage_string
		stage = self.stage

		return "Pré-lançamento" if stage == 0
		return "Em andamento" if stage > 0 && stage < 100
		return "Pronto" if stage == 100
	end

	def slider
		image = self.pictures.find_by_kind( :cover )
		image = image.nil? ? ApplicationController.helpers.image_url( 'missing.png' ) : self.pictures.find_by_kind( :cover ).file.url( :thumb )

		{
			id: self.id,
			name: self.name,
			stage: self.stage,
			stage_percent: "#{self.stage}%",
			image_src: image,
			realtor_src: '',
			following: false,
			accompaniment_id: '',
			user_src: '',
			url: "/properties/#{self.id}",
			edit_url: '#'
		}
	end

	def single_views
		self.views.select(:user_id).distinct.count
	end

	private
		def destroy_notifications
				Notification.where(notificable_id: self.id, notificable_type: 'Property').destroy_all
		end

		def create_notification
			if visibility == 'visible'
				unless notified
					User.where( role_id: 3 ).each do |user|
				  		Notification.create!({
				  			notificable_id: id,
				  			notificable_type: 'Property',
				  			user_id: user.id,
				  			modifier: 'published'
				  		})
				  	end
				  	Property.find( id ).update_column( :notified, true )
				end
		  	end
		  	if status == 'sold'
		  		Accompaniment.where( property_id: id ).each do |accompaniment|
			  		Notification.create!({
			  			notificable_id: id,
			  			notificable_type: 'Property',
			  			user_id: accompaniment.user_id,
			  			modifier: 'sold'
			  		})
		  		end
		  	end
	  	end

end
