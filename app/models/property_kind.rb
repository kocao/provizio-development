class PropertyKind < ApplicationRecord
	#has_many :properties
	validates :display_name, presence: true
	before_save :set_name
	def set_name
		self.name = self.display_name.parameterize
	end
end
