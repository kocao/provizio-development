class Role < ApplicationRecord
	has_many :users
	belongs_to :creator, class_name: 'User', foreign_key: 'created_by', optional: true
	validates :display_name, presence: true
	scope :alphabetically, -> (order = 'ASC') { order("display_name #{order}") }
	scope :registrable, -> (order = 'ASC') { where( "roles.name != 'omniauth'" ).order("display_name #{order}") }
	before_save :set_name
	def set_name
		if created_by > 0
			self.name = self.display_name.parameterize
		end
	end
end
