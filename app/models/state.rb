class State < ApplicationRecord
  has_many :cities

  validates :acronym, presence: true, uniqueness: true
  validates :name, presence: true, uniqueness: true
end
