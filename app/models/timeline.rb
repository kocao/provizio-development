class Timeline < ApplicationRecord
	belongs_to :timelinable, polymorphic: true
	belongs_to :user
	belongs_to :property, optional: true
end
