class User < ApplicationRecord
  	# Include default devise modules. Others available are:
  	# :confirmable, :lockable, :timeoutable and :omniauthable
  	devise  :database_authenticatable,
  			:registerable,
         	:recoverable,
         	:rememberable,
         	:trackable,
         	:validatable,
         	:omniauthable,
         	:omniauth_providers => [:google_oauth2, :facebook]
  after_create :add_group
	belongs_to :role
	belongs_to :state, optional: true
	belongs_to :city, optional: true
  has_many :groups
  has_many :groups_user, dependent: :destroy
  has_many :pictures, dependent: :destroy
  has_many :notification, dependent: :destroy
  has_many :views, dependent: :destroy

	#validates_acceptance_of :terms, accept: 1, :if => "role_id == 2"
	#validates_acceptance_of :terms, :allow_nil => false, :accept => 1, :on => :create, :if => "role_id == 2"
	validates :terms, on: :create, acceptance: { allow_nil: false, accept: ['TRUE', 'accepted', '1', 1], message: 'Você deve aceitar os termos de uso do site para se registrar.' }, if: "[2,3,4].include?( role_id )"

	has_one :investor_realtor, dependent: :destroy
	has_one :realtor, class_name: 'User', foreign_key: 'realtor_id', through: :investor_realtor

	has_many :identities
	has_many :realtor_customers
	has_many :customers, class_name: 'User', foreign_key: 'customer_id', through: :realtor_customers

	#has_many :realtor_contacts
	#has_many :contacts, class_name: 'User', foreign_key: 'contact_id', through: :realtor_contacts
	has_many :contacts

	has_many :accompaniments, dependent: :destroy
	has_many :properties, through: :accompaniments
	#has_and_belongs_to_many :permissions
	has_attached_file :avatar, styles: { thumb: "140x140#" }, default_url: ":style/missing-avatar.png"
	has_attached_file :marca, styles: { thumb: "140x140#" }, default_url: ":style/missing-avatar.png"
	has_attached_file :back_image, styles: { thumb: "140x140#" }
	validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/, :size => { :less_than => 2.megabyte }

	before_create :only_admin_can_create_admins

  def self.users_with_properties
    self.where("exists (select * from properties where properties.user_id = users.id and properties.visibility = 1)")
  end

	def self.search( search )
		where( "user.email ILIKE ? OR user.name ILIKE ? OR user.bio ILIKE ?", "%#{search}%", "%#{search}%", "%#{search}%" )
	end

	#def self.from_omniauth(auth)
	#  where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
	#    user.email = auth.info.email
	#    user.role_id = 5
	#    user.password = Devise.friendly_token[0,20]
	#    user.name = auth.info.name   # assuming the user model has a name
	#    user.avatar = auth.info.image # assuming the user model has an image
	#    # If you are using confirmable and the provider(s) you use validate emails,
	#    # uncomment the line below to skip the confirmation emails.
	#    # user.skip_confirmation!
	#  end
	#end

	def self.from_omniauth( auth )
		identity = Identity.where( provider: auth.provider, uid: auth.uid )
		if identity.empty?
			user = User.find_by_email( auth.info.email )
			unless user.present?
				user = User.create({
					email: auth.info.email,
				    role_id: 5,
				    password: Devise.friendly_token[0,20],
				    name: auth.info.name,
				    avatar: auth.info.image
				})
				user.identities.create( provider: auth.provider, uid: auth.uid )
				return user
			else
				Identity.create( { provider: auth.provider, uid: auth.uid, user_id: user.id } )
				return user
			end
		else
			return identity.first.user
		end
	end

	#We remove this filter when an administrator try to create a new admin, this also help us to avoid people from trying to register a new admin without permission
	def only_admin_can_create_admins
		self.role_id = role_id == 1 ? 5 : role_id
	end

	def birthdate_format( format = "%d/%m/%Y" )
		if self.has_attribute?( :birthdate ) && ! self[:birthdate].blank?
			self[:birthdate].strftime( format )
		end
	end

  def availables_roles
    Role.where("id not in (?)", [1,5,self.role_id])
  end

  def self.investors
    User.where(role_id: 3)
  end


	scope :with_role, lambda{|role_name| includes(:role).where(:roles => {:name => role_name}) }
	scope :alphabetically, -> (order = 'ASC') { order("name #{order}") }
	scope :search, -> (q) { where('lower(name) LIKE :q OR email LIKE :q', q: "%#{q.downcase}%") }
	scope :search_sort, -> (order_by, order = 'ASC') {order_by == 'alphabetically' ? alphabetically(order) : order("#{order_by} #{order}")}

  private
  def add_group
    invite = Invite.where(email: self.email).last
    unless invite.blank?
      @group = Group.find(invite.group_id)
      @group.groups_user.new(user_id: self.id)
      @group.save
      accompaniment = Accompaniment.new(property_id: @group.property.id, user_id: self.id)
      accompaniment.save
      Invite.where(email: self.email).destroy_all
    end
  end
end
