json.extract! group, :id, :name, :property_id, :user_id, :created_at, :updated_at
json.url group_url(group, format: :json)
