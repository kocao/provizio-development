class ActivitiesWorker

    def perform
        Activity.where( "advised = false AND warn = true AND DATE_PART( 'hours', scheduled_to - '#{Time.current.strftime( '%Y-%m-%d %H:%M' )}') >= 1" ).each do |activity|
            ActivityMailer::warn_customer(activity).deliver_now
            ActivityMailer::warn_realtor(activity).deliver_now
            activity.update_attribute( :advised, true )
        end
    end

end