class ReservationsWorker

    def perform
        Deal.where( "expires_at <= '#{Time.current.strftime( '%Y-%m-%d %H:%M' )}'" ).where( status: :reserved ).each do |deal|
            deal.update_attribute( :status, :expired )
            unless deal.plant.nil?
            	deal.plant.update_spot( deal.spot, 'available' )
            end
        end
    end

end