require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Provizio
  class Application < Rails::Application
    config.i18n.default_locale = :'pt-BR'
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]
    config.autoload_paths += [
      "#{Rails.root}/lib",
      "#{Rails.root}/app/presenters",
      "#{Rails.root}/app/services"
    ]
    config.before_configuration do
	  env_file = File.join(Rails.root, 'config', 'local_env.yml')
	  YAML.load(File.open(env_file)).each do |key, value|
	    ENV[key.to_s] = value
	  end if File.exists?(env_file)
	end
  end
end
