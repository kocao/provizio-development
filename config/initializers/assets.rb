# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += %w( submodules/squares/js/squares-renderer.js )
Rails.application.config.assets.precompile += %w( submodules/squares/js/squares.js )
Rails.application.config.assets.precompile += %w( submodules/squares/js/squares-elements-jquery.js )
Rails.application.config.assets.precompile += %w( submodules/squares/js/squares-controls.js )
Rails.application.config.assets.precompile += %w( submodules/wcp-editor/js/wcp-editor.js )
Rails.application.config.assets.precompile += %w( submodules/wcp-editor/js/wcp-editor-controls.js )
Rails.application.config.assets.precompile += %w( submodules/wcp-tour/js/wcp-tour.js )
Rails.application.config.assets.precompile += %w( submodules/wcp-compress/js/wcp-compress.js )
Rails.application.config.assets.precompile += %w( submodules/wcp-icons/js/wcp-icons.js )
Rails.application.config.assets.precompile += %w( js/image-map-pro-defaults.js )
Rails.application.config.assets.precompile += %w( js/image-map-pro-editor.js )
Rails.application.config.assets.precompile += %w( js/image-map-pro-editor-content.js )
Rails.application.config.assets.precompile += %w( js/image-map-pro-editor-local-storage.js )
Rails.application.config.assets.precompile += %w( js/image-map-pro-editor-init-jquery.js )
Rails.application.config.assets.precompile += %w( js/image-map-pro )
Rails.application.config.assets.precompile += %w( js/image-map-pro.min )

%w(eot svg ttf woff woff2).each do |ext|
  Rails.application.config.assets.precompile << "fontawesome-webfont.#{ext}"
end