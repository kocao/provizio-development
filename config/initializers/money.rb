MoneyRails.configure do |config|
  config.default_currency = :brl
  config.no_cents_if_whole = false
end
