Rails.application.routes.draw do

  resources :groups
  get 'pictures/create'

  devise_for :users, :path_prefix => 'account', controllers: {
    registrations: 'users/registrations',
    omniauth_callbacks: 'users/omniauth_callbacks'
  }
  # new_user_session GET      /account/users/sign_in(.:format)                     devise/sessions#new
  get '/accept_invite/:token', to: 'users#accept_invite', as: 'accept_invite'
  # get 'pages/sou_investidor', to: 'pages#investor', as: :investor

  resources :users do
    collection do
      get 'change_profile/:id', to: 'users#change_profile'
    end
  end
  resources :roles
  resources :deals
  resources :meta
  resources :image_maps
  resources :spots, only: [:create, :update, :destroy]
  resources :places, only: [:create, :update, :destroy]
  resources :contents
  resources :contacts, only: [:create, :update, :destroy]
  resources :properties do
    collection do
      get :search
      post :search, to: 'properties#search_filter'
    end
  end
  resources :messages
  resources :notes, only: [:create, :destroy]
  resources :activities, only: [:create, :destroy]
  resources :pictures, only: [:index, :create, :destroy, :update]
  resources :documents, only: [:create, :destroy, :update]
  resources :accompaniments, only: [:create, :destroy]
  resources :attachments, only: [:create, :update, :destroy]
  resources :timelines, only: [:index]
  resources :property_kinds
  resources :groups do
    collection do
      post :add_investor, to: 'groups#add_investor'
      post :invite_investor, to: 'groups#invite_investor'
      delete 'destroy_investor/:id', to: 'groups#destroy_investor'
      delete 'destroy_invite/:id', to: 'groups#destroy_invite'
    end
  end


  get 'pages/index'
  get 'pages/sobre', to: 'pages#about', as: :about
  get 'pages/sou_investidor', to: 'pages#investor', as: :investor
  get 'pages/sou_consultor', to: 'pages#realtor', as: :realtor
  get 'pages/sou_parceiro', to: 'pages#partner', as: :partner

  get 'panel/search_results', to: 'panel#search_results', as: :search_results
  get 'panel/my_clients', to: 'panel#my_clients', as: :my_clients
  get 'panel/my_contacts', to: 'panel#my_contacts', as: :my_contacts
  get 'panel/my_profile', to: 'panel#my_profile', as: :my_profile
  get 'panel/my_profile_cc/:id', to: 'panel#my_profile_cc', as: :my_profile_cc
  get 'panel/public_profile/:id', to: 'panel#public_profile', as: :public_profile
  get 'panel/add_contact', to: 'panel#add_contact', as: :add_contact
  get 'panel/edit_contact/:id', to: 'panel#edit_contact', as: :edit_contact
  get 'panel/edit_profile', to: 'panel#edit_profile', as: :edit_profile
  get 'panel/edit_profile_cc', to: 'panel#edit_profile_cc', as: :edit_profile_cc
  get 'panel/remove_contact/:id', to: 'panel#remove_contact', as: :remove_contact
  get 'panel/my_realtor', to: 'panel#my_realtor', as: :my_realtor
  get 'panel/change_realtor/:id', to: 'panel#change_realtor', as: :change_realtor
  get 'panel/customer_activities/:id', to: 'panel#customer_activities', as: :customer_activities
  get 'panel/notifications', to: 'panel#notifications', as: :notifications
  get 'panel/my_ventures', to: 'panel#my_ventures', as: :my_ventures
  get 'panel/my_deals', to: 'panel#my_deals', as: :my_deals
  get 'panel/view_docs', to: 'panel#view_docs', as: :view_docs
  get 'panel/notify_gallery', to: 'panel#notify_gallery', as: :notify_gallery
  get 'panel/change_deal_status', to: 'panel#change_deal_status', as: :change_deal_status
  get 'panel/mark_notifications_as_visualiazed', to: 'panel#mark_notifications_as_visualiazed', as: :mark_notifications_as_visualiazed
  get 'properties/change_plants_order/:id', to: 'properties#change_plants_order', as: :change_plants_order
  # get 'properties/busca', to: 'properties#index_busca', controller: 'properties'

  authenticated :user, ->(u) { u.role.name == 'admin' } do
    root "admin#index", as: :admin_root
  end

  get 'admin/manage_users', to: 'admin#manage_users', as: :manage_users
  get 'admin/configurations', to: 'admin#configurations', as: :configurations
  get 'admin/edit_pages', to: 'admin#edit_pages', as: :edit_pages
  get 'admin/update_page_attr', to: 'admin#update_page_attr', as: :update_page_attr
  get 'admin/add_page_meta_contents', to: 'admin#add_page_meta_contents', as: :add_page_meta_contents
  get 'admin/deals', to: 'admin#deals', as: :admin_deals

  authenticated :user, ->(u) { u.role.name == 'omniauth' } do
    root "panel#first_login", as: :first_login
    get 'panel/choose_role', to: 'panel#choose_role', as: :choose_role
  end

  authenticated :user do
    root 'properties#index', as: :authenticated_root
  end

  root 'pages#index'

  namespace :autocomplete, constraints: { format: :json } do
    resources :cities, only: :index
  end

  #get '*path' => redirect('/')

end
