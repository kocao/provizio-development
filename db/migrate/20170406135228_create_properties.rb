class CreateProperties < ActiveRecord::Migration[5.0]
  def change
    create_table :properties do |t|
    	t.belongs_to :user, index: true
    	t.integer :kind
    	t.string :name
    	t.text :info
    	t.text :doubts
    	t.monetize :value, null: false, default: 0
    	t.text :payment_conditions
    	t.integer :stage, null: false, default: 0
        t.integer :visibility, null: false, default: 0
    	t.text :stage_info
        t.string :address
        t.string :city
        t.string :state
        t.string :zipcode
      t.timestamps
    end
    add_foreign_key :properties, :users
  end
end
