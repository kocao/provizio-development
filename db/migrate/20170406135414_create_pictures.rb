class CreatePictures < ActiveRecord::Migration[5.0]
  def change
    create_table :pictures do |t|
    	t.belongs_to :property, index: true
    	t.attachment :file
    	t.integer :kind, null: false, default: 0
    	t.integer :sequence, null: false, default: 0
      t.timestamps
    end
    add_foreign_key :pictures, :properties
  end
end
