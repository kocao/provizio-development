class CreateAccompaniments < ActiveRecord::Migration[5.0]
  def change
    create_table :accompaniments do |t|
    	t.belongs_to :user, index: true
      	t.belongs_to :property, index: true
      	t.timestamps
    end
    add_foreign_key :accompaniments, :users
    add_foreign_key :accompaniments, :properties
  end
end
