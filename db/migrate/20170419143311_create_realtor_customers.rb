class CreateRealtorCustomers < ActiveRecord::Migration[5.0]
  def change
    create_table :realtor_customers do |t|
    	t.integer :user_id, index: true
      	t.integer :customer_id, index: true
    	t.timestamps
    end
    add_foreign_key :realtor_customers, :users
  end
end
