class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages do |t|
    	t.belongs_to :user, index: true
      t.integer :prev_id
    	t.string :to
    	t.string :from
    	t.string :subject
    	t.text :text
    	t.integer :box, default: 0
    	t.boolean :read, default: false
    	t.datetime :read_at
      t.timestamps
    end
    add_foreign_key :messages, :users
  end
end
