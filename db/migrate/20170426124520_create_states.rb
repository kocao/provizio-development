class CreateStates < ActiveRecord::Migration[5.0]
  def change
    create_table :states do |t|
      t.string :acronym, index: true
      t.string :name, index: true
      t.string :region
    end
  end
end
