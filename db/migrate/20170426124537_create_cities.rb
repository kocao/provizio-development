class CreateCities < ActiveRecord::Migration[5.0]
  def change
    create_table :cities do |t|
      t.string :name, index: true
      t.boolean :capital
      t.references :state, foreign_key: :state_id
    end
  end
end
