class AddFieldsToUsers < ActiveRecord::Migration[5.0]
  def change
    change_table :users do |t|
      t.attachment :avatar
      t.belongs_to :state
      t.belongs_to :city
      t.string :phone
      t.string :cellphone
      t.string :address
      t.string :job
      t.text :bio
    end
    add_foreign_key :users, :cities
  end
end
