class CreateRealtorContacts < ActiveRecord::Migration[5.0]
  def change
    create_table :realtor_contacts do |t|
    	t.integer :user_id, index: true
      	t.integer :contact_id, index: true
    	t.timestamps
    end
    add_foreign_key :realtor_contacts, :users
  end
end
