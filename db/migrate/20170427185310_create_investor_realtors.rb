class CreateInvestorRealtors < ActiveRecord::Migration[5.0]
  	def change
    	create_table :investor_realtors do |t|
    		t.integer :user_id, index: true
      		t.integer :realtor_id, index: true
    		t.timestamps
    	end
    	add_foreign_key :investor_realtors, :users
  	end
end
