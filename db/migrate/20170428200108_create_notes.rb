class CreateNotes < ActiveRecord::Migration[5.0]
	def change
    	create_table :notes do |t|
    		t.text :text, default: ''
    		t.belongs_to :user, index: true
    		t.belongs_to :property, index: true
    		t.integer :created_by
      		t.timestamps
    	end
    	add_foreign_key :notes, :users
    	add_foreign_key :notes, :properties
  	end
end
