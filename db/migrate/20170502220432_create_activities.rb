class CreateActivities < ActiveRecord::Migration[5.0]
  	def change
    	create_table :activities do |t|
    		t.string :title, default: ''
    		t.text :text, default: ''
    		t.datetime :scheduled_to
    		t.belongs_to :user, index: true
    		t.belongs_to :property, index: true
    		t.integer :created_by
        t.boolean :warn, default: false
      	t.timestamps
    	end
    	add_foreign_key :activities, :users
    	add_foreign_key :activities, :properties
  	end
end