class CreateAttachments < ActiveRecord::Migration[5.0]
  def change
    create_table :attachments do |t|
    	t.references :attachable, polymorphic: true, index: true
    	t.belongs_to :property, index: true
    	t.integer :uploaded_by
    	t.attachment :file
      	t.string :modifier
    	t.integer :sequence, default: 0
      	t.timestamps
    end
  end
end
