class CreateTimelines < ActiveRecord::Migration[5.0]
 	def change
    	create_table :timelines do |t|
    		t.references :timelinable, polymorphic: true, index: true
    		t.belongs_to :user, index: true
    		t.belongs_to :property, index: true
      		t.timestamps
    	end
    	add_foreign_key :timelines, :users
    	add_foreign_key :timelines, :properties
  	end
end
