class AddPropertyIdToMessage < ActiveRecord::Migration[5.0]
  def change
  	 add_column :messages, :property_id, :integer, index: true
  	 add_foreign_key :messages, :properties
  end
end
