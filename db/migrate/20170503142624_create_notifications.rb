class CreateNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :notifications do |t|
		t.references :notificable, polymorphic: true, index: true
    	t.belongs_to :user, index: true
    	t.boolean :visualiazed
    	t.string :modifier
      	t.timestamps
    end
    add_foreign_key :notifications, :users
  end
end
