class AddExtraFieldsToProperty < ActiveRecord::Migration[5.0]
  def change
  	add_column :properties, :status, :integer, default: 0
  	add_column :properties, :notified, :boolean, default: false
  end
end
