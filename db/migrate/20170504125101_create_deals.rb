class CreateDeals < ActiveRecord::Migration[5.0]
  def change
    create_table :deals do |t|
    	t.integer :dealer_id, index: true
    	t.integer :customer_id, index: true
    	t.belongs_to :property, index: true
    	t.integer :status
      	t.timestamps
    end
    add_foreign_key :deals, :users, column: :dealer_id
    add_foreign_key :deals, :users, column: :customer_id
    add_foreign_key :deals, :properties
  end
end
