class CreateContacts < ActiveRecord::Migration[5.0]
  def change
    create_table :contacts do |t|
    	t.string :name
    	t.string :email
    	t.attachment :avatar
    	t.belongs_to :user
	    t.belongs_to :state
	    t.belongs_to :city
	    t.string :phone
	    t.string :cellphone
	    t.string :address
	    t.string :job
	    t.text :bio
      	t.timestamps
    end
    add_foreign_key :contacts, :cities
    add_foreign_key :contacts, :users
  end
end
