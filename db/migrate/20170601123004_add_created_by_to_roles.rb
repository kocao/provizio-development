class AddCreatedByToRoles < ActiveRecord::Migration[5.0]
  def change
  	add_column :roles, :created_by, :integer, default: 0
  end
end
