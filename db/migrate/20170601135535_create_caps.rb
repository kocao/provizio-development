class CreateCaps < ActiveRecord::Migration[5.0]
  def change
    create_table :caps do |t|
    	t.string :name
    	t.string :display_name
    	t.string :resource
      	t.timestamps
    end
  end
end
