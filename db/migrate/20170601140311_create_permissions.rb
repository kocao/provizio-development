class CreatePermissions < ActiveRecord::Migration[5.0]
  def change
    create_table :permissions do |t|
     	t.references :permissionable, polymorphic: true, index: true
     	t.belongs_to :cap, index: true
      	t.timestamps
    end
    add_foreign_key :permissions, :caps
  end
end
