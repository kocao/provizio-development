class CreatePages < ActiveRecord::Migration[5.0]
  def change
    create_table :pages do |t|
    	t.string :name
    	t.string :display_name
    	t.string :title
    	t.text :description
    	t.text :keywords
      	t.timestamps
    end
  end
end
