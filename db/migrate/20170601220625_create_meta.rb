class CreateMeta < ActiveRecord::Migration[5.0]
  def change
    create_table :meta do |t|
    	t.belongs_to :page
    	t.string :name
    	t.text :value
        t.text :label
        t.text :field_type
    	t.integer :order, default: 0
      	t.timestamps
    end
    add_foreign_key :meta, :pages
  end
end
