class CreateContents < ActiveRecord::Migration[5.0]
  def change
    create_table :contents do |t|
    	t.belongs_to :metum
    	t.string :name
    	t.text :value
        t.text :label
        t.text :field_type
    	t.integer :order, default: 0
      	t.timestamps
    end
    add_foreign_key :contents, :meta
  end
end
