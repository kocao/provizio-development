class AddFieldsToDeals < ActiveRecord::Migration[5.0]
	def change
  		add_monetize :deals, :down_payment, default: 0, null: false
  		add_column :deals, :installments, :integer, default: 0
  		add_column :deals, :is_change, :boolean, default: false
  		add_column :deals, :exchange_object, :string, default: ""
  		add_column :deals, :details, :text, default: ""
  		add_column :deals, :plant_id, :integer
  		add_column :deals, :spot, :text, default: ""
  	end
end
