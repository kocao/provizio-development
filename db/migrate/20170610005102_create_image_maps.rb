class CreateImageMaps < ActiveRecord::Migration[5.0]
	def change
    	create_table :image_maps do |t|
    		t.belongs_to :picture, index: true
    		t.json :map_data
      		t.timestamps
    	end
    	add_foreign_key :image_maps, :pictures
  	end
end
