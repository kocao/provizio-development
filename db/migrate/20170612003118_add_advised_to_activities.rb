class AddAdvisedToActivities < ActiveRecord::Migration[5.0]
  def change
  	add_column :activities, :advised, :boolean, default: false
  end
end
