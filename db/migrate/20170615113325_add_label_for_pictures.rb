class AddLabelForPictures < ActiveRecord::Migration[5.0]
  def change
  	add_column :pictures, :label, :text, default: ''
  end
end
