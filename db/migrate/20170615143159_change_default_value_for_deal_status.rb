class ChangeDefaultValueForDealStatus < ActiveRecord::Migration[5.0]
  def up
  	change_column_default(:deals, :status, 0)
  end
  def down
  	change_column_default(:deals, :status, nil)
  end
end
