class CreateDocuments < ActiveRecord::Migration[5.0]
  	def change
    	create_table :documents do |t|
    		t.belongs_to :property
    		t.text :name
    		t.attachment :file
      		t.timestamps
    	end
    	add_foreign_key :documents, :properties
  	end
end
