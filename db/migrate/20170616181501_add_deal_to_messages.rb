class AddDealToMessages < ActiveRecord::Migration[5.0]
  def change
  	add_column :messages, :modifier, :text, default: ''
  	add_column :messages, :deal_id, :integer
  	add_index  :messages, :deal_id
  end
end