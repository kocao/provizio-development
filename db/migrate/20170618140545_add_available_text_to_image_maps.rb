class AddAvailableTextToImageMaps < ActiveRecord::Migration[5.0]
  def change
  	add_column :image_maps, :available_text, :text, default: ''
  end
end
