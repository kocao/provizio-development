class AddMoreFieldsToUser < ActiveRecord::Migration[5.0]
  def change
  	add_column :users, :cpf, :text, default: ''
  	add_column :users, :cnpj, :text, default: ''
  	add_column :users, :birthdate, :date, default: ''
  	add_column :users, :ready, :boolean, default: false
  end
end
