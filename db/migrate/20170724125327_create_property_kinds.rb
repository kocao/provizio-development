class CreatePropertyKinds < ActiveRecord::Migration[5.0]
	def change
    	create_table :property_kinds do |t|
    		t.string :name
    		t.string :display_name
      		t.timestamps
    	end
  	end
end
