class AddKindForeignKeyOnProperties < ActiveRecord::Migration[5.0]
  def change
  	add_foreign_key :properties, :property_kinds, column: :kind, primary_key: :id
  end
end