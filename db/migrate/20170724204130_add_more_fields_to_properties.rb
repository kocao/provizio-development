class AddMoreFieldsToProperties < ActiveRecord::Migration[5.0]
	def change
  		add_column :properties, :accept_other_payment_methods, :boolean, default: false
  		add_column :properties, :other_payment_methods, :text, default: ''
  		add_column :properties, :reservation_time, :integer, default: 0
  	end
end
