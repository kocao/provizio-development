class AddExpiresAtToDeals < ActiveRecord::Migration[5.0]
  def change
  	add_column :deals, :expires_at, :datetime
  	add_column :deals, :payment_method, :text
  	add_column :deals, :indication, :boolean, default: false
  	add_monetize :deals, :total_value, default: 0, null: false
  end
end
