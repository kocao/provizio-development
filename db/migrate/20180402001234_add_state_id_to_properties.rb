class AddStateIdToProperties < ActiveRecord::Migration[5.0]
  def change
    add_column :properties, :state_id, :integer
    add_column :properties, :city_id, :integer

  end
end
