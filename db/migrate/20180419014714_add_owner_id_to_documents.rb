class AddOwnerIdToDocuments < ActiveRecord::Migration[5.0]
  def change
    add_column :documents, :owner_id, :integer
  end
end
