class RemoveForeignKeyPropertyFromPicture < ActiveRecord::Migration[5.0]
  def change
    remove_foreign_key :pictures, column: :property_id
  end
end
