class AddBackImageToUser < ActiveRecord::Migration[5.0]
  def change
    add_attachment 'users', 'back_image'
  end
end
