class AddOriginalRoleIdToUser < ActiveRecord::Migration[5.0]
  def change
    add_column 'users', 'original_role_id', 'integer'
  end
end
