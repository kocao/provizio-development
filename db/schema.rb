# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180507002508) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "unaccent"

  create_table "accompaniments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "property_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["property_id"], name: "index_accompaniments_on_property_id", using: :btree
    t.index ["user_id"], name: "index_accompaniments_on_user_id", using: :btree
  end

  create_table "activities", force: :cascade do |t|
    t.string   "title",        default: ""
    t.text     "text",         default: ""
    t.datetime "scheduled_to"
    t.integer  "user_id"
    t.integer  "property_id"
    t.integer  "created_by"
    t.boolean  "warn",         default: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.boolean  "advised",      default: false
    t.index ["property_id"], name: "index_activities_on_property_id", using: :btree
    t.index ["user_id"], name: "index_activities_on_user_id", using: :btree
  end

  create_table "attachments", force: :cascade do |t|
    t.string   "attachable_type"
    t.integer  "attachable_id"
    t.integer  "property_id"
    t.integer  "uploaded_by"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.string   "modifier"
    t.integer  "sequence",          default: 0
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["attachable_type", "attachable_id"], name: "index_attachments_on_attachable_type_and_attachable_id", using: :btree
    t.index ["property_id"], name: "index_attachments_on_property_id", using: :btree
  end

  create_table "caps", force: :cascade do |t|
    t.string   "name"
    t.string   "display_name"
    t.string   "resource"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "cities", force: :cascade do |t|
    t.string  "name"
    t.boolean "capital"
    t.integer "state_id"
    t.index ["name"], name: "index_cities_on_name", using: :btree
    t.index ["state_id"], name: "index_cities_on_state_id", using: :btree
  end

  create_table "contacts", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.integer  "user_id"
    t.integer  "state_id"
    t.integer  "city_id"
    t.string   "phone"
    t.string   "cellphone"
    t.string   "address"
    t.string   "job"
    t.text     "bio"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["city_id"], name: "index_contacts_on_city_id", using: :btree
    t.index ["state_id"], name: "index_contacts_on_state_id", using: :btree
    t.index ["user_id"], name: "index_contacts_on_user_id", using: :btree
  end

  create_table "contents", force: :cascade do |t|
    t.integer  "metum_id"
    t.string   "name"
    t.text     "value"
    t.text     "label"
    t.text     "field_type"
    t.integer  "order",      default: 0
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["metum_id"], name: "index_contents_on_metum_id", using: :btree
  end

  create_table "deals", force: :cascade do |t|
    t.integer  "dealer_id"
    t.integer  "customer_id"
    t.integer  "property_id"
    t.integer  "status",                default: 0
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "down_payment_centavos", default: 0,     null: false
    t.string   "down_payment_currency", default: "BRL", null: false
    t.integer  "installments",          default: 0
    t.boolean  "is_change",             default: false
    t.string   "exchange_object",       default: ""
    t.text     "details",               default: ""
    t.integer  "plant_id"
    t.text     "spot",                  default: ""
    t.datetime "expires_at"
    t.text     "payment_method"
    t.boolean  "indication",            default: false
    t.integer  "total_value_centavos",  default: 0,     null: false
    t.string   "total_value_currency",  default: "BRL", null: false
    t.index ["customer_id"], name: "index_deals_on_customer_id", using: :btree
    t.index ["dealer_id"], name: "index_deals_on_dealer_id", using: :btree
    t.index ["property_id"], name: "index_deals_on_property_id", using: :btree
  end

  create_table "documents", force: :cascade do |t|
    t.integer  "property_id"
    t.text     "name"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "owner_id"
    t.index ["property_id"], name: "index_documents_on_property_id", using: :btree
  end

  create_table "groups", force: :cascade do |t|
    t.string   "name"
    t.integer  "property_id"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["property_id"], name: "index_groups_on_property_id", using: :btree
    t.index ["user_id"], name: "index_groups_on_user_id", using: :btree
  end

  create_table "groups_users", force: :cascade do |t|
    t.integer  "group_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id"], name: "index_groups_users_on_group_id", using: :btree
    t.index ["user_id"], name: "index_groups_users_on_user_id", using: :btree
  end

  create_table "identities", force: :cascade do |t|
    t.string  "uid"
    t.string  "provider"
    t.integer "user_id"
    t.index ["user_id"], name: "index_identities_on_user_id", using: :btree
  end

  create_table "image_maps", force: :cascade do |t|
    t.integer  "picture_id"
    t.json     "map_data"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.text     "available_text", default: ""
    t.index ["picture_id"], name: "index_image_maps_on_picture_id", using: :btree
  end

  create_table "investor_realtors", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "realtor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["realtor_id"], name: "index_investor_realtors_on_realtor_id", using: :btree
    t.index ["user_id"], name: "index_investor_realtors_on_user_id", using: :btree
  end

  create_table "invites", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "token"
    t.integer  "group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id"], name: "index_invites_on_group_id", using: :btree
  end

  create_table "messages", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "prev_id"
    t.string   "to"
    t.string   "from"
    t.string   "subject"
    t.text     "text"
    t.integer  "box",         default: 0
    t.boolean  "read",        default: false
    t.datetime "read_at"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "property_id"
    t.text     "modifier",    default: ""
    t.integer  "deal_id"
    t.index ["deal_id"], name: "index_messages_on_deal_id", using: :btree
    t.index ["user_id"], name: "index_messages_on_user_id", using: :btree
  end

  create_table "meta", force: :cascade do |t|
    t.integer  "page_id"
    t.string   "name"
    t.text     "value"
    t.text     "label"
    t.text     "field_type"
    t.integer  "order",      default: 0
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["page_id"], name: "index_meta_on_page_id", using: :btree
  end

  create_table "notes", force: :cascade do |t|
    t.text     "text",        default: ""
    t.integer  "user_id"
    t.integer  "property_id"
    t.integer  "created_by"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.index ["property_id"], name: "index_notes_on_property_id", using: :btree
    t.index ["user_id"], name: "index_notes_on_user_id", using: :btree
  end

  create_table "notifications", force: :cascade do |t|
    t.string   "notificable_type"
    t.integer  "notificable_id"
    t.integer  "user_id"
    t.boolean  "visualiazed"
    t.string   "modifier"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["notificable_type", "notificable_id"], name: "index_notifications_on_notificable_type_and_notificable_id", using: :btree
    t.index ["user_id"], name: "index_notifications_on_user_id", using: :btree
  end

  create_table "pages", force: :cascade do |t|
    t.string   "name"
    t.string   "display_name"
    t.string   "title"
    t.text     "description"
    t.text     "keywords"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "permissions", force: :cascade do |t|
    t.string   "permissionable_type"
    t.integer  "permissionable_id"
    t.integer  "cap_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["cap_id"], name: "index_permissions_on_cap_id", using: :btree
    t.index ["permissionable_type", "permissionable_id"], name: "index_permissions_on_permissionable_type_and_permissionable_id", using: :btree
  end

  create_table "pictures", force: :cascade do |t|
    t.integer  "property_id"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.integer  "kind",              default: 0,  null: false
    t.integer  "sequence",          default: 0,  null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.text     "label",             default: ""
    t.integer  "user_id"
    t.index ["property_id"], name: "index_pictures_on_property_id", using: :btree
  end

  create_table "properties", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "kind"
    t.string   "name"
    t.text     "info"
    t.text     "doubts"
    t.integer  "value_centavos",               default: 0,     null: false
    t.string   "value_currency",               default: "BRL", null: false
    t.text     "payment_conditions"
    t.integer  "stage",                        default: 0,     null: false
    t.integer  "visibility",                   default: 0,     null: false
    t.text     "stage_info"
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.string   "zipcode"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "status",                       default: 0
    t.boolean  "notified",                     default: false
    t.boolean  "accept_other_payment_methods", default: false
    t.text     "other_payment_methods",        default: ""
    t.integer  "reservation_time",             default: 0
    t.integer  "state_id"
    t.integer  "city_id"
    t.text     "district"
    t.index ["user_id"], name: "index_properties_on_user_id", using: :btree
  end

  create_table "property_kinds", force: :cascade do |t|
    t.string   "name"
    t.string   "display_name"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "realtor_customers", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "customer_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["customer_id"], name: "index_realtor_customers_on_customer_id", using: :btree
    t.index ["user_id"], name: "index_realtor_customers_on_user_id", using: :btree
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "display_name"
    t.integer  "created_by",   default: 0
  end

  create_table "states", force: :cascade do |t|
    t.string "acronym"
    t.string "name"
    t.string "region"
    t.index ["acronym"], name: "index_states_on_acronym", using: :btree
    t.index ["name"], name: "index_states_on_name", using: :btree
  end

  create_table "timelines", force: :cascade do |t|
    t.string   "timelinable_type"
    t.integer  "timelinable_id"
    t.integer  "user_id"
    t.integer  "property_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["property_id"], name: "index_timelines_on_property_id", using: :btree
    t.index ["timelinable_type", "timelinable_id"], name: "index_timelines_on_timelinable_type_and_timelinable_id", using: :btree
    t.index ["user_id"], name: "index_timelines_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                   default: "",    null: false
    t.string   "encrypted_password",      default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",           default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "role_id"
    t.string   "name"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.integer  "state_id"
    t.integer  "city_id"
    t.string   "phone"
    t.string   "cellphone"
    t.string   "address"
    t.string   "job"
    t.text     "bio"
    t.string   "provider"
    t.string   "uid"
    t.text     "creci",                   default: ""
    t.text     "cpf",                     default: ""
    t.text     "cnpj",                    default: ""
    t.date     "birthdate"
    t.boolean  "ready",                   default: false
    t.string   "marca_file_name"
    t.string   "marca_content_type"
    t.integer  "marca_file_size"
    t.datetime "marca_updated_at"
    t.float    "latitude"
    t.float    "longitude"
    t.text     "about_me"
    t.string   "back_image_file_name"
    t.string   "back_image_content_type"
    t.integer  "back_image_file_size"
    t.datetime "back_image_updated_at"
    t.integer  "original_role_id"
    t.index ["city_id"], name: "index_users_on_city_id", using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["role_id"], name: "index_users_on_role_id", using: :btree
    t.index ["state_id"], name: "index_users_on_state_id", using: :btree
  end

  create_table "views", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "property_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["property_id"], name: "index_views_on_property_id", using: :btree
    t.index ["user_id"], name: "index_views_on_user_id", using: :btree
  end

  add_foreign_key "accompaniments", "properties"
  add_foreign_key "accompaniments", "users"
  add_foreign_key "activities", "properties"
  add_foreign_key "activities", "users"
  add_foreign_key "cities", "states"
  add_foreign_key "contacts", "cities"
  add_foreign_key "contacts", "users"
  add_foreign_key "contents", "meta"
  add_foreign_key "deals", "properties"
  add_foreign_key "deals", "users", column: "customer_id"
  add_foreign_key "deals", "users", column: "dealer_id"
  add_foreign_key "documents", "properties"
  add_foreign_key "groups", "properties"
  add_foreign_key "groups", "users"
  add_foreign_key "groups_users", "groups"
  add_foreign_key "groups_users", "users"
  add_foreign_key "identities", "users"
  add_foreign_key "image_maps", "pictures"
  add_foreign_key "investor_realtors", "users"
  add_foreign_key "invites", "groups"
  add_foreign_key "messages", "properties"
  add_foreign_key "messages", "users"
  add_foreign_key "meta", "pages"
  add_foreign_key "notes", "properties"
  add_foreign_key "notes", "users"
  add_foreign_key "notifications", "users"
  add_foreign_key "permissions", "caps"
  add_foreign_key "properties", "property_kinds", column: "kind"
  add_foreign_key "properties", "users"
  add_foreign_key "realtor_customers", "users"
  add_foreign_key "timelines", "properties"
  add_foreign_key "timelines", "users"
  add_foreign_key "users", "cities"
  add_foreign_key "users", "roles"
  add_foreign_key "views", "properties"
  add_foreign_key "views", "users"
end
