PopulateDatabaseWithPropertyKinds.new.populate
PopulateDatabaseWithCitiesAndStates.new(Rails.root.join('db', 'seeds', 'states.json')).populate
PopulateDatabaseWithCaps.new( Rails.root.join( 'db', 'seeds', 'caps.json' ) ).populate
PopulateDatabaseWithRolesPermissions.new( Rails.root.join( 'db', 'seeds', 'roles_permissions.json' ) ).populate
PopulateDatabaseWithUsers.new( Rails.root.join( 'db', 'seeds', 'users.json' ) ).populate
PopulateDatabaseWithProperties.new( Rails.root.join( 'db', 'seeds', 'properties.json' ) ).populate
PopulateDatabaseWithPage.new( Rails.root.join( 'db', 'seeds', 'page_home.json' ), :home ).populate
PopulateDatabaseWithPage.new( Rails.root.join( 'db', 'seeds', 'page_about.json' ), :about ).populate
PopulateDatabaseWithPage.new( Rails.root.join( 'db', 'seeds', 'page_partner.json' ), :partner ).populate
PopulateDatabaseWithPage.new( Rails.root.join( 'db', 'seeds', 'page_realtor.json' ), :realtor ).populate
PopulateDatabaseWithPage.new( Rails.root.join( 'db', 'seeds', 'page_investor.json' ), :investor ).populate
PopulateDatabaseWithPage.new( Rails.root.join( 'db', 'seeds', 'page_terms.json' ), :terms ).populate

RealtorCustomer.create({ user_id: 1, customer_id: 4 })
RealtorCustomer.create({ user_id: 55, customer_id: 12 })
RealtorCustomer.create({ user_id: 56, customer_id: 13 })
RealtorCustomer.create({ user_id: 56, customer_id: 14 })
RealtorCustomer.create({ user_id: 56, customer_id: 15 })
RealtorCustomer.create({ user_id: 56, customer_id: 16 })
RealtorCustomer.create({ user_id: 55, customer_id: 17 })
RealtorCustomer.create({ user_id: 56, customer_id: 18 })
RealtorCustomer.create({ user_id: 55, customer_id: 19 })
RealtorCustomer.create({ user_id: 55, customer_id: 21 })
RealtorCustomer.create({ user_id: 56, customer_id: 22 })
RealtorCustomer.create({ user_id: 55, customer_id: 23 })
RealtorCustomer.create({ user_id: 55, customer_id: 24 })
RealtorCustomer.create({ user_id: 55, customer_id: 25 })
RealtorCustomer.create({ user_id: 55, customer_id: 26 })
RealtorCustomer.create({ user_id: 55, customer_id: 27 })
RealtorCustomer.create({ user_id: 55, customer_id: 28 })
RealtorCustomer.create({ user_id: 56, customer_id: 29 })
RealtorCustomer.create({ user_id: 55, customer_id: 30 })
RealtorCustomer.create({ user_id: 55, customer_id: 31 })
RealtorCustomer.create({ user_id: 57, customer_id: 32 })
RealtorCustomer.create({ user_id: 55, customer_id: 33 })
RealtorCustomer.create({ user_id: 55, customer_id: 35 })
RealtorCustomer.create({ user_id: 57, customer_id: 36 })
RealtorCustomer.create({ user_id: 55, customer_id: 37 })
RealtorCustomer.create({ user_id: 57, customer_id: 38 })
RealtorCustomer.create({ user_id: 57, customer_id: 39 })
RealtorCustomer.create({ user_id: 55, customer_id: 40 })
RealtorCustomer.create({ user_id: 55, customer_id: 41 })
RealtorCustomer.create({ user_id: 55, customer_id: 42 })
RealtorCustomer.create({ user_id: 55, customer_id: 43 })
RealtorCustomer.create({ user_id: 55, customer_id: 44 })
RealtorCustomer.create({ user_id: 55, customer_id: 45 })
RealtorCustomer.create({ user_id: 55, customer_id: 46 })
RealtorCustomer.create({ user_id: 57, customer_id: 47 })
RealtorCustomer.create({ user_id: 55, customer_id: 48 })
RealtorCustomer.create({ user_id: 55, customer_id: 49 })
RealtorCustomer.create({ user_id: 55, customer_id: 20 })
RealtorCustomer.create({ user_id: 55, customer_id: 50 })
RealtorCustomer.create({ user_id: 55, customer_id: 51 })
RealtorCustomer.create({ user_id: 55, customer_id: 52 })
RealtorCustomer.create({ user_id: 55, customer_id: 53 })
RealtorCustomer.create({ user_id: 55, customer_id: 54 })

InvestorRealtor.create({ user_id: 4, realtor_id: 1 })
InvestorRealtor.create({ user_id: 12, realtor_id: 55 })
InvestorRealtor.create({ user_id: 13, realtor_id: 56 })
InvestorRealtor.create({ user_id: 14, realtor_id: 56 })
InvestorRealtor.create({ user_id: 15, realtor_id: 56 })
InvestorRealtor.create({ user_id: 16, realtor_id: 56 })
InvestorRealtor.create({ user_id: 17, realtor_id: 55 })
InvestorRealtor.create({ user_id: 18, realtor_id: 56 })
InvestorRealtor.create({ user_id: 19, realtor_id: 55 })
InvestorRealtor.create({ user_id: 21, realtor_id: 55 })
InvestorRealtor.create({ user_id: 22, realtor_id: 56 })
InvestorRealtor.create({ user_id: 23, realtor_id: 55 })
InvestorRealtor.create({ user_id: 24, realtor_id: 55 })
InvestorRealtor.create({ user_id: 25, realtor_id: 55 })
InvestorRealtor.create({ user_id: 26, realtor_id: 55 })
InvestorRealtor.create({ user_id: 27, realtor_id: 55 })
InvestorRealtor.create({ user_id: 28, realtor_id: 55 })
InvestorRealtor.create({ user_id: 29, realtor_id: 56 })
InvestorRealtor.create({ user_id: 30, realtor_id: 55 })
InvestorRealtor.create({ user_id: 31, realtor_id: 55 })
InvestorRealtor.create({ user_id: 32, realtor_id: 57 })
InvestorRealtor.create({ user_id: 33, realtor_id: 55 })
InvestorRealtor.create({ user_id: 35, realtor_id: 55 })
InvestorRealtor.create({ user_id: 36, realtor_id: 57 })
InvestorRealtor.create({ user_id: 37, realtor_id: 55 })
InvestorRealtor.create({ user_id: 38, realtor_id: 57 })
InvestorRealtor.create({ user_id: 39, realtor_id: 57 })
InvestorRealtor.create({ user_id: 40, realtor_id: 55 })
InvestorRealtor.create({ user_id: 41, realtor_id: 55 })
InvestorRealtor.create({ user_id: 42, realtor_id: 55 })
InvestorRealtor.create({ user_id: 43, realtor_id: 55 })
InvestorRealtor.create({ user_id: 44, realtor_id: 55 })
InvestorRealtor.create({ user_id: 45, realtor_id: 55 })
InvestorRealtor.create({ user_id: 46, realtor_id: 55 })
InvestorRealtor.create({ user_id: 47, realtor_id: 57 })
InvestorRealtor.create({ user_id: 48, realtor_id: 55 })
InvestorRealtor.create({ user_id: 49, realtor_id: 55 })
InvestorRealtor.create({ user_id: 20, realtor_id: 55 })
InvestorRealtor.create({ user_id: 50, realtor_id: 55 })
InvestorRealtor.create({ user_id: 51, realtor_id: 55 })
InvestorRealtor.create({ user_id: 52, realtor_id: 55 })
InvestorRealtor.create({ user_id: 53, realtor_id: 55 })
InvestorRealtor.create({ user_id: 54, realtor_id: 55 })

Property.create({
	user_id: 5,
	kind: 2,
    name: "Loteamento Campo Belli",
    info: "",
    value_centavos: 0,
    stage: 0,
    visibility: 1
})

Property.create({
	user_id: 7,
	kind: 2,
    name: "Lot. São Francisco",
    info: "<div><strong>Localização</strong>: Linha Três Pontes - Descida Parque da FEMI (próximo a Vantec) - Xanxerê - SC<br><br><strong>Quantidade de lotes disponíveis</strong>: 12/67<br><br><strong>Tamanho dos lotes</strong>: acima de 360m²<br><br><strong>Infraestrutura disponível</strong>: Pavimentação asfáltica; drenagem; rede de água; rede elétrica; meio fio; arborização e placas de sinalização.<br><br></div>",
    value_centavos: 11699900,
    stage: 100,
    visibility: 1,
    stage_info: "<div><strong>Situação Obras</strong>: Infra estrutura completa, finalizando pintura de ruas e meio fio, prazo estimado de 2 (dois) meses. <br><br><strong>Situação Documentos</strong>: Escritura liberada.</div>",
    address: "-26.880701, -52.429033",
    latitude: "-26,8816918",
    longitude: "-52,4289125"
})

Property.create({
	user_id: 8,
	kind: 2,
    name: "Lot. Vale do Sol",
    info: "<div><strong>Localização</strong>: Linha Taquarussu- Saída para Bom Jesus (próximo ao posto Colpani) - Xanxerê - SC<br><br><strong>Quantidade de lotes disponíveis</strong>: 34/89<br><br><strong>Tamanho dos lotes</strong>: acima de 360m² <br><br><strong>Infraestrutura disponível</strong>: Pavimentação asfáltica; drenagem; rede de água; poço artesiano; rede elétrica; meio fio; arborização e placas de sinalização.</div>",
    value_centavos: 9000000,
    stage: 65,
    visibility: 1,
    stage_info: "<div>Situação Obras: Abertura de ruas e inicio de drenagem, prazo estimado para finalizar infra estrutura - 8 meses.<br><br>Situação Documentos: Aguardando aprovação da Casan - prazo estimado de 4 a 6 meses.</div>",
    address: "-26.847074, -52.405416",
    latitude: "-26,8461988",
    longitude: "-52,4054408"
})

Property.create({
	user_id: 9,
	kind: 2,
    name: "Lot. Parque das Flores",
    info: "<div><strong>Localização</strong>: Bairro Rosa (próximo a Cooper Vale) - Faxinal dos Guedes - SC<br><br><strong>Quantidade de lotes disponíveis</strong>: 77/124<br><br><strong>Tamanho dos lotes</strong>: acima de 360m²<br><br><strong>Infraestrutura disponível</strong>: Pavimentação asfáltica; drenagem; rede de água; rede elétrica; meio fio; arborização e placas de sinalização.</div>",
    value_centavos: 6779500,
    stage: 50,
    visibility: 1,
    stage_info: "<div><strong>Situação Obras</strong>: Abertura de ruas iniciando em até 2 meses, prazo estimado para finalizar infra estrutura de 12 meses.<br><strong><br>Situação Documentos</strong>: Aguardando liberação do licenciamento ambiental para posterior aprovação de iluminação e água. Tempo aproximado de 8 meses para conclusão.</div>",
    address: "-26.843258, -52.265452",
    latitude: "-26,8432896",
    longitude: "-52,2654793"
})

Property.create({
	user_id: 10,
	kind: 2,
    name: "Lot. Àgua Verde",
    info: "<div>Localização: Linha Serrinha - Interior de Xanxerê - SC (próximo as piscinas Lírio Tronco)<br><br>Quantidade de lotes disponíveis: 108/128<br><br>Tamanho dos lotes: acima de 360m²<br><br>Infraestrutura: Pavimentação asfáltica; drenagem; rede de água; poço artesiano; rede elétrica; meio fio; arborização e placas de sinalização.</div>",
    value_centavos: 6331638,
    stage: 60,
    visibility: 1,
    stage_info: "<div>Situação Obras: Abertura de ruas e iniciando a drenagem, prazo estimado para finalizar infra estrutura - 8 meses.<br><br>Situação Documentos: Aguardando aprovação da Casan, prazo estimado de 4 a 6 meses.</div>",
    address: "-26.903384, -52.438255",
    latitude: "-26,9031624",
    longitude: "-52,4394294"
})

Property.create({
	user_id: 11,
	kind: 2,
    name: "Lot. Levinski",
    info: "<div><strong>Localização</strong>: Centro de Ipuaçu - SC<br><br><strong>Quantidade de lotes disponíveis</strong>: 124<br><br><strong>Tamanho dos lotes</strong>: acima de 360m².<br><br><strong>Infraestrutura</strong>: Pavimentação asfáltica; drenagem; rede de água; poço artesiano; rede elétrica; meio fio; arborização e placas de sinalização.</div>",
    value_centavos: 5261840,
    stage: 50,
    visibility: 1,
    stage_info: "<div>Situação Obras: Início em até 2 meses da abertura de ruas e início de drenagem, prazo estimado para conclusão de 12 meses.<br><br>Situação Documentos: Aguardando liberação de licenciamento ambiental para posterior aprovação de água e iluminação. Tempo aproximado para aprovação final de 8 meses.</div>",
    address: "-26.630310, -52.461619",
    latitude: "-26,6324939",
    longitude: "-52,4588"
})
Property.create({
	user_id: 8,
	kind: 2,
    name: "Loteamento Xanxere",
    info: "<div>ABCD</div>",
    value_centavos: 5000000,
    stage: 0,
    visibility: 0
})
Property.create({
	user_id: 8,
	kind: 2,
    name: "Edifício Verdes Campos",
    value_centavos: 0,
    stage: 76,
    visibility: 0
})

Document.create({property_id:9, name: "luminus_-_materiais_anuncioinst_facebook.pdf", file_file_name: "luminus_-_materiais_anuncioinst_facebook.pdf", file_content_type: "application/pdf", file_file_size: 34467, file_updated_at: Time.now})
Document.create({property_id:9, file_file_name: "luminus_-_materiais_anuncioinst_facebook.pdf", file_content_type: "application/pdf", file_file_size: 34467, file_updated_at: Time.now})
Document.create({property_id:9, file_file_name: "Loteamento-Jequié-final.jpg", file_content_type: "image/jpeg", file_file_size: 307870, file_updated_at: Time.now})
Document.create({property_id:9, file_file_name: "Loteamento-Jequié-final.jpg", file_content_type: "image/jpeg", file_file_size: 307870, file_updated_at: Time.now})

Document.create({property_id:11, file_file_name: "ALVARÁ_DE_LICENÇA_-_PREFEITURA.pdf", file_content_type: "application/pdf", file_file_size: 1300040, file_updated_at: Time.now})
Document.create({property_id:11, file_file_name: "ATO_DE_APROVAÇÃO.pdf", file_content_type: "application/pdf", file_file_size: 1206881, file_updated_at: Time.now})
Document.create({property_id:11, file_file_name: "MEMORIAL_DESCRITIVO.pdf", file_content_type: "application/pdf", file_file_size: 288424, file_updated_at: Time.now})
Document.create({property_id:11, file_file_name: "PROJETO_DE_ARBORIZAÇÃO.pdf", file_content_type: "application/pdf", file_file_size: 1539869, file_updated_at: Time.now})
Document.create({property_id:11, file_file_name: "PROJETO_DE_DRENAGEM_PARA_LOTEAMENTO.pdf", file_content_type: "application/pdf", file_file_size: 1601903, file_updated_at: Time.now})
Document.create({property_id:11, file_file_name: "PROJETO_DE_REDE_DE_DISTRIBUIÇÃO_MT_E_BT.pdf", file_content_type: "application/pdf", file_file_size: 1507451, file_updated_at: Time.now})
Document.create({property_id:11, file_file_name: "PROJETO_DE_SINALIZAÇÃO_DE_VIAS.pdf", file_content_type: "application/pdf", file_file_size: 1504766, file_updated_at: Time.now})
Document.create({property_id:11, file_file_name: "PROJETO_TRATAMENTO_DE_ESGOTO.pdf", file_content_type: "application/pdf", file_file_size: 1411675, file_updated_at: Time.now})
Document.create({property_id:11, file_file_name: "CERTIDÃO_MATRICULA_N_24.209.compressed.pdf", file_content_type: "application/pdf", file_file_size: 1034015, file_updated_at: Time.now})
Document.create({property_id:11, file_file_name: "REDE_DE_ÁGUA.pdf", file_content_type: "application/pdf", file_file_size: 1748345, file_updated_at: Time.now})
Document.create({property_id:11, file_file_name: "LAI-min.pdf", file_content_type: "application/pdf", file_file_size: 483360, file_updated_at: Time.now})
Document.create({property_id:11, file_file_name: "LAP-min.pdf", file_content_type: "application/pdf", file_file_size: 459749, file_updated_at: Time.now})
Document.create({property_id:11, file_file_name: "PROJETO_DE_TERRAPLANAGEM_E_PAVIMENTAÇÃO_DO_LOTEAMENTO-min.pdf", file_content_type: "application/pdf", file_file_size: 1032369, file_updated_at: Time.now})
Document.create({property_id:11, file_file_name: "CONTRATO_SOCIAL.pdf", file_content_type: "application/pdf", file_file_size: 1951498, file_updated_at: Time.now})

Document.create({property_id:10, file_file_name: "ALVARÁ_DE_LICENÇA_-_PREFEITURA.pdf", file_content_type: "application/pdf", file_file_size: 638518, file_updated_at: Time.now})
Document.create({property_id:10, file_file_name: "ATO_DE_APROVAÇÃO.pdf", file_content_type: "application/pdf", file_file_size: 564893, file_updated_at: Time.now})
Document.create({property_id:10, file_file_name: "CERTIDÃO_MATRICULA_25.086.pdf", file_content_type: "application/pdf", file_file_size: 823141, file_updated_at: Time.now})
Document.create({property_id:10, file_file_name: "CONSULTA_DE_VIABILIDADE_CASAN.pdf", file_content_type: "application/pdf", file_file_size: 522570, file_updated_at: Time.now})
Document.create({property_id:10, file_file_name: "CONTRATO_SOCIAL.pdf", file_content_type: "application/pdf", file_file_size: 938442, file_updated_at: Time.now})
Document.create({property_id:10, file_file_name: "MEMORIAL_DESCRITIVO.pdf", file_content_type: "application/pdf", file_file_size: 285655, file_updated_at: Time.now})
Document.create({property_id:10, file_file_name: "TERMO_DE_COMPROMISSO_DE_CAUÇÃO.pdf", file_content_type: "application/pdf", file_file_size: 1608737, file_updated_at: Time.now})
Document.create({property_id:10, file_file_name: "EDITAL_DE_LOTEAMENTO.pdf", file_content_type: "application/pdf", file_file_size: 580005, file_updated_at: Time.now})
Document.create({property_id:10, file_file_name: "LAP.pdf", file_content_type: "application/pdf", file_file_size: 1251085, file_updated_at: Time.now})
Document.create({property_id:10, file_file_name: "LAI.pdf", file_content_type: "application/pdf", file_file_size: 1260820, file_updated_at: Time.now})
Document.create({property_id:10, file_file_name: "PROJETO_LOTEAMENTO.pdf", file_content_type: "application/pdf", file_file_size: 650762, file_updated_at: Time.now})

Picture.create({property_id:9, file_file_name: "Loteamento-Jequié-final.jpg", file_content_type: "image/jpeg", file_file_size: 307870, kind: 5, sequence: 0, label: ""})
Picture.create({property_id:9, file_file_name: "Loteamento-Jequié-final.jpg", file_content_type: "image/jpeg", file_file_size: 307870, kind: 4, sequence: 0, label: ""})
Picture.create({property_id:9, file_file_name: "Loteamento-Jequié-final.jpg", file_content_type: "image/jpeg", file_file_size: 307870, kind: 1, sequence: 0, label: ""})

Picture.create({property_id:10, file_file_name: "thumbnail_0202.jpg", file_content_type: "image/jpeg", file_file_size: 205833, kind: 1, sequence: 0, label: ""})
Picture.create({property_id:10, file_file_name: "thumbnail_0208.jpg", file_content_type: "image/jpeg", file_file_size: 188717, kind: 0, sequence: 0, label: ""})
Picture.create({property_id:10, file_file_name: "thumbnail_0205.jpg", file_content_type: "image/jpeg", file_file_size: 164740, kind: 0, sequence: 0, label: ""})
Picture.create({property_id:10, file_file_name: "thumbnail_0201.jpg", file_content_type: "image/jpeg", file_file_size: 202734, kind: 0, sequence: 0, label: ""})
Picture.create({property_id:10, file_file_name: "thumbnail_0201.jpg", file_content_type: "image/jpeg", file_file_size: 202734, kind: 0, sequence: 0, label: ""})
Picture.create({property_id:10, file_file_name: "thumbnail_0202.jpg", file_content_type: "image/jpeg", file_file_size: 205833, kind: 1, sequence: 0, label: ""})
Picture.create({property_id:10, file_file_name: "thumbnail_0202.jpg", file_content_type: "image/jpeg", file_file_size: 205833, kind: 1, sequence: 0, label: ""})
Picture.create({property_id:10, file_file_name: "thumbnail_0202.jpg", file_content_type: "image/jpeg", file_file_size: 205833, kind: 1, sequence: 0, label: ""})
Picture.create({property_id:10, file_file_name: "thumbnail_0204.jpg", file_content_type: "image/jpeg", file_file_size: 170888, kind: 0, sequence: 0, label: ""})
Picture.create({property_id:10, file_file_name: "thumbnail_0204.jpg", file_content_type: "image/jpeg", file_file_size: 170888, kind: 0, sequence: 0, label: ""})

Picture.create({property_id:11, file_file_name: "0202.jpg", file_content_type: "image/jpeg", file_file_size: 856682, kind: 1, sequence: 0, label: ""})
Picture.create({property_id:11, file_file_name: "0101.jpg", file_content_type: "image/jpeg", file_file_size: 761339, kind: 0, sequence: 0, label: ""})
Picture.create({property_id:11, file_file_name: "0108.jpg", file_content_type: "image/jpeg", file_file_size: 679653, kind: 0, sequence: 0, label: ""})
Picture.create({property_id:11, file_file_name: "0205.jpg", file_content_type: "image/jpeg", file_file_size: 521202, kind: 0, sequence: 0, label: ""})
Picture.create({property_id:11, file_file_name: "0105.jpg", file_content_type: "image/jpeg", file_file_size: 516348, kind: 0, sequence: 0, label: ""})
Picture.create({property_id:11, file_file_name: "0105.jpg", file_content_type: "image/jpeg", file_file_size: 516348, kind: 0, sequence: 0, label: ""})

Picture.create({property_id:12, file_file_name: "0206.jpg", file_content_type: "image/jpeg", file_file_size: 608615, kind: 1, sequence: 0, label: ""})
Picture.create({property_id:12, file_file_name: "0101.jpg", file_content_type: "image/jpeg", file_file_size: 455741, kind: 0, sequence: 0, label: ""})
Picture.create({property_id:12, file_file_name: "0105.jpg", file_content_type: "image/jpeg", file_file_size: 589406, kind: 0, sequence: 0, label: ""})
Picture.create({property_id:12, file_file_name: "0102.jpg", file_content_type: "image/jpeg", file_file_size: 325164, kind: 0, sequence: 0, label: ""})
Picture.create({property_id:12, file_file_name: "0108.jpg", file_content_type: "image/jpeg", file_file_size: 536719, kind: 0, sequence: 0, label: ""})
Picture.create({property_id:12, file_file_name: "0108.jpg", file_content_type: "image/jpeg", file_file_size: 536719, kind: 0, sequence: 0, label: ""})

Picture.create({property_id:13, file_file_name: "thumbnail_agua_verde_0104.jpg", file_content_type: "image/jpeg", file_file_size: 304581, kind: 1, sequence: 0, label: ""})
Picture.create({property_id:13, file_file_name: "thumbnail_agua_verde_0106.jpg", file_content_type: "image/jpeg", file_file_size: 178088, kind: 0, sequence: 0, label: ""})
Picture.create({property_id:13, file_file_name: "thumbnail_agua_verde_0110.jpg", file_content_type: "image/jpeg", file_file_size: 294328, kind: 0, sequence: 0, label: ""})
Picture.create({property_id:13, file_file_name: "thumbnail_agua_verde_0107.jpg", file_content_type: "image/jpeg", file_file_size: 309808, kind: 0, sequence: 0, label: ""})
Picture.create({property_id:13, file_file_name: "thumbnail_agua_verde_0101.jpg", file_content_type: "image/jpeg", file_file_size: 148612, kind: 0, sequence: 0, label: ""})
Picture.create({property_id:13, file_file_name: "mapa_Água_Verde-1.jpg", file_content_type: "image/jpeg", file_file_size: 602986, kind: 4, sequence: 0, label: ""})

Picture.create({property_id:11, file_file_name: "mapa_Vale_do_Sol-_horizontal.jpg", file_content_type: "image/jpeg", file_file_size: 775262, kind: 4, sequence: 0, label: ""})
Picture.create({property_id:14, file_file_name: "mapa_Levinski-_horizontal.jpg", file_content_type: "image/jpeg", file_file_size: 781195, kind: 4, sequence: 0, label: ""})
Picture.create({property_id:12, file_file_name: "mapa_Parque_das_Flores-1_horizontal.jpg", file_content_type: "image/jpeg", file_file_size: 985797, kind: 4, sequence: 0, label: ""})
Picture.create({property_id:10, file_file_name: "BODANESE-LOTSAOFRANCISCO-FINAL-A4-Layout1-1.jpg", file_content_type: "image/jpeg", file_file_size: 370512, kind: 4, sequence: 0, label: ""})
Picture.create({property_id:16, file_file_name: "Planta_Alegria_114m_padrao.jpg", file_content_type: "image/jpeg", file_file_size: 120931, kind: 4, sequence: 0, label: "Planta_Andar1"})
Picture.create({property_id:16, file_file_name: "Planta_Alegria_114m_padrao.jpg", file_content_type: "image/jpeg", file_file_size: 120931, kind: 4, sequence: 0, label: "Planta_Andar2"})

Picture.delete([16,18,19,21,27,33])