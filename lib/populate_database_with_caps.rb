class PopulateDatabaseWithCaps
	def initialize(file_path)
    	@caps = JSON.parse(File.read(file_path))
  	end

  	def populate
    	@caps.each do |cap|
      		Cap.create(cap)
    	end
    end
end
