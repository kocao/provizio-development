class PopulateDatabaseWithCitiesAndStates
  def initialize(file_path)
    @data = JSON.parse(File.read(file_path))
  end

  def populate
    @data.each do |state_data|
      state = create_state(state_data)
      state_data['cities'].each do |city_data|
        create_city(city_data, state, capital?(city_data, state_data))
      end
    end
  end

  private

  def capital?(city, state)
    city['name'] == state['capital']
  end

  def create_state(data)
    state_data = data.slice('acronym', 'name', 'region', 'code')
    state_data['id'] = state_data.delete('code')
    State.create(state_data)
  end

  def create_city(data, state, capital = false)
    city_data = data.slice('name', 'code')
    city_data['id'] = city_data.delete('code').to_i
    City.create(city_data.merge(capital: capital, state: state))
  end
end
