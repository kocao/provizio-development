class PopulateDatabaseWithPage
	def initialize( file_path, name = nil )
    	@page = JSON.parse( File.read( file_path ) )
        unless name.nil?
            page = Page.find_by_name( name )
            page.destroy unless page.nil?
        end
  	end

  	def populate
    	page = Page.create({
            name: @page['name'].parameterize,
            display_name: @page['display_name'],
            title: @page['title'],
            description: @page['description'],
            keywords: @page['keywords'],
        })
        if @page.has_key?( 'meta' )
            @page['meta'].each do |m|
                metum = Metum.create({
                    page_id: page.id,
                    name: m['name'],
                    value: m['value'],
                    label: m['label'],
                    field_type: m['field_type'],
                    order: m.has_key?( 'order') ? m['order'] : 0
                })
                if m['field_type'] == 'dropbox_image'
                    attach = Attachment.create!({
                        attachable_type: 'Metum',
                        attachable_id: metum.id,
                        modifier: m['name'],
                        file: File.new("#{Rails.root}/app/assets/images/#{m['value']}")
                    })
                    metum.update_attribute( :value, attach.id )
                end
                if m.has_key?( 'contents' )
                    m['contents'].each do |c|
                        content = Content.create({
                            metum_id: metum.id,
                            name: c['name'],
                            value: c['value'],
                            label: c['label'],
                            field_type: c['field_type'],
                            order: c.has_key?( 'order') ? c['order'] : 0
                        })
                        if c['field_type'] == 'dropbox_image'
                            attach = Attachment.create!({
                                attachable_type: 'Content',
                                attachable_id: content.id,
                                modifier: c['name'],
                                file: File.new("#{Rails.root}/app/assets/images/#{c['value']}")
                            })
                            content.update_attribute( :value, attach.id )
                        end
                    end
                end
            end
        end
    end
end
