class PopulateDatabaseWithProperties
	def initialize( file_path )
    	@properties = JSON.parse( File.read( file_path ) )
  	end

  	def populate
    	@properties.each do |property|
      		prop = Property.create({
                name: property['name'],
                kind: property['kind'],
                visibility: 1,
                value_centavos: rand(100000..1000000),
                user_id: User.find_by_email( 'shane@gmail.com' ).id
            })
            Picture.create!({
                kind: 1,
                property_id: prop.id,
                file: File.new("#{Rails.root}/app/assets/images/#{rand(1..4)}.png")
            })
    	end
    end
end
