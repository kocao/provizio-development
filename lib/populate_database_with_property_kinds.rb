class PopulateDatabaseWithPropertyKinds
  	def populate
    	PropertyKind.create!({ name: 'apartment', display_name: 'Apartamento' })
		PropertyKind.create!({ name: 'house', display_name: 'Casa' })
		PropertyKind.create!({ name: 'condominium', display_name: 'Condomínio' })
		PropertyKind.create!({ name: 'allotment', display_name: 'Loteamento' })
		PropertyKind.create!({ name: 'comercial_room', display_name: 'Sala comercial' })
		PropertyKind.create!({ name: 'terrain', display_name: 'Terreno' })
    end
end
