class PopulateDatabaseWithRolesPermissions
	def initialize( file_path )
    	@roles = JSON.parse( File.read( file_path ) )
  	end

  	def populate
  		caps = Cap.all
    	@roles.each do |role|
      		o_role = Role.create({
      			name: role['name'],
      			display_name: role['display_name']
      		})
      		if role['caps'] == '*'
      			caps.each do |cap|
      				Permission.create({
      					"permissionable_type": "Role",
      					"permissionable_id": o_role.id,
      					"cap_id": cap.id
      				})
      			end
      		else
      			role['caps'].each do |cap_name|
      				cap = caps.detect{|c| c.name == cap_name}
      				Permission.create({
      					"permissionable_type": "Role",
      					"permissionable_id": o_role.id,
      					"cap_id": cap.id
      				})
      			end
      		end
    	end
    end
end
