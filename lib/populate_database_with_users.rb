class PopulateDatabaseWithUsers
	def initialize( file_path )
    	@users = JSON.parse( File.read( file_path ) )
  	end

  	def populate
    	@users.each do |user|
      		User.create!({
                name: user['name'],
                email: user['email'],
                role_id: Role.find_by_name( user['role'] ).id,
                password: "643216",
                password_confirmation: "643216",
                terms: '1'
            })
    	end
    end
end
