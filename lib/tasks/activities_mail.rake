desc "Check for pending activities notifications"
task :activities_mail => :environment do
  puts "Checking for pending activities notifications"
  ActivitiesWorker.new.perform
  puts "All pending notifications are sent"
end