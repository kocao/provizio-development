desc "Check for reserved properties and remove reservation if expired"
task :reservations_expiration => :environment do
  puts "Checking for reservations"
  ReservationsWorker.new.perform
  puts "Reservations checked"
end